//
//  SearchLocationViewController.h
//  JO Taxi
//
//  Created by Hammoudeh Alamri on 19/04/2017.
//  Copyright © 2017 iTeks. All rights reserved.
//

#import "BaseController.h"
#import "SelectFromMapViewController.h"
@interface SearchLocationViewController : BaseController<HTTPDel, UITextFieldDelegate , UIScrollViewDelegate>


@property (nonatomic) int seachMode;
@property (nonatomic) int seachType; /** 1: from 2: to **/
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constBuildingWidth;

@property (weak, nonatomic) IBOutlet UIButton *btnBack;

@property (weak, nonatomic) IBOutlet UITextField *etSearch;
@property (weak, nonatomic) IBOutlet UITextField *etBuilding;
@property (weak, nonatomic) IBOutlet UIButton *btnMap;
@property (weak, nonatomic) IBOutlet UIButton *btnBuilding;
@property (weak, nonatomic) IBOutlet UIButton *btnPersonal;
@property (weak, nonatomic) IBOutlet UIView *viPersonalSelect;
@property (weak, nonatomic) IBOutlet UIView *viBuildingSelect;
@property (weak, nonatomic) IBOutlet UIView *viFavSelect;
@property (weak, nonatomic) IBOutlet UIView *viTabs;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loadingConst;

@property (strong, nonatomic) id<SelectViewControllerDelegate> delegate;


@end

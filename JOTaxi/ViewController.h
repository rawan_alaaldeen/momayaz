//
//  ViewController.h
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/10/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"
#import "SignalRMan.h"
#import "HTTPDelegate.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKProfile.h> 
#import <FBSDKCoreKit/FBSDKAccessToken.h>
#import <FBSDKCoreKit/FBSDKGraphRequest.h>
#import <Google/SignIn.h>

@import Firebase;
@import FirebaseMessaging;



@interface ViewController : BaseController <HTTPDel,GIDSignInUIDelegate,FBSDKLoginButtonDelegate,GIDSignInDelegate>

@property (weak, nonatomic) IBOutlet UIView *viwLogin;
@property (weak, nonatomic) IBOutlet UIView *viwRegister;
 
@property (weak, nonatomic) IBOutlet UITextField *etxtUsername_login;

@property (weak, nonatomic) IBOutlet UITextField *etxtPass_login;

//@property (weak, nonatomic)  id<loginProtocol>  delegate;
@property (nonatomic)   int destination; // 1: amman, 2: other city,  3: airport

@property(weak, nonatomic) IBOutlet GIDSignInButton *GsignInButton;







@end


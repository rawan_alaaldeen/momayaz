//
//  ViewController.m
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/10/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import "ViewController.h"
#import "MainViewController.h"
#import "MainRootViewController.h"
#import "RegisterViewController.h"
#import "User.h"
#import "Consts.h"
#import "Order.h"
#import "AppDelegate.h"
#import "HomeViewController.h"
#import "JOTaxi-Swift.h"

 

@interface ViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *stackViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *stackViewSmalliPhoneHeightConstrant;

@end

@implementation ViewController
{
    UIAlertAction *okAction;
     HTTPDelegate * dl ;
    FBSDKLoginButton *FBloginButton;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
      [GIDSignIn sharedInstance].uiDelegate = self;
      [GIDSignIn sharedInstance].delegate = self;

    FBloginButton = [[FBSDKLoginButton alloc] init];
    FBloginButton.hidden = YES;

    
    FBloginButton.readPermissions =
    @[@"public_profile", @"email"];
    FBloginButton.delegate=self;
 
    self.navigationController.navigationBarHidden =YES;
    

}


-(void)viewWillAppear:(BOOL)animated{

    
    
    if( [GeneralFunc getPrefIntForKey:@"userid"]){
    
        [self goToNext];
    
    }

}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self updateConstraint];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)updateConstraint{
    
    if ([GeneralFunc isSmallDevise]){
        _stackViewSmalliPhoneHeightConstrant.active = true;
        _stackViewHeight.active = false;
    }
    else{
        _stackViewSmalliPhoneHeightConstrant.active = false;
        _stackViewHeight.active = true;
        
    }
}

- (IBAction)actionForgetPass:(id)sender {
    
    UIInputViewController *recoverPasswordViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"RecoverPasswordViewController"];
    
     [self presentViewController:recoverPasswordViewController animated:true completion:nil ];
    

}

 - (IBAction)actionDoLogin:(id)sender {
    
     
     if(![AppHelper isThereInternetConnection]){
         [self showCallAlert];
     }
     else{
    BOOL allIsWell= true;
     //    NSLog(@"%@", [o JSONString]);
//    
    
    // validate email
    if(!([GeneralFunc validateEmail:_etxtUsername_login.text] ||[GeneralFunc validatePhone:_etxtUsername_login.text]) ){
        
        
        allIsWell =false;
        
        [GeneralFunc validateTextField:_etxtUsername_login isValid:false];
        
        
    }else{
        [GeneralFunc validateTextField:_etxtUsername_login isValid:true];
        
    }
    
    if([_etxtPass_login.text stringByReplacingOccurrencesOfString:@" " withString:@""].length<1  ){
        allIsWell =false;
        [GeneralFunc validateTextField:_etxtPass_login isValid:false];
        
    }else{
        [GeneralFunc validateTextField:_etxtPass_login isValid:true];
     }


    
    if(!allIsWell){
    
        return;
    }
    
    NSString * url = [NSString stringWithFormat:@"%@%@Identifier=%@&Password=%@&token=&authService=&GCMTokenID=%@", URLsMainURL ,URLsLogin,_etxtUsername_login.text , _etxtPass_login.text  ,[[FIRInstanceID instanceID] token]];
    
    dl = [[HTTPDelegate alloc] initWithURL:url];
    dl.delegate =self;
    [dl startHttp];
     }
    
}


-(void)showCallAlert{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        CallPopup *popup = [[NSBundle mainBundle] loadNibNamed:@"CallPopup" owner:nil options:nil].lastObject;
        popup.frame = self.view.bounds;
        popup.alpha = 0;
        [self.view addSubview:popup];
        
        [popup fadeInWithDuration:0.5];
    });
    
}



- (IBAction)actionReg:(id)sender{

    
    RegisterViewController * v = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    
//    v.delegate = self.delegate;
    [self presentViewController:v animated:false completion:nil ];
    
    
}


-(void) goToNext{
    
    
//    UIViewController *v = nil ;
//    
//    NSInteger type= [GeneralFunc getPrefIntForKey:@"type"];
    
    
    
    
//    switch (type) {
//        case 1:
//            
//            v = [self.storyboard instantiateViewControllerWithIdentifier:@"PickupFromAmmanViewController"];
//            break;
//            
//        case 2:
//            v = [self.storyboard instantiateViewControllerWithIdentifier:@"PickupOutAmmanViewController"];
//            break;
//            
//        case 3:
//            
//            v = [self.storyboard instantiateViewControllerWithIdentifier:@"FillAirportsInfoViewController"];
//            
//            break;
//            
//        default:
//            break;
//    }
    
    HomeViewController *viewController = [HomeViewController new];
    
    MainRootViewController *navigationController = [[MainRootViewController alloc] initWithRootViewController:viewController];
    
    
    RootMainViewController = [[MainViewController alloc] initWithRootViewController:navigationController presentationStyle:LGSideMenuPresentationStyleScaleFromLittle
                                                                               type:0];
    
    
    
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    
    window.rootViewController = RootMainViewController;
    
    [UIView transitionWithView:window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromRight
                    animations:nil
                    completion:nil];

    
    
    
//    [self dismissViewControllerAnimated:YES completion:^{
//        
//        if([self.delegate respondsToSelector:@selector(loginWithStatus:)]){
//        
//            [self.delegate loginWithStatus:YES];
//        
//            
//        }
//    
//    }];

    
}

-(void) showLeftMenu {
    
    if(RootMainViewController)
        [RootMainViewController showLeftViewAnimated:YES completionHandler:nil];
}


- (IBAction)goBack:(id)sender {
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}



#pragma mark - Http

-(void)HTTPJustStart{
    
    [GeneralFunc showLoading:self];
}


-(void)HTTPDone:(NSData *)data response:(NSURLResponse *)res error:(NSError *)error{
    

    dispatch_async(dispatch_get_main_queue(), ^{
        [GeneralFunc hideLoading:self];
        
        NSObject *res = [JSONParser ParseLoginRes:data];
        
        if (res){
        if([res isKindOfClass:[User class]]){
        
              User *u = (User *)res;
              [GeneralFunc addIntPref:(int)[u.userID integerValue] forKey:@"userid"];
              [GeneralFunc addStringPref:u.Email forKey:@"Email"];
              [GeneralFunc addStringPref:u.NameAR forKey:@"NameAR"];
              [GeneralFunc addStringPref:u.NameEn forKey:@"NameEn"];
              [GeneralFunc addStringPref:u.Phone forKey:@"Phone"];
              [GeneralFunc addStringPref:u.Gender forKey:@"Gender"];
              [GeneralFunc addStringPref:u.Verified forKey:@"Verified"];
              [GeneralFunc addStringPref:u.Password forKey:@"Password"];
            
            
          
             [self goToNext];
           

            
        }
        
        else{
            
            NSDictionary *dic = (NSDictionary*) res;
            if ([dic objectForKey:@"Status"]){
                if([[dic objectForKey:@"Status"] integerValue] == 0 ){
                    
                    UIAlertController * al = [GeneralFunc showAlertDialogWithTitle:NSLocalizedString(@"", @"") Message:NSLocalizedString(@"user_not_Found", @"") ];
                    
                    
                    [al addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction *l ){
                        [al popoverPresentationController];
                        
                    }]];
                    
                    [self presentViewController:al animated:YES completion:nil];

                }
                
                
            }
            else{
                UIAlertController * al = [GeneralFunc showAlertDialogWithTitle:NSLocalizedString(@"", @"") Message:NSLocalizedString((NSString*)res, @"") ];
            
            
                [al addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction *l ){
                                    [al popoverPresentationController];
                
                                }]];
            
                [self presentViewController:al animated:YES completion:nil];
            }
        
        }
        
        
        }
    });
}
-(void)HTTPDone:(NSData *)data response:(NSURLResponse *)res error:(NSError *)error forID:(int)ID{

    

   dispatch_async(dispatch_get_main_queue(), ^{
      
       [GeneralFunc hideLoading:self];
    
    if(ID ==101){ // social media
        
        NSObject *res = [JSONParser ParseLoginRes:data];
        
        if (res){
            
        
        if([res isKindOfClass:[User class]]){
            
            User *u = (User *)res;
            
            //** go to home when user ID is exist
            if ([u.userID integerValue] != 0){
                [GeneralFunc addIntPref:(int)[u.userID integerValue] forKey:@"userid"];
                [GeneralFunc addStringPref:u.Email forKey:@"Email"];
                [GeneralFunc addStringPref:u.NameAR forKey:@"NameAR"];
                [GeneralFunc addStringPref:u.NameEn forKey:@"NameEn"];
                [GeneralFunc addStringPref:u.Gender forKey:@"Gender"];
                [GeneralFunc addStringPref:u.Verified forKey:@"Verified"];
                [GeneralFunc addStringPref:u.Password forKey:@"Password"];
                [GeneralFunc addStringPref:u.Phone forKey:@"Phone"];
                
                [self goToNext];

            }
            
            else{
                [self checkUserStatus:data];}
        }
        else{
            
            [self checkUserStatus:data];}
        }
        
        else{
            NSLog(@"Login ERROR");
        }
    
    }else if (ID==102) { // change password
    
    
        UIAlertController * al=nil;
        
        NSString * s = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        if( [[s stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:@"1"]){
            al = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Password recovery", @"")
                                                     message:NSLocalizedString(@"Your new password has been sent. Please check your email.",@"")
                                              preferredStyle:UIAlertControllerStyleAlert];
            
        }
        else if( [s isEqualToString:@"-1"]){
            al = [UIAlertController alertControllerWithTitle:@""
                                                     message:NSLocalizedString(@"email not found",@"")
                                              preferredStyle:UIAlertControllerStyleAlert];
            
        }
        else{
            
            
            al = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Password recovery", @"")
                                                     message:NSLocalizedString(@"Some error occur, please try again",@"")
                                              preferredStyle:UIAlertControllerStyleAlert];
            
        }
        
        
        
        if(al){
            
            [al addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleCancel handler:^(UIAlertAction *l ){                     [al popoverPresentationController];
                
            }]];
            
            [self presentViewController:al animated:YES completion:nil];
            
        }
        
    
    }
});


}


-(void)checkUserStatus:(NSData*)data{
    
    NSMutableDictionary  *dic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    
    if( [dic objectForKey:@"Status"]){
        //** go to reg when user has status -2
        if([[dic objectForKey:@"Status"] integerValue] == -2 ){
            
            RegisterViewController * v = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
            
            v.isSMUser = true;
            //    v.delegate = self.delegate;
            [self presentViewController:v animated:YES completion:nil ];
            
        }else{
            
            UIAlertController * al =  [GeneralFunc showAlertDialogWithTitle:@"" Message:[dic objectForKey:@"Message"]];
            
            [al addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction *l ){
                [al popoverPresentationController];
                
            }]];
            
            [self presentViewController:al animated:YES completion:nil];
            
        }
    }
    
    
}
-(void)HTTPInProgress{

}


#pragma mark- facebook login
- (IBAction)loginViaFacebook {
    [FBloginButton sendActionsForControlEvents: UIControlEventTouchUpInside];
}

-(void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error{

    
    if ([FBSDKAccessToken currentAccessToken]) {
        [GeneralFunc showLoading:self];
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"email,name,first_name,gender"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 NSLog(@"fetched user:%@", result);
                 NSLog(@"%@",result[@"email"]);
                 
                 if([FBSDKAccessToken currentAccessToken]){
                 [GeneralFunc addIntPref:0 forKey:@"userid"];
                 [GeneralFunc addStringPref:result[@"email"] forKey:@"Email"];
                 [GeneralFunc addStringPref:result[@"name"] forKey:@"NameAR"];
                 [GeneralFunc addStringPref:result[@"name"] forKey:@"NameEn"];
                 [GeneralFunc addStringPref:result[@"gender"] forKey:@"Gender"];
                 [GeneralFunc addStringPref:@"1" forKey:@"Verified"];
                 [GeneralFunc addStringPref:@"" forKey:@"Password"];
                 
                 [GeneralFunc hideLoading:self];
                 

                 NSString * url = [NSString stringWithFormat:@"%@%@Identifier=%@&Password=&authService=f&token=%@&GCMTokenID=%@", URLsMainURL ,URLsLogin,result[@"email"] , [FBSDKAccessToken currentAccessToken].tokenString ,[[FIRInstanceID instanceID]token]];
                 
                 dl = [[HTTPDelegate alloc] initWithURL:url];
                 dl.delegate =self;
                 [dl startHttp:101];
                 }
                 
             }
         }];

    }
    
   //  NSLog(@"FBTOKEN == %@",[FBSDKAccessToken currentAccessToken].tokenString);
    
    


}

-(void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton{

}

#pragma mark- Google login
- (IBAction)signInViaGoogle:(id)sender {
    [[GIDSignIn sharedInstance] signIn];

}


- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
        dispatch_async(dispatch_get_main_queue(), ^{
        [GeneralFunc hideLoading:self];
    });
}
-(void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error{

  
    dispatch_async(dispatch_get_main_queue(), ^{
        [GeneralFunc hideLoading:self];
    });

    
    if( signIn.currentUser ) {
        
        NSLog(@"inffooooo %@ ",  signIn.currentUser .profile.email);
        NSLog(@"inffooooo %@ ",  signIn.currentUser.authentication.idToken);
        
        
        
        
        [GeneralFunc addIntPref:0 forKey:@"userid"];
        [GeneralFunc addStringPref: signIn.currentUser.profile.email forKey:@"Email"];
        [GeneralFunc addStringPref:signIn.currentUser.profile.givenName forKey:@"NameAR"];
        [GeneralFunc addStringPref:signIn.currentUser.profile.givenName forKey:@"NameEn"];
        [GeneralFunc addStringPref:@"male" forKey:@"Gender"];
        [GeneralFunc addStringPref:@"1" forKey:@"Verified"];
        [GeneralFunc addStringPref:@"" forKey:@"Password"];
        
        [GeneralFunc hideLoading:self];
        
        NSString * url = [NSString stringWithFormat:@"%@%@Identifier=%@&Password=&authService=g&token=%@&GCMTokenID=%@", URLsMainURL ,URLsLogin,signIn.currentUser.profile.email , signIn.currentUser.authentication.idToken ,[[FIRInstanceID instanceID] token] ];
        
        dl = [[HTTPDelegate alloc] initWithURL:url];
        dl.delegate =self;
        [dl startHttp:101];
        
        
        
        NSLog( @"TOken %@ ", signIn.currentUser.authentication.idToken );
        
    }


}


// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    //[[GIDSignIn sharedInstance] signOut];
   
    [self presentViewController:viewController animated:YES completion:nil];
}


// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField.tag == 100){
        NSString *finalString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        [okAction setEnabled:(finalString.length >= 1)];

    }
    return YES;

}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
     
    if (textField == _etxtUsername_login){
        [_etxtPass_login becomeFirstResponder];
    }
    else{
        [textField resignFirstResponder];
    }
    return true;
}

@end

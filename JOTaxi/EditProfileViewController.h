//
//  EditProfileViewController.h
//  Momayaz
//
//  Created by Rawan Alaeddin on 8/11/17.
//  Copyright © 2017 GCE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTTPDelegate.h"
 

@interface EditProfileViewController : UIViewController <HTTPDel>
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *genderSegment;




@end

//
//  MainViewController.m
//  JO Taxi
//
//  Created by Hammoudeh Alamri on 09/04/2017.
//  Copyright © 2017 iTeks. All rights reserved.
//

#import "HomeViewController.h"
#import "StylingHelper.h" 

#import "SearchLocationViewController.h"
#import "TrackOrderViewController.h" 
#import "RateDriverViewController.h"
#import "UserMarker.h"
#import "CarMarker.h"
#import "JOTaxi-Swift.h"


@interface HomeViewController ()

@end


@implementation HomeViewController
{

    
    GMSMarker *sourceMarker,*destinationMarker;
    UIImageView *fromPinMarkerIcon;

 
    CLLocationManager *locationManager;
    Place *selected ;
    GMSPolyline *polyline;
    
    HTTPDelegate *httpBookLater ;
    HTTPDelegate *http ;
    HTTPDelegate *findPath ;
    HTTPDelegate *httpTaxies ;
    HTTPDelegate *httpLocation ;
    
    CLLocation *currentLocation;
    BOOL mapDraged;
    BOOL driveMood;
    
    BOOL stillWorking;
    BOOL isHttpTaxiesWorking;
    BOOL isFirstTimeToOpenTheMap;

    UITextField *selectedUF;
    
    Order *o ;
    NSTimer *timer ;
    Place *fromPlace , *toPlace;
    NSMutableArray *carsMarksers;
    NSMutableArray *availabeCars;

    NSDate *selectedLaterDate ;
    
     BOOL isUserDragging;
    BOOL isListOpend;
    BOOL mustUpdateSource;

    UserMarker *userMarkerView;
    CarMarker *carMarkerView;
 
    int bookingType; // 1 for booknoe 2 for bookLater
}

- (void)viewDidLoad {
    [super viewDidLoad];
 
    _map.delegate=self;
    _map.myLocationEnabled = YES;
    _map.settings.myLocationButton = YES;
    selectedLaterDate =nil ;

    [self styleTheMap];
    isFirstTimeToOpenTheMap = true;
    
   
    selected = [Place new];
    
    
     
    CGFloat r = _bookBtnHolder.frame.size.width/ 2;
    [StylingHelper addCornerRadius:_bookBtnHolder radius:r];
    [StylingHelper addShadow:_bookBtnHolder radius:r];
    
    CGFloat r2 = _confirmBtnHolder.frame.size.width/ 2;
    [StylingHelper addCornerRadius:_confirmBtnHolder radius:r2];
    [StylingHelper addShadow:_confirmBtnHolder radius:r2];
    _confirmBtnBottom.constant=-100;
    
  
    _viBookNow.hidden =YES;
    _viBookLater.hidden =YES;
    
    [StylingHelper addShadow:_viBookNow radius:15];
    [StylingHelper addCornerRadius:_viBookNow radius:15];
    [StylingHelper addShadow:_viBookLater radius:15];
    [StylingHelper addCornerRadius:_viBookLater radius:15];
    
    
    carsMarksers = [NSMutableArray new];
 
    _etFrom.placeholder= NSLocalizedString(@"source_location", nil);
    _etTo.placeholder= NSLocalizedString(@"to_location", nil);

    [_btnBookNow setTitle:NSLocalizedString(@"Book Now", nil) forState:UIControlStateNormal];
    [_btnBookLater setTitle:NSLocalizedString(@"Book Later", nil) forState:UIControlStateNormal];
    
    fromPinMarkerIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    fromPinMarkerIcon.image = [UIImage imageNamed:@"gpin.png"];
    fromPinMarkerIcon.contentMode = UIViewContentModeScaleAspectFit;

    userMarkerView = [[[NSBundle mainBundle] loadNibNamed:@"UserMarker" owner:self options:nil] objectAtIndex:0];

    carMarkerView = [[[NSBundle mainBundle] loadNibNamed:@"CarMarker" owner:self options:nil] objectAtIndex:0];
    
    _nearestCarsList.dataSource = self;
    
    
}




- (void)startLocationManager
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
        [locationManager requestWhenInUseAuthorization];
    }else{
        [locationManager startUpdatingLocation];
    }
    
}


-(void)viewDidAppear:(BOOL)animated{
    
    
    if([GeneralFunc getPrefIntForKey:@"Reset"]>0){
        
        _etFrom.text =@"";
         fromPlace=nil;
        _etTo.text =@"";
        destinationMarker=nil ;
        toPlace=nil ;
        _map.trafficEnabled = YES;
        [_map clear];

        sourceMarker = nil;
        [GeneralFunc addIntPref:0 forKey:@"Reset"];
        
    }
    
    mustUpdateSource = true;

     [self startLocationManager];
    
   //   [GeneralFunc addIntPref:11017 forKey:@"NeedToRate"];
    if([GeneralFunc getPrefIntForKey:@"NeedToRate"]>0){
        RateDriverViewController *dr = [RateDriverViewController new];
        dr.isFromHistory = false;
        [self presentViewController:dr animated:YES completion:nil];
    }
    
 
    if(![AppHelper isThereInternetConnection]){
        [self showCallAlert];
    }
 
}

-(void)viewWillAppear:(BOOL)animated{

    [AppDelegate setDelegate:self];
    self.navigationController.navigationBarHidden =YES;
    
    
}

-(void)viewWillDisappear:(BOOL)animated{

    [AppDelegate setDelegate:nil];

}





#pragma mark - location

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            [locationManager requestWhenInUseAuthorization];
        }
            break;
        default:{
            [locationManager startUpdatingLocation];
        }
            break;
    }
    
    

}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{

         currentLocation = [locations objectAtIndex:0];
    
        //** update camera position for first time only
        if  (isFirstTimeToOpenTheMap){
            

            dispatch_async(dispatch_get_main_queue(), ^{

            [CATransaction begin];
            [CATransaction setValue:[NSNumber numberWithFloat: 1] forKey:kCATransactionAnimationDuration];
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentLocation.coordinate.latitude
                                                                longitude:currentLocation.coordinate.longitude
                                                                     zoom:18];
            [_map animateToCameraPosition: camera];
                [CATransaction commit];});

            [self setSourceLocation:CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude)];

            isFirstTimeToOpenTheMap = false;
            
        }
    
        if(mustUpdateSource && !sourceMarker){
 
            mustUpdateSource = false;
            [self setSourceLocation:CLLocationCoordinate2DMake(currentLocation.coordinate.latitude,     currentLocation.coordinate.longitude)];
            
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentLocation.coordinate.latitude
                                                                    longitude:currentLocation.coordinate.longitude
                                                                         zoom:18];
            [_map animateToCameraPosition: camera];

        }
    
    
        [GeneralFunc addStringPref:[ NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude] forKey:  @"lat"];
        [GeneralFunc addStringPref:  [ NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude] forKey:@"lng"];
    

}

-(void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    
        if (isUserDragging){
            isUserDragging = false;
            CGPoint mapCenter = [_map center];
            mapCenter.y += _imgCenterPin.frame.size.height/2 + 24;
            mapCenter.x += _imgCenterPin.frame.size.width/2 + 11;
            
            
            CLLocationCoordinate2D selectedLocation = [[_map projection] coordinateForPoint:mapCenter];
            
            [self setSourceLocation:selectedLocation];}
}

-(void)setSourceLocation:(CLLocationCoordinate2D)location{
    
    if ([GeneralFunc isLocationInJordan:location]){
     
        if (!sourceMarker){
        [self setSourcePlaceMarker];
    }
    [self hideSelectFromMapPin];
    
     sourceMarker.position = location;
    
    [self loadThePlaceName:[NSString stringWithFormat:@"%lf",location.latitude] lng:[NSString stringWithFormat:@"%lf",location.longitude]];
    
    [self updateNearbyTaxi];
    }
    else{
        _imgCenterPin.image = [UIImage imageNamed:@"pin_outside"];
        _lblMsg.text = NSLocalizedString(@"OUT SIDE JORDAN", nil);

     }
    
}



-(void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture{
    
    if (gesture && (!destinationMarker || !sourceMarker)){
        //** allow user to select source place JUST in case destination dosnt select
        isUserDragging = true;
        [self showSelectFromMapPin];
    }
    else{
        isUserDragging = false;
    }
    
}

-(void)setSourcePlaceMarker{
    sourceMarker = [GMSMarker new];
    
    //[self updateNearestCarDuration:@""];
    [self updateUserLocation:@""];
    sourceMarker.iconView = userMarkerView ;
    sourceMarker.map = _map;
}

-(void)showSelectFromMapPin{
    _imgCenterPin.hidden = false;
    _imgCenterPin.image = [UIImage imageNamed:@"user_pin"];

    //**hide pin
     sourceMarker.map = nil;
     sourceMarker = nil;
}

-(void)hideSelectFromMapPin{
    sourceMarker.map = _map;
   _imgCenterPin.hidden = true;
 
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    
}



-(void)loadThePlaceName:(NSString *) lat lng:(NSString *) lng{
    
    
   
    NSString *url =[NSString stringWithFormat:@"%@AddressingService/GetAddress.ashx?x=%@&y=%@",@"http://188.247.86.76/", lng,lat];
    
    
    if(httpLocation) [httpLocation stopHttp];
    if(!fromPlace){
        fromPlace = [Place new];
    }
    fromPlace.lat = lat;
    fromPlace.lng = lng;
    
    
    httpLocation = [[HTTPDelegate alloc] initWithURL:url];
    httpLocation.delegate=self;
    [httpLocation startHttp:90];
    
    
}




-(void) updateNearbyTaxi {
    
    if(!isHttpTaxiesWorking){
    
        NSString *lat = (fromPlace)? fromPlace.lat:@"31.942";
        NSString *lng = (fromPlace)? fromPlace.lng:@"35.88";
        
        httpTaxies = [[HTTPDelegate alloc  ]  initWithURL:[NSString stringWithFormat:@"%@%@Longitude=%@&Latitude=%@",URLsMainURL , URLsNearby, lng,lat]];
    
        httpTaxies.delegate =self;
        [httpTaxies startHttp:1001];
    
        isHttpTaxiesWorking= true;
    
    }
    
}




-(void) styleTheMap{

    NSBundle *mainBundle = [NSBundle mainBundle];
    NSURL *styleUrl = [mainBundle URLForResource:@"googlemapstyle" withExtension:@"json"];
    NSError *error;
    
    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];
    
    if (!style) {
        NSLog(@"The style definition could not be loaded: %@", error);
    }
    
    _map.mapStyle = style;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - actions


-(void)updateSourcePinToCurrentLocation{

    
 }


- (IBAction)openMapFrom:(id)sender {
}
- (IBAction)openMapTo:(id)sender {
}


- (IBAction)BookLater:(id)sender {
    
    if([self checkTime:2 time:selectedLaterDate])
    
        [self showExtraJDDialog:2 popupType:extraFeesPopup];
    
}


-(void) doBookLater{

    
    MDDatePickerDialog *datePicker = [[MDDatePickerDialog alloc] init];
    
    datePicker.tintColor = UIColorFromRGB(0xF1DA4E);
    [datePicker setMinimumDate:[NSDate date]];
    datePicker.delegate = self;
    [datePicker show];
    
    
    

}


-(BOOL)checkTime:(int) type time:(NSDate *) bookdate{

    BOOL showMessage = false;
    
    NSString *time1 = [GeneralFunc getPrefStringForKey:@"lastOrderTime"];
    NSString *laterTimes =[GeneralFunc getPrefStringForKey:@"lastOrderTime_later"];
    if(!time1) time1=@"0";
    if(!laterTimes) laterTimes=@"0";
    
    
    NSMutableArray * times = [NSMutableArray new];
    [times addObject:time1];
    [times addObjectsFromArray:[laterTimes componentsSeparatedByString:@","]];
    
    for(NSString *t in times){
    long lasttime = [t longLongValue];
    //18 , 25
    NSLog(@"%ld , %ld ", lasttime , (long)bookdate.timeIntervalSince1970 );
    if(lasttime >0 && (lasttime+ (60*10)>(long)bookdate.timeIntervalSince1970)){
        showMessage=true;
        break;
    }
    
    }
    
    if(showMessage){
    
            [self showExtraJDDialog:type popupType:orderAgainPopup];
        
        return false;
    
    }
    
    
 return true;
    

}

- (IBAction)BookNow:(id)sender {
    

    BOOL allIsWell = true;
    
    
    if(!fromPlace){
        [GeneralFunc validateTextField:_etFrom isValid:false ];
        allIsWell=false;
    }else{
        [GeneralFunc validateTextField:_etFrom isValid:true ];
    }
    
    
    
    
    
    if(allIsWell && [self checkTime:1 time:[NSDate date]]){
         [self showExtraJDDialog:1 popupType:extraFeesPopup];
        
    }else{
    
    
    }
    
    
}

-(void)doBookNow{
    
    
    o = [Order new];
    o.AppUserID = [GeneralFunc getPrefStringForKey:@"userid"];
    
    
    if(fromPlace){
        
        o.SAddress = fromPlace.address;
        o.SLat = fromPlace.lat;
        o.SLng = fromPlace.lng;
    }
    
    if(toPlace){
        o.EAddress = toPlace.address;
        o.ELat = toPlace.lat;
        o.ELng = toPlace.lng;
    }
    
    
    
    
    o.PickupTime  =[GeneralFunc getDateTimeAsStringForServer:[NSDate date]];
    
    o.Status =@"4";
    o.DestinationCategory = [NSString stringWithFormat:@"%@" , @"3"];
    
    
    
    int pref = 0;
    pref +=0;
    
    o.Preferences = [NSString stringWithFormat:@"%d", pref];
    NSString *s = [o JSONString];
    
    [GeneralFunc addStringPref:s forKey:@"lastOrderInfo"];
    
    
    TrackOrderViewController *v = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]]  instantiateViewControllerWithIdentifier:@"TrackOrderViewController"];
    v.passedOrder=o;
    
    
    [self presentViewController:v animated:YES completion:^{
        
        NSLog(@"complete");
        [self.navigationController popToRootViewControllerAnimated:YES];
        
         
    }];
    
    


}



-(void) showExtraJDDialog:(int)_bookingType popupType:(int)popupType{

   /*  CreateOrderPopup *createOrderPopup = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]]  instantiateViewControllerWithIdentifier:@"CreateOrderPopup"];
    
    createOrderPopup.delegate = self;
    createOrderPopup.popupType = popupType;
    
    bookingType = _bookingType;
    [self presentViewController:createOrderPopup animated:YES completion:^{
    }];
*/
    
    CreateOrderPopup *createOrderPopup = [[NSBundle mainBundle] loadNibNamed:@"CreateOrderPopup" owner:nil options:nil].lastObject;
    createOrderPopup.frame = self.view.bounds;
    createOrderPopup.alpha = 0;

    createOrderPopup.delegate = self;
    createOrderPopup.popupType = popupType;
    bookingType = _bookingType;

    [self.view addSubview:createOrderPopup];
    [createOrderPopup fadeInWithDuration:1];
    
   /* UIAlertController * extra = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"", nil) message:NSLocalizedString(@"An extra 0.5 JD will be added to the trip fees", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    [extra addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [extra dismissViewControllerAnimated:YES completion:nil ];
        
        
    }]];
    
    
    [extra addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Confirm", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if(type==1){// now
        
            [self doBookNow];
            
        }else if(type ==2){// later
            [self doBookLater];
        }
        
        
    }]];
    
    
    [self presentViewController:extra animated:YES completion:nil];
    */
    


}

-(void)showCallAlert{
    
    dispatch_async(dispatch_get_main_queue(), ^{

    CallPopup *popup = [[NSBundle mainBundle] loadNibNamed:@"CallPopup" owner:nil options:nil].lastObject;
    popup.frame = self.view.bounds;
    popup.alpha = 0;
    [self.view addSubview:popup];
    
        [popup fadeInWithDuration:1];
    });
    
}

-(void)openBookingScreen{
    [GeneralFunc addStringPref:[NSString stringWithFormat:@"%d",0] forKey:@"lastOrderTime"];
        if(bookingType==1){// now
            
            [self doBookNow];
            
        }else if(bookingType ==2){// later
            [self doBookLater];
        }

}

#pragma mark - text

-(void)textFieldDidBeginEditing:(UITextField *)textField{

    [textField endEditing:YES];
    selectedUF = textField;
    
    
    [GeneralFunc addIntPref:0 forKey:@"Reset"];

    SearchLocationViewController *search = [SearchLocationViewController new];
    search.delegate = self;
    
    if(textField==_etFrom){
        search.seachType = 1;
    }else {
        search.seachType =2;
    }
    
    
    UINavigationController * searchNav = [UINavigationController new];
    searchNav.navigationBarHidden = true;
    [searchNav pushViewController:search animated:YES];
    
    [self presentViewController:searchNav animated:YES completion:nil];
 

}



-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    if(textField==_etFrom){
        fromPlace =nil;
        mustUpdateSource = false;
        [self showSelectFromMapPin];
        
        _lblMsg.text = NSLocalizedString(@"Please select pick up location", nil);
    }else {
        destinationMarker.map=nil ;
        destinationMarker =nil;
        toPlace =nil;
    }
    
    polyline.map=nil;
    polyline=nil;
    
    
    textField.text = @"";
    [textField resignFirstResponder];
    return NO;
}




#pragma mark- Http 

-(void)HTTPJustStart{
}

-(void)HTTPInProgress{

}
-(void)HTTPDone:(NSData *)data response:(NSURLResponse *)res error:(NSError *)error{
    
}
-(void)HTTPDone:(NSData *)data response:(NSURLResponse *)res error:(NSError *)error forID:(int)ID{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
      [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        [GeneralFunc hideLoading:self];
    
  
    
    if(ID==100){
        
        
        
        
        if(data ){
            
            NSInteger orderID = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] integerValue];
            if(orderID>0){
                [GeneralFunc addIntPref:(int)orderID  forKey:@"latestOrder"];
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self addSelectedLocMarker];
                    
                    NSString *url = [NSString stringWithFormat:@"%@&point=%@,%@&point=%@,%@",URLsGetPath,selected.lat,selected.lng,@"31.975937194733",@"35.95500488281249"];
                    http = [[HTTPDelegate alloc] initWithURL:url];
                    
                    http.delegate=self;
                    [http startHttp:101];
                    
                });
                
                
                
            }else {
                
                NSMutableDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                if([dic objectForKey:@"Message"]){
                    
                    // show message
                }
                
                
            }
        }
        
        
        
        
    }else if(ID==101 ){
        
        
        GMSPolyline *polygon = [[GMSPolyline alloc] init];
        polygon.path = [GMSPath pathFromEncodedPath:[JSONParser decodedpath:data]];
        polygon.strokeColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:1.0];
        polygon.strokeWidth = 2;
        polygon.map = _map;
        
        
    }else if(ID==90){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(data){
           _etFrom.text =[JSONParser ParsePlaceName:data];
                
                if(!fromPlace){
                    fromPlace = [Place new];
                }
                
                fromPlace.lat = [NSString stringWithFormat:@"%lf",sourceMarker.position.latitude];
                fromPlace.lng = [NSString stringWithFormat:@"%lf",sourceMarker.position.longitude];
                fromPlace.nameAr = [JSONParser ParsePlaceName:data];
                fromPlace.address =  [JSONParser getPlaceAdress:data];
                [self updateUserLocation:[JSONParser getStreetName:data]];

           /*     fromPin.map = nil;
                if(!fromPin){
                    fromPin = [GMSMarker markerWithPosition: CLLocationCoordinate2DMake([fromPlace.lat doubleValue], [fromPlace.lng doubleValue])];
                    
                }

                fromPin.position = CLLocationCoordinate2DMake([fromPlace.lat doubleValue], [fromPlace.lng doubleValue]);
                fromPin.icon = [UIImage imageNamed:@"gpin.png"];
                fromPin.appearAnimation =kGMSMarkerAnimationPop;
                fromPin.map = _map;*/

            
                //** draw line when source and destenation was selected
                if (sourceMarker && destinationMarker){
                    [self updateMapCam];
                }
                
                _viBookNow.hidden =NO;
                _viBookLater.hidden =NO;
                _lblMsg.text = NSLocalizedString(@"Please select drop off location", nil);
                
            }else{
              //  fromPin =nil;
                _etFrom.text=@"";
                _viBookNow.hidden =YES;
                _viBookLater.hidden =YES;
                
            }
            
        });
        
        
        
    }else if (ID==1001){
    
        
        isHttpTaxiesWorking =false;
        [availabeCars removeAllObjects];
        if(data){
            
            NSArray * cars  = [JSONParser ParseNearby:data];
            if (cars.count > 0){
                [self addCarsToMap:cars];
            //    Car *nearestCar = cars[0];
           //     [self updateNearestCarDuration:nearestCar.duration];
//                if (!_noCarsAlert.isHidden){
//                    [self hideNoCarsAlert];}
            }
            else{
                self.nearestCarDurationLabel.text = @"";
                [self removeAllCarsFromMap];
             }
            
            
        }

    }else if(ID==1002){
    
        if(!data) return ;
        
        NSString *olderTimes = [GeneralFunc getPrefStringForKey:@"lastOrderTime_later"];
        if(!olderTimes) olderTimes=@"0";
        
        [GeneralFunc addStringPref:[NSString stringWithFormat:@"%@,%ld",olderTimes, (long)[NSDate date].timeIntervalSince1970] forKey:@"lastOrderTime_later"];
        
            UIAlertController * c = [GeneralFunc showAlertDialogWithTitle:@"" Message:NSLocalizedString(@"Your order sent, our call center agent will call you before your taxi time. Thank you", @"")];
        
        [c addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *ac ){
            
             [self.navigationController popToRootViewControllerAnimated:YES];
            
        }]];
        
            [self presentViewController:c animated:YES completion:nil];
        
        
        destinationMarker.map =nil ;
        destinationMarker =nil;
        _etTo.text=@"";
        _etFrom.text=@"";
        _viBookNow.hidden=YES;
        _viBookLater.hidden=YES;
        polyline.map =nil;
        
        [self updateSourcePinToCurrentLocation];

    } else  if( ID ==2){ // draw Path
        
        
   
        [self drawLine: [JSONParser decodedpath:data]];
        NSDictionary *d = [JSONParser decodedInfo:data];
        _lblMsg.text = (d)?[NSString stringWithFormat:@"%@ %.2lf %@ %@", NSLocalizedString(@"Approximate Distance: ", nil) , [d[@"dis"] doubleValue]/1000 , NSLocalizedString(@"KM", ""), [NSString stringWithFormat:@"%@ %ld %@",NSLocalizedString(@"Estimated Time:", ""),[d[@"time"] integerValue]/60000,NSLocalizedString(@"Minutes", "")]]:@" ";
        
//        _lblMsg.text = (d)?[NSString stringWithFormat:@"%@ %ld %@",NSLocalizedString(@"Estimated Time:", ""),[d[@"time"] integerValue]/6000,NSLocalizedString(@"Minutes", "")]:@"";
        
        
    }
    
        
    });
    
}


-(void) addSelectedLocMarker{
    
    [_map clear];
    
    _map.trafficEnabled = true;
    GMSMarker *marker=[[GMSMarker alloc]init];
    marker.position=CLLocationCoordinate2DMake([selected.lat doubleValue], [selected.lng doubleValue]);
    marker.icon=  [UIImage imageNamed:@"pin_m.png"]  ;
    marker.groundAnchor=CGPointMake(0.5,0.5);
    marker.map=_map;
}

 -(void)addCarsToMap:(NSArray*)cars{
     dispatch_async(dispatch_get_main_queue(), ^{
         
         Car *nearestCar = cars[0];
         [self updateNearesCarDuration:nearestCar.duration];
         
         int maxCars =10;
         
         availabeCars = [cars mutableCopy];
         
         [self removeAllCarsFromMap];
          for (Car *c in cars) {
             GMSMarker *marker=[[GMSMarker alloc]init];
             marker.position=CLLocationCoordinate2DMake([c.Latitude doubleValue], [c.Longitude doubleValue]);
          //   marker.rotation=  [c.Angle floatValue];
              marker.groundAnchor = CGPointMake(0.5, 0.5);
              
              
              // Use Car marker view for the nearest 3 cars
             if (carsMarksers.count < 3){
                 CarMarker *iconView = [[[NSBundle mainBundle] loadNibNamed:@"CarMarker" owner:self options:nil] objectAtIndex:0];
                 
                 [iconView fillCarData:c];
                 marker.iconView = iconView  ;
             }
             else{
                 marker.icon = [UIImage imageNamed:@"car_marker_icon"];
             }
              
             [marker setFlat:true];
             marker.map=_map;
              
             [carsMarksers addObject:marker];
             
             if(maxCars-- <1) break;
         }
         
     });
    
}

-(void)updateNearesCarDuration:(NSString*)duration{
    self.nearestCarDurationLabel.text = [self getDurationText:duration];
}
    
    
-(void)removeAllCarsFromMap{
    if(carsMarksers.count>0){
        
        for(GMSMarker *c in carsMarksers){
            c.map = nil;
        }
        
    }
    [carsMarksers removeAllObjects];

}

-(void)updateUserLocation:(NSString*)location{
    dispatch_async(dispatch_get_main_queue(), ^{
             userMarkerView.userName.text = [GeneralFunc getPrefStringForKey:@"NameEn"];
    });
    
    
}

-(void)hideNoCarsAlert{
    dispatch_async(dispatch_get_main_queue(), ^{
        
             _alertTopSpace.constant = -1*_noCarsAlert.frame.size.height;
            
            [UIView animateWithDuration:0.5 animations:^{
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                [_noCarsAlert setHidden:true];
            }];
     });
}

-(void)showNoCarsAlert{
    
    if (_noCarsAlert.isHidden){
        [_noCarsAlert setHidden:false];
        
        _alertTopSpace.constant = 0;
        
        [UIView animateWithDuration:0.5 animations:^{
            [self.view layoutIfNeeded];
        }];
    }

    
}

#pragma mark - searchViewControllerDidReturnPlace

-(void)searchViewControllerDidReturnPlace:(Place *)place{

    polyline.map =nil;
    polyline = nil;
    NSLog(@"%@",place);
    if([[NSNull null] isEqual:place.lat]|| [[NSNull null] isEqual:place.lng]||[[NSNull null] isEqual:place.nameAr]){
    
        return;
    }
    
    selectedUF.text = place.nameAr;
    if(selectedUF==_etFrom){
    
        fromPlace = place;
        
        _viBookNow.hidden =NO;
        _viBookLater.hidden =NO;
        
        _lblMsg.text = NSLocalizedString(@"Please select drop off location", nil);
        CLLocationCoordinate2D selecedLocaition = CLLocationCoordinate2DMake([place.lat doubleValue], [place.lng doubleValue]);
        
        sourceMarker.map = nil;
        sourceMarker = nil;

        [self setSourceLocation:selecedLocaition];

        
    }else{
    
        toPlace =place;
        destinationMarker.map=nil;
        destinationMarker=nil ;
        destinationMarker = [GMSMarker markerWithPosition: CLLocationCoordinate2DMake([place.lat doubleValue], [place.lng doubleValue])];
        destinationMarker.icon = [UIImage imageNamed:@"destenation_pin"];
        destinationMarker.map = _map;
    
    }
    
    
    [self updateMapCam];
    
    

}
    
 
#pragma mark - cam map
-(void) updateMapCam{

    
    GMSCameraPosition * cm = nil;
    
    if(destinationMarker){
         cm = [GMSCameraPosition cameraWithLatitude:destinationMarker.position.latitude
                                                            longitude:destinationMarker.position.longitude
                                                                 zoom:18];
        
        _lblMsg.text = NSLocalizedString(@"Please select drop off location", nil);
        
    
    }else if (sourceMarker){
    
        cm = [GMSCameraPosition cameraWithLatitude:sourceMarker.position.latitude
                                         longitude:sourceMarker.position.longitude
                                              zoom:18];
        
    _map.camera =cm;
        
    }
    
    if(destinationMarker && sourceMarker && !polyline ){
    
        
          NSString * url = [NSString stringWithFormat:@"%@&point=%lf,%lf&point=%lf,%lf",URLsGetPath,sourceMarker.position.latitude,sourceMarker.position.longitude, destinationMarker.position.latitude,destinationMarker.position.longitude ];
        
        findPath = [[HTTPDelegate alloc] initWithURL:url];
        
        findPath.delegate = self;
        [findPath startHttp:2];
        
        _lblMsg.text = NSLocalizedString(@"Loading info ...", nil);
        
        
    }
    
    
    if (polyline){
        GMSCoordinateBounds *b = [[GMSCoordinateBounds alloc] initWithPath:polyline.path];
        
        GMSCameraUpdate *up =  [GMSCameraUpdate fitBounds:b];//[[GMSCoordinateBounds alloc] initWithCoordinate:CarMarker.position coordinate:UserMarker.position] withPadding:20];
        
        [_map animateWithCameraUpdate:up];
        
    }
    
}

-(BOOL)didTapMyLocationButtonForMapView:(GMSMapView *)mapView{
 
   
    [self setSourceLocation:CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude)];
    [self updateMapCam];
    
    return true;

}

-(void) drawLine:(NSString *) path{
    
    if(!path) return;
    @try {
        
        if(polyline){
            polyline.map = nil;
        }
        
        _map.trafficEnabled = false;

        polyline = [[GMSPolyline alloc] init];
        polyline.path = [GMSPath pathFromEncodedPath:path];
        polyline.strokeColor = [UIColor colorWithRed:65.0 green:134.0 blue:176.0 alpha:1.0];
        polyline.strokeWidth = 2;
        
        polyline.map = _map;
        
        [self updateMapCam];
        
      
        
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
}

#pragma mark - time, date 

-(void)timePickerDialog:(MDTimePickerDialog *)timePickerDialog didSelectHour:(NSInteger)hour andMinute:(NSInteger)minute{

    
    
    NSDate *oldDate = selectedLaterDate; // Or however you get it.
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [calendar components:unitFlags fromDate:oldDate];
    comps.hour   = hour;
    comps.minute = minute;
    comps.second = 0;
    NSDate *newDate = [calendar dateFromComponents:comps];
    
    NSDate *date1 = [NSDate date];
    
    int dateCompare = [GeneralFunc compareDates:date1 date2:newDate];
    
    if( dateCompare < 1){
    
        // this is correct
        selectedLaterDate = newDate;
        
        
        
        o = [Order new];
        o.AppUserID = [GeneralFunc getPrefStringForKey:@"userid"];
        
        if(fromPlace){
            o.SAddress = _etFrom.text;
            o.SLat = fromPlace.lat;
            o.SLng = fromPlace.lng;
        }
        
        if(toPlace){
            o.EAddress = _etTo.text;
            o.ELat = fromPlace.lat;
            o.ELng = fromPlace.lng;
        }
        
        
        o.PickupTime  =[GeneralFunc getDateTimeAsStringForServer:newDate];
        o.Status =@"4";
        o.DestinationCategory = [NSString stringWithFormat:@"%@" , @"3"];
        
        
        
        int pref = 0;
        pref +=16;
        
        o.Preferences = [NSString stringWithFormat:@"%d", pref];
        NSString *s = [o JSONString];
        
        [GeneralFunc addStringPref:s forKey:@"lastOrderInfo"];
        
        NSString * r = [[FIRInstanceID instanceID] token];
       httpBookLater = [[HTTPDelegate alloc] initPOSTWithURL:[NSString stringWithFormat:@"%@%@customerName=%@&customerPhone=%@&customerGCM=%@", URLsMainURL, URLsNewOrder ,  [GeneralFunc urlencode:  [GeneralFunc getPrefStringForKey:@"NameAR"] ],[GeneralFunc urlencode: [GeneralFunc getPrefStringForKey:@"Phone"]],[GeneralFunc urlencode:  r] ] body:s];
        
        httpBookLater.delegate = self;
        [httpBookLater startHttp:1002];
        [GeneralFunc showLoading:self];
        
        
    
    }else{
    
        UIAlertController * error  = [GeneralFunc showAlertDialogWithTitle:NSLocalizedString(@"", @"") Message:NSLocalizedString(@"Time cannot be in the past", @"")];
        
        
        [error addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [error dismissViewControllerAnimated:YES completion:nil ];
        }]];
        
        [self presentViewController:error animated:YES completion:nil];
        
         
    
    }
    
    


}


-(void)datePickerDialogDidSelectDate:(NSDate *)date{
    
    MDTimePickerDialog *timePicker = [[MDTimePickerDialog alloc] init];
    timePicker.delegate = self;
    timePicker.theme = MDTimePickerThemeDark;
    
    [timePicker show];
    
    selectedLaterDate = date;
    
}



#pragma mark - fcm receiver 

/*
 
 Message Types
 AcceptOrder = 3,
 CancelOrder = 5,
 Pickup = 6,
 Tripend = 7,
 Arriving = 9,
 Arrived = 10,
 DriverLocation = 11
 */

-(void)MessageReceived:(NSDictionary *)msg{
    
    
    if(msg[@"id"]){
        
        switch ([msg [@"type"] integerValue]) {
           case 10 :{
               
                
            }break;
                
              
                
            default:
                break;
        }
        
        
    }
    
    
    
}

- (IBAction)shownearestTaxiesList{
    
    if (isListOpend){
        isListOpend = false;
        _bottomSpaceForListView.constant = -150;
    }
    else{
        isListOpend = true;
        // show max 3 cells
        if (carsMarksers.count<3){
            _bottomSpaceForListView.constant += carsMarksers.count*50;
        }
        else{
            _bottomSpaceForListView.constant = 0;
        }

    }
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
     }];

    [_nearestCarsList reloadData];
 
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    NearestCarsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NearestCarsCell"];
    if (!cell){
        cell = (NearestCarsCell*) [[NSBundle mainBundle] loadNibNamed:@"NearestCarsCell" owner:nil options:nil].lastObject;
    }
    
    Car *car = availabeCars[indexPath.row];
    
    cell.driverName.text = [NSString stringWithFormat:@"%@ %@" ,NSLocalizedString(@"captain",nil),car.driverName];
    cell.duration.text = [self getDurationText:car.duration];
 
    cell.carType.text = car.carType;

    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return availabeCars.count;
}


-(NSString*)getDurationText:(NSString*)duration{
    if ([duration isEqualToString:@"0"]){
        return  NSLocalizedString(@"less_than_minute",nil);
    }
    else{
        return [NSString stringWithFormat:@" %@ %@",duration,NSLocalizedString(@"min",nil)];
    }
    
}

 

@end

//
//  Car.h
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/22/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Car : NSObject



@property (strong , nonatomic) NSString * GPSTime ;
@property (strong , nonatomic) NSString * Longitude ;
@property (strong , nonatomic) NSString * Latitude ;
@property (strong , nonatomic) NSString * Speed ;
@property (strong , nonatomic) NSString * Angle ;
@property (strong , nonatomic) NSString * VehicleIGN ;
@property (strong , nonatomic) NSString * AssociatedVehicleID ;
@property (strong , nonatomic) NSString * AddressAr ;
@property (strong , nonatomic) NSString * PlateNumber ;
@property (strong , nonatomic) NSString * Driver ;
@property (strong , nonatomic) NSString * Phone ;
@property (strong , nonatomic) NSString * Picture ;
@property (strong , nonatomic) NSString * NearBy ;
@property (strong , nonatomic) NSString * Distance ;
@property (strong , nonatomic) NSString * duration ;
@property (strong , nonatomic) NSString * driverName ;
@property (strong , nonatomic) NSString * carType ;
@property  int driverRate ;
    
@property float durationInMillSec;

@end

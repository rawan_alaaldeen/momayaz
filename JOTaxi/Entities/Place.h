//
//  Place.h
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/16/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Place : NSObject

@property (strong , nonatomic) NSString * nameAr ;
@property (strong , nonatomic) NSString * nameEN ;
@property (strong , nonatomic) NSString * stAr ;
@property (strong , nonatomic) NSString * stEN ;
@property (strong , nonatomic) NSString * BNum ;

@property (strong , nonatomic) NSString * lat ;
@property (strong , nonatomic) NSString * lng ;
@property (strong , nonatomic) NSString * angle ;
@property (strong , nonatomic) NSString * TypeAr;
@property (strong , nonatomic) NSString * TypeEn;
@property (strong , nonatomic) NSString * address;


@property (nonatomic) BOOL isFav;

@property (nonatomic) long ID;



@end

//
//  Place.h
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/16/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FavPlace : NSObject

@property (strong , nonatomic) NSString * UserID ;
@property (strong , nonatomic) NSString * Latitude ;
@property (strong , nonatomic) NSString * AddressType ;
@property (strong , nonatomic) NSString * AddressName ;
@property (strong , nonatomic) NSString * Longitude ;

@property (strong , nonatomic) NSString * StreetName ;
@property (nonatomic) long ID ;


@end


//[0]	(null)	@"UserID" : (long)2
//[1]	(null)	@"Latitude" : (double)31.9852
//[2]	(null)	@"AddressType" : @"تكنولوجيا"
//[3]	(null)	@"AddressName" : @"العامة للحاسبات والإلكترونيات"
//[4]	(null)	@"Longitude" : (double)35.8821
//[5]	(null)	@"StreetName" : @"شارع وصفي التل"
//[6]	(null)	@"ID" : (long)180

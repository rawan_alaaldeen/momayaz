//
//  User.h
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/17/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

//UserID: 1,
//NameAR: "arr",
//NameEn: "121",
//Password: "",
//Email: "123@12.11",
//Phone: "111",
//Gender: 1,
//Verified: 1,
//SignupDate: "2016-08-17T01:28:58.68"


@property (strong, atomic) NSString * userID;
@property (strong, atomic) NSString * NameAR;
@property (strong, atomic) NSString * NameEn;
@property (strong, atomic) NSString * Password;
@property (strong, atomic) NSString * Email;
@property (strong, atomic) NSString * Phone;
@property (strong, atomic) NSString * Verified;
@property (strong, atomic) NSString * Gender;

 
@end

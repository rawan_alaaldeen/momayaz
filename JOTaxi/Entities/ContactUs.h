//
//  ContactUs.h
//  Momayaz
//
//  Created by Hammoudeh Alamri on 10/1/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContactUs : NSObject

@property(strong , atomic) NSString *email;
@property(strong , atomic) NSString *name;
@property(strong , atomic) NSString *body;
@property(strong , atomic) NSString *reason;
@property(strong , atomic) NSString *subject;
@property(strong , atomic) NSString *phone;
@property(strong , atomic) NSString *userID;


- (NSString *)JSONString; 

 
@end

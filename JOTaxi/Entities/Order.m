//
//  Order.m
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/18/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import "Order.h"
#import <objc/runtime.h>

@implementation Order


//@property(strong , atomic) NSString *OrderID;
//@property(strong , atomic) NSString *AppUserID;
//@property(strong , atomic) NSString *SLng;
//@property(strong , atomic) NSString *SLat;
//@property(strong , atomic) NSString *SAddress;
//@property(strong , atomic) NSString *SBuildingNo;
//@property(strong , atomic) NSString *ELng;
//@property(strong , atomic) NSString *EAddress;
//@property(strong , atomic) NSString *EBuildingNo;
//@property(strong , atomic) NSString *EGov;
//@property(strong , atomic) NSString *Note;
//@property(strong , atomic) NSString *Preferences;
//@property(strong , atomic) NSString *Status;
//@property(strong , atomic) NSDate *PickupTime;
//@property(strong , atomic) NSDate *DropOffTime;
//@property(strong , atomic) NSString *DestinationCategory;
//@property(strong , atomic) NSString *luggage;


//- (NSMutableDictionary *)toNSDictionary
//{
//    
//    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
//    [dictionary setValue:self.OrderID forKey:@"OrderID"];
//    [dictionary setValue:self.AppUserID forKey:@"AppUserID"];
//    [dictionary setValue:self.SLng forKey:@"SLng"];
//    [dictionary setValue:self.SLat forKey:@"SLng"];
//    [dictionary setValue:self.SAddress forKey:@"SLng"];
//    [dictionary setValue:self.SBuildingNo forKey:@"SLng"];
//    [dictionary setValue:self.ELng forKey:@"SLng"];
//    [dictionary setValue:self.El forKey:@"SLng"];
//    [dictionary setValue:self.SLng forKey:@"SLng"];
//    [dictionary setValue:self.SLng forKey:@"SLng"];
//    [dictionary setValue:self.SLng forKey:@"SLng"];
//    [dictionary setValue:self.SLng forKey:@"SLng"];
//    [dictionary setValue:self.SLng forKey:@"SLng"];
//    [dictionary setValue:self.SLng forKey:@"SLng"];
//    [dictionary setValue:self.SLng forKey:@"SLng"];
//    
//    return dictionary;
//}

- (NSDictionary *)dictionaryReflectFromAttributes
{
    @autoreleasepool
    {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        unsigned int count = 0;
        objc_property_t *attributes = class_copyPropertyList([self class], &count);
        objc_property_t property;
        NSString *key, *value;
        
        for (int i = 0; i < count; i++)
        {
            property = attributes[i];
            key = [NSString stringWithUTF8String:property_getName(property)];
            value = [self valueForKey:key];
            [dict setObject:(value ? value : @"") forKey:key];
        }
        
        free(attributes);
        attributes = nil;
        
        return dict;
    }
}


- (NSString *)JSONString
{
    NSDictionary *dict = [self dictionaryReflectFromAttributes];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    if (jsonData.length > 0 && !error)
    {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        return jsonString;
    }
    return nil;
}

@end

//
//  JourneyModel.swift
//  JOTaxi
//
//  Created by Rawan Alaeddin on 9/15/17.
//  Copyright © 2017 GCE. All rights reserved.
//

import UIKit
import SwiftyJSON

class JourneyModel {
 
    var duration: Int?
    var distance: Int?
    var driverRating: Int?
    var plateNumber: String?
    var carType: String?

    init(_ data:JSON) {
        
        duration = data["TripDuration"].intValue
        distance = data["TripDistance"].intValue
        driverRating = data["UserRating"].intValue
        plateNumber = data["PlateNumber"].stringValue
        carType = data["CarType"].stringValue
 
    }
    
    
}

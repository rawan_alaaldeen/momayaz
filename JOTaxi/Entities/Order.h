//
//  Order.h
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/18/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Order : NSObject


//public int OrderID;
//public int AppUserID;
//public double SLng;
//public double SLat;
//public string SAddress;
//public string SLandmark;
//public int SBuildingNo;
//public double ELng;
//public double ELat;
//public string EAddress;
//public string ELandmark;
//public int EBuildingNo;
//public string EGov;
//public string Note;
//public int Preferences; // Enum
//public int Status; // Order Status (1-5) check actionController for details
//public DateTime PickupTime;
//public DateTime DropOffTime;
//public int DestinationCategory;

@property(strong , atomic) NSString *OrderID;
@property(strong , atomic) NSString *AppUserID;
@property(strong , atomic) NSString *SLng;
@property(strong , atomic) NSString *SLat;
@property(strong , atomic) NSString *SAddress;
@property(strong , atomic) NSString *SLandmark;
@property(strong , atomic) NSString *SBuildingNo;
@property(strong , atomic) NSString *ELng;
@property(strong , atomic) NSString *ELat;
@property(strong , atomic) NSString *EAddress;
@property(strong , atomic) NSString *EBuildingNo;
@property(strong , atomic) NSString *EGov;
@property(strong , atomic) NSString *Note;
@property(strong , atomic) NSString *Preferences;
@property(strong , atomic) NSString *Status;
@property(strong , atomic) NSString *PickupTime;
@property(strong , atomic) NSString *DropOffTime;
@property(strong , atomic) NSString *DestinationCategory;
@property(strong , atomic) NSString *Luggage;
@property(strong , atomic) NSString *GCMTokenID;
@property(strong , atomic) NSString *ELandmark;


 


- (NSDictionary *)dictionaryReflectFromAttributes;
- (NSString *)JSONString;



@end

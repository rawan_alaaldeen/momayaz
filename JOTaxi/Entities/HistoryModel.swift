//
//  HistoryModel.swift
//  JOTaxi
//
//  Created by Rawan Alaeddin on 9/12/17.
//  Copyright © 2017 GCE. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation

class HistoryModel: NSObject {
    
    var orderID:Int?
    var creationDate:String?
    var sAddress:String?
    var eAddress:String?
    var driverName:String?
    var isSchaduled:Bool!
    var location:CLLocationCoordinate2D?
 
    init(data:JSON) {
        orderID = data["OrderID"].intValue
        sAddress = data["SAddress"].stringValue
        driverName = data["DriverName"].stringValue

        
        // get destenation address
        if data["EAddressTablet"] != ""{
            eAddress = data["EAddressTablet"] .stringValue
        }
        else{
            eAddress = data["EAddress"].stringValue
        }
        
        
        let dateAndTime = data["CreationDate"].stringValue
        if dateAndTime != ""{
        let split = dateAndTime.characters.split{$0 == "T"}.map(String.init)
            if split.count == 2 {
                creationDate = split[0]
                creationDate! += "  "+split[1]
            }
          }
        
        // get source location 
        
        let lng =  data["SLng"].doubleValue
        let lat = data["SLat"].doubleValue
        
        if lng != 0 && lat != 0  {
            location = CLLocationCoordinate2D(latitude: lat, longitude: lng)}

    }
    
    static func getArray(fromData jsonArray:JSON)->[HistoryModel]{
    
        var hisoryItems = [HistoryModel]()
        for jsonItem in jsonArray.arrayValue {
            let historyItem = HistoryModel(data: jsonItem)
            hisoryItems.append(historyItem)
            
        }
        
        return hisoryItems
    }
    
  
}

//
//  TaxiInfo.h
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/18/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TaxiInfo : NSObject


//GPSTime: "2016-08-18T01:22:48",
//Longitude: 35.92998,
//Latitude: 31.8698521,
//Speed: 0,
//Angle: 77,
//VehicleIGN: 1,
//AddressAr: "محافظة العاصمة عمان - منطقة خريبة السوق - جاوا الشمالي - شارع الوقار - سوبر ماركت السلفينى",
//PlateNumber: "70-25399",
//Driver: "Arena29",
//Phone: "",
//Picture: "Null",
//NearBy: true


@property (nonatomic, strong) NSString *GPSTime;
@property (nonatomic, strong) NSString *Longitude;
@property (nonatomic, strong) NSString *Latitude;
@property (nonatomic, strong) NSString *Speed;
@property (nonatomic, strong) NSString *Angle;
@property (nonatomic, strong) NSString *VehicleIGN;
@property (nonatomic, strong) NSString *AddressAr;
@property (nonatomic, strong) NSString *PlateNumber;
@property (nonatomic, strong) NSString *Driver;
@property (nonatomic, strong) NSString *Picture;
@property (nonatomic, strong) NSString *NearBy;
@property (nonatomic, strong) NSString *Phone;
@end

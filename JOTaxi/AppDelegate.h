//
//  AppDelegate.h
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/10/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
@import GoogleMaps;


#import "SignalRMan.h"
#import <Google/SignIn.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <UserNotifications/UserNotifications.h>
#import "MainViewController.h"


//#import "Momayaz-Swift.h"

@import FirebaseMessaging;
@import UserNotifications;
static MainViewController *RootMainViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate, GIDSignInDelegate,UNUserNotificationCenterDelegate,FIRMessagingDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

//@property (strong,nonatomic) SignalRHelper *sHelper;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


+(void) setDelegate:(id) dele;
+(void) removeDelegate;


@end




@protocol CMReceiver <NSObject>

-(void)MessageReceived:(NSDictionary *) msg;

@end


//
//  SelectFromMapViewController.m
//  JO Taxi
//
//  Created by Hammoudeh Alamri on 20/04/2017.
//  Copyright © 2017 iTeks. All rights reserved.
//

#import "SelectFromMapViewController.h"
#import "StylingHelper.h" 


@interface SelectFromMapViewController ()

@end

@implementation SelectFromMapViewController
{

    CLLocationManager *locationManager;
    Place *selected ;
    
    HTTPDelegate *http ;
    HTTPDelegate *httpLocation ;
    
    CLLocation *currentLocation;
    BOOL mapDraged;
    BOOL isUserDragging;
    BOOL isUserSelectPlace;

}


- (void)viewDidLoad {
    [super viewDidLoad];

    [self startLocationManager];
    selected = [Place new];
     
    _map.delegate =self;
   
    [self setPinIcon];
    [self setScreenTitle];
    
    [_btnSelect setTitle:NSLocalizedString(@"Set Location", nil) forState:UIControlStateNormal];

}

-(void)viewDidAppear:(BOOL)animated{
    
    
    _btnSelect.layer.borderWidth = 1;
    _btnSelect.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _btnCancel.layer.borderWidth = 1;
    _btnCancel.layer.borderColor = [UIColor whiteColor].CGColor;

    if(_parentController){
         [self.presentedViewController.parentViewController
         dismissViewControllerAnimated:YES
         completion:nil];
    }
}




- (IBAction)back:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
    
}
- (IBAction)setLocation:(id)sender {
    
    
    CGPoint mapCenter = [_map center];
    CLLocationCoordinate2D selectedLocation = [[_map projection] coordinateForPoint:mapCenter];
    
    isUserSelectPlace = true;
    [self loadThePlaceName:[NSString stringWithFormat:@"%lf",selectedLocation.latitude] lng:[NSString stringWithFormat:@"%lf",selectedLocation.longitude]];
    
}

-(void)palceDidSelecte{
    CGPoint mapCenter = [_map center];
    CLLocationCoordinate2D selectedLocation = [[_map projection] coordinateForPoint:mapCenter];
    
    selected.lat =[NSString stringWithFormat:@"%lf",selectedLocation.latitude];
    selected.lng =[NSString stringWithFormat:@"%lf",selectedLocation.longitude];
    
    [self.delegate searchViewControllerDidReturnPlace:selected];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)placeFailToGet{
    UIAlertController * al = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Wait",nil) message:NSLocalizedString(@"Can not set this location, please wait!",nil) preferredStyle:UIAlertControllerStyleActionSheet];
    
    [al addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", nil) style:UIAlertActionStyleCancel handler:nil]];
    
    [self presentViewController:al animated:YES completion:nil];

}

- (void)startLocationManager
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    
    [locationManager startUpdatingLocation];
    [locationManager requestWhenInUseAuthorization];
    
    
}



#pragma mark - Gmap
-(void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    
    
    if (isUserDragging){
        isUserDragging = false;
        CGPoint mapCenter = [_map center];
        mapCenter.y += _imgPin.frame.size.height/2 + 24;
        mapCenter.x += _imgPin.frame.size.width/2 + 11;
        CLLocationCoordinate2D selectedLocation = [[_map projection] coordinateForPoint:mapCenter];
        if(![GeneralFunc isLocationInJordan:selectedLocation]){
            _imgPin.image = [UIImage imageNamed:@"pin_outside"];
        }
        else{
            [self setPinIcon];
        }
        
        [self loadThePlaceName:[NSString stringWithFormat:@"%lf",selectedLocation.latitude] lng:[NSString stringWithFormat:@"%lf",selectedLocation.longitude]];
    }
    
}

-(void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture{
         isUserDragging = true;
    
}


-(void) loadThePlaceName:(NSString *) lat lng:(NSString *) lng{
    
    
    NSString *url =[NSString stringWithFormat:@"http://188.247.86.76/AddressingService/GetAddress.ashx?x=%@&y=%@", lng,lat];
    
    
    if(httpLocation) [httpLocation stopHttp];
    
    httpLocation = [[HTTPDelegate alloc] initWithURL:url];
    httpLocation.delegate=self;
    [httpLocation startHttp:90];
    
}


#pragma mark - location

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    if(!mapDraged){
        GMSCameraPosition *cm = [GMSCameraPosition cameraWithLatitude:newLocation.coordinate.latitude
                                                            longitude:newLocation.coordinate.longitude
                                                                 zoom:18];
        _map.camera =cm;
        
        mapDraged=true;
        
        [GeneralFunc addStringPref:[ NSString stringWithFormat:@"%f",newLocation.coordinate.latitude]  forKey:@"lat"];
        [GeneralFunc addStringPref:[ NSString stringWithFormat:@"%f",newLocation.coordinate.longitude] forKey:@"lng"];
    }
    
}



-(void) styleTheMap{
    
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSURL *styleUrl = [mainBundle URLForResource:@"googlemapstyle" withExtension:@"json"];
    NSError *error;
    
    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];
    
    if (!style) {
        NSLog(@"The style definition could not be loaded: %@", error);
    }
    
    _map.mapStyle = style;
    
    
}

-(void)setPinIcon{
    if(_seachType==1){
        _imgPin.image = [UIImage imageNamed:@"user_pin"];
    }else{
        _imgPin.image = [UIImage imageNamed:@"destenation_pin"];
    }
}

-(void)setScreenTitle{
    if(_seachType==1){
        _viewTitle.text = NSLocalizedString(@"pick_up_location", nil);
    }else{
        _viewTitle.text = NSLocalizedString(@"where_to_go", nil);
    }

}

#pragma mark - http delegate
-(void)HTTPJustStart{
    
    
    
    
}
-(void)HTTPInProgress{}
-(void)HTTPDone:(NSData *)data response:(NSURLResponse *)res error:(NSError *)error{
    
}
-(void)HTTPDone:(NSData *)data response:(NSURLResponse *)res error:(NSError *)error forID:(int)ID{
    
   
 if(ID==90){
        
    if (error){
             [self placeFailToGet];
             return;}
         
     else{
        dispatch_async(dispatch_get_main_queue(), ^{
            
            _etText.text =[JSONParser ParsePlaceName:data];
            selected.address  = [JSONParser getPlaceAdress:data];
            selected.nameAr = _etText.text;
            
            if(isUserSelectPlace){
                [self palceDidSelecte];
            }

        });}
         
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
 
@end

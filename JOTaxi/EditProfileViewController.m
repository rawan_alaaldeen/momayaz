//
//  EditProfileViewController.m
//  Momayaz
//
//  Created by Rawan Alaeddin on 8/11/17.
//  Copyright © 2017 GCE. All rights reserved.
//

#import "EditProfileViewController.h"
#import "User.h"
#import "Consts.h"
#import "JOTaxi-Swift.h"

@interface EditProfileViewController ()<FullPopupDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *stackViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *stackViewSmalliPhoneHeightConstrant;

@end

@import Firebase;
@import FirebaseMessaging;

@implementation EditProfileViewController
{
    
    HTTPDelegate *http;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if( [GeneralFunc getPrefStringForKey:@"Email"]){
        
        _emailTextField.text = [GeneralFunc getPrefStringForKey:@"Email"];
         _nameTextField.text = [GeneralFunc getPrefStringForKey:@"NameEn"];
         _phoneTextField.text = [GeneralFunc getPrefStringForKey:@"Phone"];
        // male 0 // femail 1
        if ([[GeneralFunc getPrefStringForKey:@"Gender"]  isEqualToString:@"0"]){
            [_genderSegment setSelectedSegmentIndex:0];
        }
        else{
            [_genderSegment setSelectedSegmentIndex:1];
        }
    }
    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self updateConstraint];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateConstraint{
    
    if ([GeneralFunc isSmallDevise]){
        _stackViewSmalliPhoneHeightConstrant.active = true;
        _stackViewHeight.active = false;
    }
    else{
        _stackViewSmalliPhoneHeightConstrant.active = false;
        _stackViewHeight.active = true;
        
    }
}

-(IBAction)submit{
    
    BOOL allIsWell= true;

    // validate email
    if(![GeneralFunc validateEmail:_emailTextField.text]){
        
         allIsWell= false;

        [GeneralFunc validateTextField:_emailTextField isValid:false];
        
        
    }else{
        [GeneralFunc validateTextField:_emailTextField isValid:true];
        
    }
    
    
    /// validate phone
    
    
    /// validate phone
    NSString *phoneNumber = _phoneTextField.text;
    if (![phoneNumber hasPrefix:@"0"]){
        phoneNumber = [NSString stringWithFormat:@"%@%@",@"0",phoneNumber];
    }
    
    if ([phoneNumber length] != 10 ){
        allIsWell =false;
        [GeneralFunc validateTextField:_phoneTextField isValid:false];
    }
    else{
        if(![GeneralFunc validatePhone:phoneNumber]){
            allIsWell =false;
            [GeneralFunc validateTextField:_phoneTextField isValid:false];
        }
        else{
            [GeneralFunc validateTextField:_phoneTextField isValid:true];
            
        }
        
    }
     
    
    /// validate name
    if([_nameTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length<1){
         allIsWell= false;

        [GeneralFunc validateTextField:_nameTextField isValid:false];
        
        
    }else{
        [GeneralFunc validateTextField:_nameTextField isValid:true];
        
    }


    if (allIsWell){
    User *user = [User new];
    
    user.NameEn = _nameTextField.text;
    user.Email = _emailTextField.text;
    user.Phone = phoneNumber;
    user.Gender = [NSString stringWithFormat:@"%li",(long)_genderSegment.selectedSegmentIndex];
    user.NameAR = [GeneralFunc getPrefStringForKey:@"NameAR"];
    user.userID = [GeneralFunc getPrefStringForKey:@"userid"];

     NSString * url = [NSString stringWithFormat:@"%@%@UserID=%@&NameAR=%@&NameEn=%@&Email=%@&GCMTokenID=%@&Phone=%@&Gender=%@", URLsMainURL ,@"User/UpdateUser?",user.userID,[GeneralFunc urlencode: user.NameAR],[GeneralFunc urlencode: user.NameEn] , user.Email  ,[[FIRInstanceID instanceID] token],user.Phone,user.Gender];

    http = [[HTTPDelegate alloc] initWithURL:url];

    
    http.delegate = self;
    [http startHttp:1];}
    
    
}

- (IBAction)goBack:(id)sender {
    
    [self dismissView];
    
}

-(void)dismissView{
    [self dismissViewControllerAnimated:false completion:nil];
}


#pragma mark - HTTP
-(void)HTTPInProgress{
}

-(void)HTTPJustStart{
    dispatch_async(dispatch_get_main_queue(), ^{

    [GeneralFunc showLoading:self];
    });

}

-(void)HTTPDone:(NSData *)data response:(NSURLResponse *)res error:(NSError *)error forID:(int)ID{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [GeneralFunc hideLoading:self] ;
    });

    
    if(data){
        
        NSString * s = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        if ([s isEqualToString:@"1"]){
             [GeneralFunc addStringPref:_nameTextField.text forKey:@"NameEn"];
            [GeneralFunc addStringPref:[NSString stringWithFormat:@"%li",(long)_genderSegment.selectedSegmentIndex] forKey:@"Gender"];
            [GeneralFunc addStringPref:_phoneTextField.text forKey:@"Phone"];
            [GeneralFunc addStringPref:_emailTextField.text forKey:@"Email"];

                // show sucess alert
            [self showAlert:NSLocalizedString(@"information_uploaded_successfully", @"")  ];
        }
        // show fiald alert
        else{
            [self showAlert:NSLocalizedString(@"Some error occur, please try again", @"")  ];
   
        }
        
    }
    
}

-(void)showAlert:(NSString*)message{
    
    dispatch_async(dispatch_get_main_queue(), ^{

    FullPopupView *fullPopupView = [[NSBundle mainBundle] loadNibNamed:@"FullPopupView" owner:nil options:nil].lastObject;
    fullPopupView.frame = self.view.bounds;
    fullPopupView.delegate = self;
    fullPopupView.popupType = FullPopupTypeEditProfile;
    
    fullPopupView.message = message;
    
    fullPopupView.alpha = 0;
    [self.view addSubview:fullPopupView];
    [fullPopupView fadeInWithDuration:1];
    });
    

    
  /* UIAlertController *alert = [GeneralFunc showAlertDialogWithTitle:@"" Message:message];
    
     [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"")  style:UIAlertActionStyleDefault handler:^(UIAlertAction *ac ){
        
         [self dismissView];
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];*/

}

-(void)dismissFullPopup:(enum FullPopupType)type{
    [self dismissView];
}

-(void)HTTPDone:(NSData *)data response:(NSURLResponse *)res error:(NSError *)error{
}


#pragma mark- UITestfield delegate methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == _nameTextField){
        [_emailTextField becomeFirstResponder];
    }
    else if  (textField == _emailTextField){
        [_phoneTextField becomeFirstResponder];
    }
    else{
        [textField resignFirstResponder];
    }
    return true;
}

@end

//
//  StylingHelper.h
//  JO Taxi
//
//  Created by Hammoudeh Alamri on 18/04/2017.
//  Copyright © 2017 iTeks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface StylingHelper : NSObject


+(void) addCornerRadius:(UIView *) view radius:(CGFloat) r;
+(void) addShadow:(UIView *)view radius:(CGFloat) r;
+(void) circlizeView:(UIView *)view;

@end

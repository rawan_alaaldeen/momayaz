//
//  AppHelper.swift
//  TrafficApp
//
//  Created by admin on 10/16/16.
//  Copyright © 2016 MIT. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation
import AVFoundation
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleMaps
import SystemConfiguration


private var xoAssociationKey: UInt8  = 0
private var xoAssociationKey1: UInt8  = 1


class AppHelper :NSObject{
    
 
    class func screenBounds()-> CGRect{
        return UIScreen.main.bounds
    }

    static func isThereInternetConnection() -> Bool{
         if  Reachability.isConnectedToNetwork() {
             return  true
        }
        else{
             return false
        }
    }

}



extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(a) / 255.0)
    }
}

extension UIViewController{
    
    func beginingY() -> CGFloat {
        
        let statusBarHeight = UIApplication.shared.statusBarFrame.size.height
        if self.navigationController == nil  {
            return  statusBarHeight
        }
        else {
            return self.navigationController!.navigationBar.frame.size.height + statusBarHeight
        }
    }
    
    func hideNavigationBar() {
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    func showNavigationBar() {
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
    
}
extension UIView {
func fadeIn(withDuration duration: TimeInterval = 1.0) {
    UIView.animate(withDuration: duration, animations: {
        self.alpha = 1.0
    })
}

/// Fade out a view with a duration
/// - Parameter duration: custom animation duration
    
func fadeOut(withDuration duration: TimeInterval = 1.0) {
    UIView.animate(withDuration: duration, animations: {
        self.alpha = 0.0
    })
}
}

public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags : SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
}



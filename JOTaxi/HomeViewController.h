//
//  MainViewController.h
//  JO Taxi
//
//  Created by Hammoudeh Alamri on 09/04/2017.
//  Copyright © 2017 iTeks. All rights reserved.
//

#import "BaseController.h"
@import GoogleMaps;
#import "MDDatePickerDialog.h"
#import "MDTimePickerDialog.h" 
#import "AppDelegate.h"
#import "NearestCarsCell.h"
#import "CreateOrderPopup.h"



@interface HomeViewController : BaseController <UITextFieldDelegate, HTTPDel,GMSMapViewDelegate, CLLocationManagerDelegate, SelectViewControllerDelegate , MDDatePickerDialogDelegate, MDTimePickerDialogDelegate,CMReceiver,UITableViewDataSource,CreateOrderPopupDelegate>

@property (weak, nonatomic) IBOutlet UIView *viBookLater;
@property (weak, nonatomic) IBOutlet UIView *viBookNow;
@property (weak, nonatomic) IBOutlet UILabel *lblMsg;

@property (weak, nonatomic) IBOutlet GMSMapView *map;
@property (weak, nonatomic) IBOutlet UIView *seachHolder;
@property (weak, nonatomic) IBOutlet UIView *bookBtnHolder;
@property (weak, nonatomic) IBOutlet UIView *confirmBtnHolder;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *confirmBtnBottom;

@property (weak, nonatomic) IBOutlet UIImageView *imgCenterPin;


@property (weak, nonatomic) IBOutlet UITextField *etFrom;
@property (weak, nonatomic) IBOutlet UITextField *etTo;

 @property (weak, nonatomic) IBOutlet UIButton *btnBookNow;
@property (weak, nonatomic) IBOutlet UIButton *btnBookLater;

@property (weak, nonatomic) IBOutlet UIView *noCarsAlert;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertTopSpace;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSpaceForListView;
@property (weak, nonatomic) IBOutlet UITableView *nearestCarsList;

    @property (weak, nonatomic) IBOutlet UILabel *nearestCarDurationLabel;

@end

//
//  SelectFromMapViewController.h
//  JO Taxi
//
//  Created by Hammoudeh Alamri on 20/04/2017.
//  Copyright © 2017 iTeks. All rights reserved.
//

#import "BaseController.h"
@import GoogleMaps;
#import "Place.h"  
#import "HTTPDelegate.h" 

@interface SelectFromMapViewController : BaseController<UITextFieldDelegate, HTTPDel,GMSMapViewDelegate, CLLocationManagerDelegate, SelectViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *viewTitle;

@property (nonatomic) int seachType; /** 1: from 2: to **/

@property (weak, nonatomic) IBOutlet UIImageView *imgPin;
@property (weak, nonatomic) IBOutlet UITextField *etText;
@property (weak, nonatomic) IBOutlet GMSMapView *map;
@property (weak, nonatomic) IBOutlet UIButton *btnSelect;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;

@property (nonatomic) id<SelectViewControllerDelegate> delegate;


@property (nonatomic, weak) UIViewController * parentController;
@end

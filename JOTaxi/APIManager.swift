//
//  APIManager.swift
//  JOTaxi
//
//  Created by Rawan Alaeddin on 9/12/17.
//  Copyright © 2017 GCE. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class APIManager: NSObject {
    
    
   static func get(path:String,completion:@escaping (_ success:Bool, _ result:JSON?)->Void){
        Alamofire.request(path, method: .get,parameters: nil).validate().responseJSON { response in
            
            switch response.result {
                
            case .success:
                
                guard let json = response.result.value else{
                    completion(false,nil)
                    return
                }
                
                let data  = JSON(json)
                completion(true,data)
                
                
            case .failure(let error):
                
                 completion(false,nil)
                
            }
            
        }

    }
    
    

}

//
//  Consts.h
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/17/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import <Foundation/Foundation.h>


#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


extern const NSString *URLsMainURL   ;
extern const NSString *URLsAddUser  ;
extern const NSString *URLsLogin;
extern const NSString *URLsChangePassword;
extern const NSString *URLsNewOrder;
extern const NSString *URLsGetPath;
extern const NSString *URLsNearby;
extern const NSString *URLsCancelOrder;
extern const NSString *URLsContactUS ;
extern const NSString *URLsRateUS ;
extern const NSString *URLsAddFav;
extern const NSString *URLsRemFav;
extern const NSString *URLsLoadFav;
extern const NSString *URLsLoadRecent;
extern const NSString *URLsSearchPlaceByBuilding;
extern const NSString *URLsSearchPlaceByName;

@interface Consts : NSObject

@end

//
//  SignalRMan.h
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/13/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SignalRChanged <NSObject>

@optional
-(void) signalRResponse:(NSString *) message;
-(void)tabletResponseToApp:(NSString *)status orderID:(NSString * )orderID   capID:(NSString * )capID;
-(void)newlocation:(NSString *)ID Time:(NSString *)Time lng:(NSString *)lng lat:(NSString *)lat angle:(NSString *)angle speed:(NSString *)speed ign:(NSString *)ign lbs:(NSString *)lbs address:(NSString *)address;
-(void)taxiTabletChangeStatusApp:(NSString *)orderID capID:(NSString *)capID orderStatus:(NSString *)orderStatus taxiStatus:(NSString *)taxiStatus;
-(void)untrackTaxiResponse:(NSString *)message;
-(void)trackTaxiResponse:(NSString *)message;
-(void)newlocation:(NSString *)data;


@end

@interface SignalRMan : NSObject

+(void) startConnection;
+(void) stopConnection;
+(SignalRMan *) GetConnection;
+(void)sendMessage:(NSString *)message forMethod:(NSString *) method;

+(void) setDelegate:(id) dele;
+(void) removeDelegate;



@end

//
//  SignalRMan.m
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/13/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import "SignalRMan.h"
#import "GeneralFunc.h"

//#import "SignalR.h"


//@import SwiftR;

/*
static SRConnection *connection ;
static SRHubConnection *hubConnection;
static SRHubProxy *hub;
*/

static SignalRMan * singlton;

static id <SignalRChanged> delegate ;
static NSString * connectionID;

@implementation SignalRMan


+(SignalRMan *) GetConnection {

   if(!singlton){
        singlton = [[SignalRMan alloc] init];
    }
    
   // [SwiftR setUseWKWebView:true];

    
    //SignalR * sr =  [[SignalR alloc]init] ;
    
   // [sr setConnectionID:@"sdf"];
    
    
    //jordanlocator.com:6789 //http://194.165.150.220:6789/
   // if(!hubConnection)
     //   hubConnection = [SRHubConnection connectionWithURLString:@"http://194.165.150.220:6789/" queryString:@{@"UID": [NSString stringWithFormat:@"%ld",[GeneralFunc getPrefIntForKey:@"userid"]]}];
    
    /* */
    
   //SSinglton= [SwiftR new]  ;
    

    
   /* [SSinglton setConnectionID:[NSString stringWithFormat:@"%ld",[GeneralFunc getPrefIntForKey:@"userid"]]];
    [SSinglton setQueryString:@{@"UID":[NSString stringWithFormat:@"%ld",[GeneralFunc getPrefIntForKey:@"userid"]]}];
   
    
    
    
    
    [SSinglton start];
    */
    
    return singlton;

}

+(void) setDelegate:(id) dele{
    delegate = dele;
}

+(void) removeDelegate{
    delegate=nil; 
}

+(void) startConnection{

/*
    if(hubConnection){
    hub = [hubConnection createHubProxy:@"servicehub"];
    
        [hub on:@"newLocation" perform:singlton selector:@selector(newlocation:)] ;
     //   [hub on:@"newLocation" perform:singlton selector:@selector(newlocation:Time:lng:lat:angle:speed:ign:lbs:address:)] ;

      //  [hub on:@"broadCast" perform:singlton selector:@selector(newlocation:Time:lng:lat:angle:speed:ign:lbs:address:)];
        [hub on:@"tabletResponseToApp" perform:singlton selector:@selector(tabletResponseToApp:orderID:capID:)];
        [hub on:@"taxiTabletChangeStatusApp" perform:singlton selector:@selector(taxiTabletChangeStatusApp:capID:orderStatus:taxiStatus:)];
        [hub on:@"trackTaxiResponse" perform:singlton selector:@selector(trackTaxiResponse:)];
        [hub on:@"untrackTaxiResponse" perform:singlton selector:@selector(untrackTaxiResponse:)];
        
        
        
    [hubConnection setStarted:^{
        
        connectionID =hubConnection.connectionId ;

        NSLog(@"Connection Started");
        
    }];
    [hubConnection setReceived:^(NSString *message) {
        NSLog(@"Connection Recieved Data: %@",message);
    }];
    [hubConnection setConnectionSlow:^{
        NSLog(@"Connection Slow");
    }];
    [hubConnection setReconnecting:^{
        NSLog(@"Connection Reconnecting");
    }];
    [hubConnection setReconnected:^{
        NSLog(@"Connection Reconnected");
    }];
    [hubConnection setReconnected:^{
            NSLog(@"Connection Reconnected");
    }];
    [hubConnection setClosed:^{
        NSLog(@"Connection Closed");
        NSLog(@"Closed IDDDD = %@", connectionID );
        
        if(connectionID){
        // I still need this connenction
            [hubConnection start];
            
        }
        
        

    }];
    [hubConnection setError:^(NSError *error) {
        NSLog(@"Connection Error %@",error);
        
             }];
        
       
        
        
     
    [hubConnection start];
    
     NSLog(@"IDDDD = %@", hubConnection.connectionId );
    [hubConnection stop];
        
    }*/
}


+(void)stopConnection{

   /* if(hubConnection){
        connectionID=nil; 
        [hubConnection disconnect];
    }
    */
    
}


+(void)sendMessage:(NSString *)message forMethod:(NSString *) method{
    // Print the message when it comes in
  /*
    if(hub)
        [hub invoke:method withArgs:[NSArray arrayWithObjects:message, nil] completionHandler:^(id respose ,NSError *er){
        
            NSLog(@"reeeeeeeesponse %@" , respose);
            
        }];
*/

}

-(void)untrackTaxiResponse:(NSString *)message {
    // Print the message when it comes in
    NSLog(@"untrackTaxiResponse %@",message);
    
    if([delegate respondsToSelector:@selector(untrackTaxiResponse:)]){
    
        [delegate untrackTaxiResponse:message];
    
    }
    
    /// send it to
}
-(void)trackTaxiResponse:(NSString *)message {
    // Print the message when it comes in
    NSLog(@"trackTaxiResponse %@",message);
    
    /// send it to
    
    if([delegate respondsToSelector:@selector(trackTaxiResponse:)]){
        
        [delegate trackTaxiResponse:message];
        
    }
    
}

-(void)tabletResponseToApp:(NSString *)status orderID:(NSString * )orderID   capID:(NSString * )capID  {
    // Print the message when it comes in
    NSLog(@"tabletResponseToApp %@ %@ %@ " ,status,orderID, capID);
    
    /// send it to
    
    if([delegate respondsToSelector:@selector(tabletResponseToApp:orderID:capID:)]){
        
        [delegate tabletResponseToApp:status orderID:orderID capID:capID];
        
    }
}

-(void)newlocation:(NSString *)data{
NSLog(@"newlocation %@",data);
    
    if([delegate respondsToSelector:@selector(newlocation:)]){
        
        [delegate newlocation:data];
        
    }

    
}
-(void)newlocation:(NSString *)ID Time:(NSString *)Time lng:(NSString *)lng lat:(NSString *)lat angle:(NSString *)angle speed:(NSString *)speed ign:(NSString *)ign lbs:(NSString *)lbs address:(NSString *)address {
    // Print the message when it comes in
    NSLog(@"newlocation %@",address);
    
    /// send it to
    
    if([delegate respondsToSelector:@selector(newlocation:Time:lng:lat:angle:speed:ign:lbs:address:)]){
        
        [delegate newlocation:ID Time:Time lng:lng lat:lat angle:angle speed:speed ign:ign lbs:lbs address:address];
        
    }
}



-(void)taxiTabletChangeStatusApp:(NSString *)orderID capID:(NSString *)capID orderStatus:(NSString *)orderStatus taxiStatus:(NSString *)taxiStatus {
    
    
    
    /// Taxi Status---------------------
    /// 1: out of order
    /// 2: on the way
    /// 3: busy
    /// 4: free
    /// Order Status--------------------
    /// 1: Assigned (dispatched)
    /// 2: Canceled
    /// 3: Closed (done) // I need it for rate
    /// 4: Open (Active)
    /// 5: Driver with customer on // stop track
    // Print the message when it comes in
    NSLog(@"taxiTabletChangeStatusApp %@",orderID);
    
    if([delegate respondsToSelector:@selector(newlocation:Time:lng:lat:angle:speed:ign:lbs:address:)]){
        
        [delegate taxiTabletChangeStatusApp:orderID capID:capID orderStatus:orderStatus taxiStatus:taxiStatus];
        
    }
    
     
}




@end

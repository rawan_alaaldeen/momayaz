    //
//  HTTPDelegate.m
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/16/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import "HTTPDelegate.h"

@implementation HTTPDelegate
{

    NSMutableURLRequest *request ;
    NSURLConnection *conn ;
    NSURLSession *session;
    NSURLSessionDataTask *dataTask;
    
    int ReqID ;
    
}


-(instancetype)initPOSTWithURL:(NSString *)url body:(NSString *) jsonBody{


    
    request = [[NSMutableURLRequest alloc] init];
    [request setHTTPBody: [jsonBody dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[jsonBody length]];

    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"1" forHTTPHeaderField:@"isApp"];
    
    
    session = [NSURLSession sharedSession];
    dataTask = [session dataTaskWithRequest:request
                          completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                              
                              
                              NSLog(@"res : %@", [[NSString alloc ] initWithData:data encoding:NSUTF8StringEncoding ]);
                              
                              if(ReqID<1){
                                  if ([self.delegate respondsToSelector:@selector(HTTPDone:response:error:)]) {
                                      
                                      
                                      [self.delegate HTTPDone:data response:response error:error];
                                      
                                  }
                                  
                              }else{
                                  
                                  if ([self.delegate respondsToSelector:@selector(HTTPDone:response:error:forID:)]) {
                                      [self.delegate HTTPDone:data response:response error:error forID:ReqID];
                                      
                                  }
                              }
                              
                              
                              
                          }];
    
    return self;


    
    
}

-(instancetype)initWithURL:(NSString *)url    {
    
    
   // url = [GernralFunc urlencode:url];
    NSLog(@"LINK>>> %@",url);

    request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url ]];
    
    session = [NSURLSession sharedSession];
    dataTask = [session dataTaskWithRequest:request
                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
  
                            
                            NSLog(@"res : %@", [[NSString alloc ] initWithData:data encoding:NSUTF8StringEncoding ]);
                            
                            if(ReqID<1){
                            if ([self.delegate respondsToSelector:@selector(HTTPDone:response:error:)]) {
                                
                                
                                [self.delegate HTTPDone:data response:response error:error];
                                
                            }
                                
                            }else{
                                
                                if ([self.delegate respondsToSelector:@selector(HTTPDone:response:error:forID:)]) {
                                    [self.delegate HTTPDone:data response:response error:error forID:ReqID];
                                    
                                }
                            }
                            
                            
                            
                }];
  
    return self;
}


-(void)startHttp:(int)ID{
    
    ReqID =ID;
    [self startHttp];
    
}


-(void) startHttp{
    
    if(self.delegate && dataTask){
        
        if ([self.delegate respondsToSelector:@selector(HTTPJustStart)]) {
            
            [self.delegate HTTPJustStart];
        }
        
       [dataTask resume];
    
     
  }
    
}


-(void) stopHttp{
    
    if(self.delegate && dataTask){
        
        [dataTask suspend];
        
        request =nil;
        dataTask=nil;
        session=nil;
        
        
    }
    
}






@end

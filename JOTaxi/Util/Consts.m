//
//  Consts.m
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/17/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import "Consts.h"

const NSString *URLsMainURL  = @"http://188.247.86.76:8890/API/";

//prodtction: @"http://194.165.150.220:8890/API/"

//@"http://194.165.150.220:9000/API/";//www.jordanlocator.com
//http://194.165.150.220
// @"http://192.168.100.37:9000/API/";//@"http://192.168.8.104:9000/API/";//@"http://www.jordanlocator.com/TaxiService/API/";

 const NSString *URLsAddUser = @"User/AddUser?";
const NSString *URLsLogin = @"User/GetUserData?";
const NSString *URLsChangePassword = @"User/ChangePassword?";
const NSString *URLsNewOrder = @"order/NewOrder?";
const NSString *URLsCancelOrder = @"order/CancelOrder?";
const NSString *URLsContactUS = @"action/ContactUS"; 
const NSString *URLsRateUS = @"order/RateOrder";
const NSString *URLsNearby = @"Location/NearBy?";
const NSString *URLsAddFav = @"Location/AddFavouriteLocation?";
const NSString *URLsRemFav = @"Location/RemoveFavouriteLocation?";
const NSString *URLsLoadFav = @"Location/GetFavouriteLocation?";
 const NSString *URLsLoadRecent = @"Location/GetUserRecentLocations?";


//http://194.165.150.220:8890/api/Location/GetUserRecentLocations?UserID=2&pageno=1&Type=1


const NSString *URLsSearchPlaceByBuilding =@"http://jordanlocator.com/Taxi/Taxi/LocationSearch.ashx?op=building";
const NSString *URLsSearchPlaceByName =@"http://188.247.86.76/JoTaxi/Taxi/LocationSearch.ashx?op=landmark";
const NSString *URLsGetPath =@"http://188.247.86.76:8989/route?type=json&instructions=false&locale=ar";
@implementation Consts

@end

//
//  JSONParser.h
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/17/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Place.h" 
#import "User.h"
#import "Car.h"
#import "Order.h" 
#import "TaxiInfo.h" 


@interface JSONParser : NSObject

+(NSObject * ) ParseLoginRes:(NSData *) data;
+(NSInteger ) ParseRegRes:(NSData *) data;

+ (NSArray *) ParsePlaceByName:(NSData *) data;
+ (NSArray *) ParsePlaceByBuilding:(NSData *) data;

+(NSArray * ) ParseNearby:(NSData *) data;

+ (NSString *) ParsePlaceName:(NSData *) data;
+ (NSString *) getPlaceAdress:(NSData *) data;
+ (NSString *)getStreetName:(NSData *) data;
+ (NSString *) decodedpath:(NSData *) data;
+(NSDictionary *) decodedInfo:(NSData *) data;
+ (NSArray *) ParseFavPlace:(NSData *) data;
+ (NSArray *) ParseRecentPlace:(NSData *) data;

+(Order*) ParseOrderFromJSON:(NSString* ) str;

+(TaxiInfo * ) ParseTaxiInfoFromJSON:(NSDictionary *) data ;


@end

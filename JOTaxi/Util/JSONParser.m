//
//  JSONParser.m
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/17/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import "JSONParser.h"
#import "FavPlace.h"

@implementation JSONParser



+(NSInteger ) ParseRegRes:(NSData *) data{
    
    NSString * re = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return [re integerValue];
    
}




+ (NSObject *) ParseLoginRes:(NSData *) data  {
    
    User * u = [User new];
   
    @try {
        
    NSMutableDictionary  *dic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
        if (!dic){
            return nil;}
        
     else if([dic objectForKey:@"NameAR"]){
    u.NameAR = dic[@"NameAR"];
    u.NameEn = dic[@"NameEn"];
    u.Email = dic[@"Email"];
    u.Phone = dic[@"Phone"];
    u.userID = dic[@"UserID"];
    u.Gender =dic[@"Gender"];
      return u;
   
    }else if([dic objectForKey:@"Message"]){
        
        return dic;
    
    }
        
     } @catch (NSException *exception) {
         return  nil;
         
    }
    
    
    
}


+ (NSString *) ParsePlaceName:(NSData *) data{

    if(!data) return @"";
    
    NSMutableDictionary * re = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSString *placeName = @"";
    if([re objectForKey:@"Governorate"] && ![[re objectForKey:@"Governorate"] isEqualToString:@"<null>"]){
    
        placeName = [NSString stringWithFormat:@" %@",[re objectForKey:@"Governorate"]];
 
    }
     if([re objectForKey:@"Landmark"] && [re objectForKey:@"Landmark"] != (id)[NSNull null]){
        placeName = [placeName stringByAppendingString:[NSString stringWithFormat:@", %@",[re objectForKey:@"Landmark"]]];
    }
    
    return placeName;
}

+ (NSString *)getStreetName:(NSData *) data{
    
    if(!data) return @"";
    
    NSString *street = @"";
    
    NSMutableDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    if([dic objectForKey:@"Street"] && [dic objectForKey:@"Street"] != nil && ![[dic objectForKey:@"Street"] isKindOfClass:[NSNull class]]){
        street = [dic objectForKey:@"Street"];

    }
    return street;
}


+ (NSString *) getPlaceAdress:(NSData *) data{
    
    if(!data) return @"";
    
    NSMutableDictionary * dic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    NSString *governorate = @"";
    NSString *district = @"";
    NSString *subDistrict = @"";
    NSString *street = @"";
    NSString *landmark = @"";
    
    if([dic objectForKey:@"Governorate"] && [dic objectForKey:@"Governorate"] != nil && ![[dic objectForKey:@"Governorate"] isKindOfClass:[NSNull class]]){
        governorate = [dic objectForKey:@"Governorate"];
    }
    if([dic objectForKey:@"District"] && [dic objectForKey:@"District"] != nil && ![[dic objectForKey:@"District"] isKindOfClass:[NSNull class]]){
        district = [dic objectForKey:@"District"];
    }
    if([dic objectForKey:@"SubDistrict"] && [dic objectForKey:@"SubDistrict"] != nil && ![[dic objectForKey:@"SubDistrict"] isKindOfClass:[NSNull class]]){
        subDistrict = [dic objectForKey:@"SubDistrict"];
    }
    if([dic objectForKey:@"Street"] && [dic objectForKey:@"Street"] != nil && ![[dic objectForKey:@"Street"] isKindOfClass:[NSNull class]]){
        street = [dic objectForKey:@"Street"];
    }
    if([dic objectForKey:@"Landmark"] && [dic objectForKey:@"Landmark"] != nil && ![[dic objectForKey:@"Landmark"] isKindOfClass:[NSNull class]]){
        landmark = [dic objectForKey:@"Landmark"];
    }
    
    return [NSString stringWithFormat:@"%@,%@,%@,%@,%@", governorate,district, subDistrict ,street,landmark];
        
 }

+ (NSString *) decodedpath:(NSData *) data{
    
    if(!data) return nil;
    
    NSMutableDictionary * re = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    if([re objectForKey:@"paths"]){
        
        return re[@"paths"][0][@"points"];
        
    }
    
    return nil;
}

+(NSDictionary *) decodedInfo:(NSData *) data{
    
    
    if(!data) return nil;
    
    NSMutableDictionary * re = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    if([re objectForKey:@"paths"]){
        return @{@"dis":re[@"paths"][0][@"distance"],@"time":re[@"paths"][0][@"time"] }  ;
    
    }
    
    return nil;
}






+ (NSArray *) ParseRecentPlace:(NSData *) data{
    
 
    
    NSMutableArray * arr = [NSMutableArray new];
    
    if(!data) return arr;
    @try {
        
        NSMutableDictionary * re = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        
        for (NSDictionary *d  in re) {
            
            if(!d[@"Lat"])continue;
            NSLog(@"%@",d[@"Lat"]);
            
            Place * p = [Place new];
            
            p.lat = d[@"Lat"] ;
            p.nameAr =d[@"Address"];
            p.lng =d[@"Long"];
            
            [arr addObject:p];
            
            
        }
        
        
    } @catch (NSException *exception) {
        
    } @finally {
        return arr;
        
    }
    
    
}



+ (NSArray *) ParseFavPlace:(NSData *) data{
    
   
    //[0]	(null)	@"UserID" : (long)2
    //[1]	(null)	@"Latitude" : (double)31.9852
    //[2]	(null)	@"AddressType" : @"تكنولوجيا"
    //[3]	(null)	@"AddressName" : @"العامة للحاسبات والإلكترونيات"
    //[4]	(null)	@"Longitude" : (double)35.8821
    //[5]	(null)	@"StreetName" : @"شارع وصفي التل"
    //[6]	(null)	@"ID" : (long)180

    
    NSMutableArray * arr = [NSMutableArray new];
    
    if(!data) return arr;
    @try {
        
        NSMutableDictionary * re = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        
        for (NSDictionary *d  in re) {
            
            if(!d[@"Latitude"])continue;
            NSLog(@"%@",d[@"Latitude"]);
            
            Place * p = [Place new];
            
            p.lat = d[@"Latitude"] ;
            p.TypeAr = d[@"AddressType"] ;
            p.nameAr =d[@"AddressName"];
            p.lng =d[@"Longitude"];
            p.stAr =d[@"StreetName"];
            
            p.ID = [d[@"ID"] longLongValue];
            
            [arr addObject:p];
            
            
        }
        
        
    } @catch (NSException *exception) {
        
    } @finally {
        return arr;
        
    }
    
    
}



+ (NSArray *) ParsePlaceByBuilding:(NSData *) data{

    
//Number: "5",
//StreetAr: "شارع احمد موسى هاكوز",
//StreetEn: "AHMAD MOUSA HAKOUZ ST",
//X: 35.8180347523,
//Y: 31.9497242326
    
    
    NSMutableArray * arr = [NSMutableArray new];
    
    if(!data) return arr;
    @try {
        
        NSMutableDictionary * re = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        
        for (NSDictionary *d  in re) {
            
            if(!d[@"Number"])continue;
            NSLog(@"%@",d[@"Number"]);
            
            Place * p = [Place new];
            
            p.BNum =  d[@"Number"] ;
            p.stAr = d[@"StreetAr"] ;
            p.stEN = d[@"StreetEn"] ;
            p.nameEN =d[@"NameEn"];
            p.TypeAr =d[@"TypeAr"];
            p.TypeEn =d[@"TypeEn"];
            
            p.lat= d[@"Y"];
            p.lng= d[@"X"];
            
            
            
            [arr addObject:p];
            
            
        }
        
        
    } @catch (NSException *exception) {
        
    } @finally {
        return arr;
        
    }
    

}


+ (NSArray *) ParsePlaceByName:(NSData *) data{
    
    
    NSMutableArray * arr = [NSMutableArray new];
    
     if(!data) return arr;
    @try {
        
        NSMutableDictionary * re = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        
        for (NSDictionary *d  in re) {
            
            if(!d[@"NameAr"])continue;
            NSLog(@"%@",d[@"NameAr"]);
            
            Place * p = [Place new];
            
            p.nameAr =  d[@"NameAr"] ;
            p.stAr = d[@"StreetAr"] ;
            p.stEN = d[@"StreetEn"] ;
            p.nameEN =d[@"NameEn"];
            p.TypeAr =d[@"TypeAr"];
             p.TypeEn =d[@"TypeEn"];
 
            p.lat= d[@"Y"];
            p.lng= d[@"X"];
            
            
            
            [arr addObject:p];
            
            
        }

        
    } @catch (NSException *exception) {
        
    } @finally {
        return arr;

    }
    
    
    
}



+(NSArray * ) ParseNearby:(NSData *) data {
    
    NSMutableArray * arr = [NSMutableArray new];
    
    if(!data) return arr;
    @try {
        
        NSMutableDictionary * re = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        for (NSDictionary *d  in re) {
            Car * p = [Car new];
            
            
            p.Longitude = d[@"Longitude"];
            p.Latitude = d[@"Latitude"];
            p.GPSTime= d[@"GPSTime"];
            p.Longitude= d[@"Longitude"];
            p.Latitude= d[@"Latitude"];
            p.Speed= d[@"Speed"];
            p.VehicleIGN= d[@"VehicleIGN"];
            p.AssociatedVehicleID= d[@"AssociatedVehicleID"];
            p.PlateNumber= d[@"PlateNumber"];
            p.AddressAr= d[@"AddressAr"];
            p.Driver= d[@"Driver"];
            p.Phone= d[@"Phone"];
            p.Picture= d[@"Picture"];
            p.NearBy = d[@"NearBy"];
            float distanseInM = [d[@"Distance"] floatValue];
            p.Distance =  [NSString stringWithFormat:@"%f %@", distanseInM/1000,NSLocalizedString(@"KM", @"")];
            p.Angle = d[@"Angle"];
            p.durationInMillSec = [d[@"Time"] floatValue];
            //* get duration in minutes
            int durationInMin = (int)(p.durationInMillSec/1000)/60;
            
            p.duration = [NSString stringWithFormat:@"%d %@",durationInMin,NSLocalizedString(@"min", @"")];
            p.driverName = d[@"DriverName"];
            p.driverRate = [d[@"UserRating"] intValue];
            p.carType = d[@"Type"];

            [arr addObject:p];
        
        }
        
        
        
    }  @catch (NSException *exception) {
        
    } @finally {
        return arr;
        
    }

    
   
    
}

+(TaxiInfo * ) ParseTaxiInfoFromJSON:(NSDictionary *) data {
    
    
    TaxiInfo * p = [TaxiInfo new];
   
    
    
        
        NSDictionary * d = data;
    NSLog(@"%@",  d[@"Address"]);
    
    @try {
        
        p.AddressAr = d[@"Address"];
        p.Driver=d[@"ID"];
        p.GPSTime= d[@"Time"];
        p.Longitude= d[@"Long"];
        p.Latitude= d[@"Lat"];
        p.Speed= d[@"Speed"];
        p.VehicleIGN= d[@"VehicleIGN"];
        p.PlateNumber= d[@"PlateNumber"];
        p.AddressAr= d[@"AddressAr"];
        p.NearBy= d[@"NearBy"];
         p.Angle= d[@"Angle"];
        
        

        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
        return p;
        
    
    
    
    
}


+(Order*) ParseOrderFromJSON:(NSString* ) str{
    
    Order *o = [Order new];
    
    NSDictionary *od = [NSJSONSerialization JSONObjectWithData:[str dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    
   
    
//    
//    @property(strong , atomic) NSString *OrderID;
//    @property(strong , atomic) NSString *AppUserID;
//    @property(strong , atomic) NSString *SLng;
//    @property(strong , atomic) NSString *SLat;
//    @property(strong , atomic) NSString *SAddress;
//    @property(strong , atomic) NSString *SLandmark;
//    @property(strong , atomic) NSString *SBuildingNo;
//    @property(strong , atomic) NSString *ELng;
//    @property(strong , atomic) NSString *ELat;
//    @property(strong , atomic) NSString *EAddress;
//    @property(strong , atomic) NSString *EBuildingNo;
//    @property(strong , atomic) NSString *EGov;
//    @property(strong , atomic) NSString *Note;
//    @property(strong , atomic) NSString *Preferences;
//    @property(strong , atomic) NSString *Status;
//    @property(strong , atomic) NSString *PickupTime;
//    @property(strong , atomic) NSString *DropOffTime;
//    @property(strong , atomic) NSString *DestinationCategory;
//    @property(strong , atomic) NSString *Luggage;

    
    o.AppUserID= od[@"AppUserID"];
     o.SLng= od[@"SLng"];
     o.SLat= od[@"SLat"];
     o.SAddress= od[@"SAddress"];
     o.SLandmark= od[@"SLandmark"];
     o.SBuildingNo= od[@"SBuildingNo"];
     o.ELng= od[@"ELng"];
    o.ELat= od[@"ELat"];
    o.EAddress= od[@"EAddress"];
    o.EGov= od[@"EGov"];
    o.Note= od[@"Note"];
    o.Preferences= od[@"Preferences"];
    o.Status= od[@"Status"];
        o.PickupTime= od[@"PickupTime"];
        o.DropOffTime= od[@"DropOffTime"];
        o.DestinationCategory= od[@"DestinationCategory"];
        o.Luggage= od[@"Luggage"];
    
    
    
    
    
    
    
    return o;

}



@end

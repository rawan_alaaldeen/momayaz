//
//  GernralFunc.m
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/15/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import "GeneralFunc.h"
#import <Google/SignIn.h>
#import <sys/utsname.h>

@implementation GeneralFunc

static UIAlertController *mainAlert;
static UIView *black ;
static UIActivityIndicatorView * mainInd;




+(int) compareDates:(NSDate *) d1 date2: (NSDate *)d2{

    if ([d1 compare:d2] == NSOrderedDescending) {
        NSLog(@"date1 is later than date2");
        return 1;
    } else if ([d1 compare:d2] == NSOrderedAscending) {
        NSLog(@"date1 is earlier than date2");
          return -1;
    } else {
        NSLog(@"dates are the same");
        return 0;
    }
    
    
}





+(BOOL) validateEmail:(NSString *) email{

    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
    
+(BOOL) validatePhone:(NSString *) phone{

    NSString *phoneRegex = @"^((\\+)|(0))[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phone];
}


+(UIAlertController *) showAlertDialogWithTitle:(NSString * )title Message:(NSString *) msg    {

    mainAlert=nil;
    
      mainAlert = [UIAlertController alertControllerWithTitle:title
                                                                     message:msg
                                                              preferredStyle:UIAlertControllerStyleAlert];
    
    mainAlert.view.tintColor = UIColorFromRGB(0XFEC40D);

    
    return mainAlert;
    
   
}

+(void) hideAlert{
    
    if(mainAlert){
        
        [mainAlert popoverPresentationController];
    }


}


+(void) showLoading:(UIViewController *) cont {

    CGRect pr = cont.view.frame;
    
    if(!black)
        black = [[UIView alloc] initWithFrame:pr];
    [black setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
    
    mainInd= [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((pr.size.width/2)-50, (pr.size.height/2)-50, 100, 100)];
    
    mainInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;

    mainInd.color = UIColorFromRGB(0XFEC40D);
    [mainInd startAnimating];
    
    mainInd.tag =999;
    black.tag=991;
    
    [black addSubview:mainInd];
    [cont.view addSubview:black];
    
    [cont.view setUserInteractionEnabled:false];
    
    if( cont.navigationController){
        
         cont.navigationItem.backBarButtonItem.enabled = false;
    }

}

+(void) hideLoading:(UIViewController *) cont {
    
    
    
    [mainInd removeFromSuperview];
    [black removeFromSuperview];
    
    [cont.view setUserInteractionEnabled:true];
    
    if( cont.navigationController){
        
          cont.navigationItem.backBarButtonItem.enabled = true;
     }
    
    
    
}

+(NSString*) deviceName
{
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    
    static NSDictionary* deviceNamesByCode = nil;
    
    if (!deviceNamesByCode) {
        
        deviceNamesByCode = @{@"i386"      :@"Simulator",
                              @"x86_64"    :@"Simulator",
                              @"iPod1,1"   :@"iPod Touch",        // (Original)
                              @"iPod2,1"   :@"iPod Touch",        // (Second Generation)
                              @"iPod3,1"   :@"iPod Touch",        // (Third Generation)
                              @"iPod4,1"   :@"iPod Touch",        // (Fourth Generation)
                              @"iPod7,1"   :@"iPod Touch",        // (6th Generation)
                              @"iPhone1,1" :@"iPhone",            // (Original)
                              @"iPhone1,2" :@"iPhone",            // (3G)
                              @"iPhone2,1" :@"iPhone",            // (3GS)
                              @"iPad1,1"   :@"iPad",              // (Original)
                              @"iPad2,1"   :@"iPad 2",            //
                              @"iPad3,1"   :@"iPad",              // (3rd Generation)
                              @"iPhone3,1" :@"iPhone 4",          // (GSM)
                              @"iPhone3,3" :@"iPhone 4",          // (CDMA/Verizon/Sprint)
                              @"iPhone4,1" :@"iPhone 4S",         //
                              @"iPhone5,1" :@"iPhone 5",          // (model A1428, AT&T/Canada)
                              @"iPhone5,2" :@"iPhone 5",          // (model A1429, everything else)
                              @"iPad3,4"   :@"iPad",              // (4th Generation)
                              @"iPad2,5"   :@"iPad Mini",         // (Original)
                              @"iPhone5,3" :@"iPhone 5c",         // (model A1456, A1532 | GSM)
                              @"iPhone5,4" :@"iPhone 5c",         // (model A1507, A1516, A1526 (China), A1529 | Global)
                              @"iPhone6,1" :@"iPhone 5s",         // (model A1433, A1533 | GSM)
                              @"iPhone6,2" :@"iPhone 5s",         // (model A1457, A1518, A1528 (China), A1530 | Global)
                              @"iPhone7,1" :@"iPhone 6 Plus",     //
                              @"iPhone7,2" :@"iPhone 6",          //
                              @"iPhone8,1" :@"iPhone 6S",         //
                              @"iPhone8,2" :@"iPhone 6S Plus",    //
                              @"iPhone8,4" :@"iPhone SE",         //
                              @"iPhone9,1" :@"iPhone 7",          //
                              @"iPhone9,3" :@"iPhone 7",          //
                              @"iPhone9,2" :@"iPhone 7 Plus",     //
                              @"iPhone9,4" :@"iPhone 7 Plus",     //
                              @"iPhone10,1": @"iPhone 8",         // CDMA
                              @"iPhone10,4": @"iPhone 8",         // GSM
                              @"iPhone10,2": @"iPhone 8 Plus",    // CDMA
                              @"iPhone10,5": @"iPhone 8 Plus",    // GSM
                              @"iPhone10,3": @"iPhone X",         // CDMA
                              @"iPhone10,6":@"iPhone X",          // GSM
                              
                              @"iPad4,1"   :@"iPad Air",          // 5th Generation iPad (iPad Air) - Wifi
                              @"iPad4,2"   :@"iPad Air",          // 5th Generation iPad (iPad Air) - Cellular
                              @"iPad4,4"   :@"iPad Mini",         // (2nd Generation iPad Mini - Wifi)
                              @"iPad4,5"   :@"iPad Mini",         // (2nd Generation iPad Mini - Cellular)
                              @"iPad4,7"   :@"iPad Mini",         // (3rd Generation iPad Mini - Wifi (model A1599))
                              @"iPad6,7"   :@"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1584)
                              @"iPad6,8"   :@"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1652)
                              @"iPad6,3"   :@"iPad Pro (9.7\")",  // iPad Pro 9.7 inches - (model A1673)
                              @"iPad6,4"   :@"iPad Pro (9.7\")"   // iPad Pro 9.7 inches - (models A1674 and A1675)
                              };
    }
    
    NSString* deviceName = [deviceNamesByCode objectForKey:code];
    
    if (!deviceName) {
        // Not found on database. At least guess main device type from string contents:
        
        if ([code rangeOfString:@"iPod"].location != NSNotFound) {
            deviceName = @"iPod Touch";
        }
        else if([code rangeOfString:@"iPad"].location != NSNotFound) {
            deviceName = @"iPad";
        }
        else if([code rangeOfString:@"iPhone"].location != NSNotFound){
            deviceName = @"iPhone";
        }
        else {
            deviceName = @"Unknown";
        }
    }
    
    return deviceName;
}

+(BOOL) isSmallDevise{
    if ([[self deviceName] isEqualToString:@"iPhone 4S"] || [[self deviceName] isEqualToString:@"iPhone 4"]
         || [[self deviceName] isEqualToString:@"iPhone 5s"] || [[self deviceName] isEqualToString:@"iPhone 5c"] || [[self deviceName] isEqualToString:@"iPhone 5"] || [[self deviceName] hasPrefix:@"iPad"]){
        return true;
    }
    
    return false;
}

+(void) validateTextField:(UITextField * ) tf isValid:(BOOL) val {

    if(val){
        tf.layer.borderWidth = 0;
        tf.layer.borderColor = [[UIColor blackColor] CGColor];
        
    }else{
        tf.layer.borderColor = [[UIColor redColor] CGColor];
        tf.layer.borderWidth = 1.0;
 
    }

}



+(BOOL) addIntPref:(NSInteger)i forKey:(NSString *) key{

    
    [[NSUserDefaults standardUserDefaults]setInteger:i forKey:key ];
    
    return [[NSUserDefaults standardUserDefaults] synchronize];

}


+(BOOL) addStringPref:(NSString *)str forKey:(NSString *) key{
    
    
    [[NSUserDefaults standardUserDefaults]setObject:str forKey:key ];
    
    return [[NSUserDefaults standardUserDefaults] synchronize];
    
}

+(NSString * ) getPrefStringForKey:(NSString * )key {


    return [[NSUserDefaults standardUserDefaults] stringForKey:key];

}


+(NSInteger) getPrefIntForKey:(NSString * )key {
    
    
    return [[NSUserDefaults standardUserDefaults] integerForKey:key];
    
}

+(void) logout{

    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userid"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"current_ride"];
    
    
    [GeneralFunc addIntPref:0 forKey:@"userid"];
    [GeneralFunc addStringPref:nil forKey:@"Email"];
    [GeneralFunc addStringPref:nil forKey:@"NameAR"];
    [GeneralFunc addStringPref:nil forKey:@"NameEn"];
    [GeneralFunc addStringPref:nil forKey:@"Gender"];
    [GeneralFunc addStringPref:nil forKey:@"Verified"];
    [GeneralFunc addStringPref:nil forKey:@"Password"];
    
     [[GIDSignIn sharedInstance] signOut];
    

}


+(NSString *) getDateTimeAsString:(NSDate *) date{

    NSDateFormatter *f = [NSDateFormatter new];
    [f setDateFormat:@"hh:mm a  dd/MM/YYYY"];
    
    return [f stringFromDate:date];

}


+(NSString *) getDateTimeAsStringForServer:(NSDate *) date{
    
    NSDateFormatter *f = [NSDateFormatter new];
    [f setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
    
    return [f stringFromDate:date];
}

+(NSDate *) getFinalDateTimeAsStringFrom:(NSDate *) date hour:(int) h minutes:(int) m  {

    NSDate *oldDate = date;
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [calendar components:unitFlags fromDate:oldDate];
    comps.hour   = h;
    comps.minute = m;
    
    return  [calendar dateFromComponents:comps];


}


+(NSString *) getDateAsString:(NSDate *) date{
    
    NSDateFormatter *f = [NSDateFormatter new];
    [f setDateFormat:@"dd/MM/YYYY"];
    
    return [f stringFromDate:date];
    
}


+(NSString *) getDateAsStringServer:(NSDate *) date{
    
    NSDateFormatter *f = [NSDateFormatter new];
    [f setDateFormat:@"YYYY-MM-dd"];
    
    return [f stringFromDate:date];
    
}


+(NSDate *) getDate :(NSString *) date{
    
    NSDateFormatter *f = [NSDateFormatter new];
    [f setDateFormat:@"dd/MM/YYYY"];
    
    return [f dateFromString:date];
    
}


+(NSDate *) getTime :(NSString *) date{
    
    NSDateFormatter *f = [NSDateFormatter new];
    [f setDateFormat:@"hh:mm"];
    
    return [f dateFromString:date];
    
}



+(NSString *) getTimeAsString:(NSDate *) date{
    
    NSDateFormatter *f = [NSDateFormatter new];
    [f setDateFormat:@"hh:mm a"];
    
    return [f stringFromDate:date];
    
}


+ (NSString *)urlencode:(NSString *)input {
    
     return [input stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
 }


// Convert string From Arabic/Persian numbers to English numbers
+(NSString *) convertToEnglishNumber:(NSString *) string {
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"fa"];
    for (NSInteger i = 0; i < 10; i++) {
        NSNumber *num = @(i);
        string = [string stringByReplacingOccurrencesOfString:[formatter stringFromNumber:num] withString:num.stringValue];
    }
    
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
    for (NSInteger i = 0; i < 10; i++) {
        NSNumber *num = @(i);
        string = [string stringByReplacingOccurrencesOfString:[formatter stringFromNumber:num] withString:num.stringValue];
    }
    
    return string;
}


+(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize
{
    CGFloat scale = [[UIScreen mainScreen]scale];
    /*You can remove the below comment if you dont want to scale the image in retina   device .Dont forget to comment UIGraphicsBeginImageContextWithOptions*/
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, scale);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}



+(NSInteger) distance:(NSString *) slat slng:(NSString *) slng elat:(NSString *) elat elng:(NSString *) elng{

    
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:[slat doubleValue] longitude:[slng doubleValue] ];
    
    CLLocation *locB = [[CLLocation alloc] initWithLatitude:[elat doubleValue]  longitude:[elng doubleValue] ];
    
    CLLocationDistance distance = [locA distanceFromLocation:locB];
    
    return distance;
    
}


+(BOOL)isLocationInJordan:(CLLocationCoordinate2D)location{
    
    NSString *urlString = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=false",location.latitude, location.longitude];
    NSError* error;
    NSString *locationString = [NSString stringWithContentsOfURL:[NSURL URLWithString:urlString] encoding:NSASCIIStringEncoding error:&error];
    
    NSData *data = [locationString dataUsingEncoding:NSUTF8StringEncoding];
    
    if (data != nil) {
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
        NSArray *result = [json objectForKey:@"results"];
        if (result.count > 0){
            NSDictionary *dic = result[0];
            NSArray *addressComponents = [dic objectForKey:@"address_components"];
            NSString *cityCode = addressComponents.lastObject[@"short_name"];
        
            if  (cityCode){
                return [cityCode isEqualToString:@"JO"];}
        
            }
    }
    
    return false;
}
    
+(void)openLink:(NSString*)link{
    NSURL *url = [NSURL URLWithString:link];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

@end

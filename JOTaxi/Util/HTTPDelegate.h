//
//  HTTPDelegate.h
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/16/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GeneralFunc.h"

@protocol HTTPDel <NSObject>

-(void)HTTPJustStart;
-(void)HTTPInProgress;
-(void)HTTPDone:(NSData *) data response:(NSURLResponse * )res error:(NSError *) error;

@optional
-(void)HTTPDone:(NSData *) data response:(NSURLResponse * )res error:(NSError *) error forID:(int)ID;


@end


@interface HTTPDelegate : NSObject

-(instancetype)initPOSTWithURL:(NSString *)url body:(NSString *) jsonBody;
-(instancetype)initWithURL:(NSString *) url ;

-(void) startHttp;
-(void) startHttp:(int) ID;
-(void) stopHttp;

@property (nonatomic, weak) id <HTTPDel> delegate;  


@end

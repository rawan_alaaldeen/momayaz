//
//  GernralFunc.h
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/15/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <Google/Analytics.h>

#import "Consts.h" 
 
#define FONT_NAME @"Dax-Regular"

#define FBLink @"https://www.facebook.com/JO-Taxi-116526292307263/"
#define TwitterLink @"https://twitter.com/joTaxiJo?s=01"
#define InstagrameLink @"https://www.instagram.com/jo_taxi/"


#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]


@interface GeneralFunc : NSObject


+(int) compareDates:(NSDate *) d1 date2: (NSDate *)d2;
+(NSString *) convertToEnglishNumber:(NSString *) string ;

+(BOOL) validateEmail:(NSString *) email;
+(BOOL) validatePhone:(NSString *) phone;

+(void) showLoading:(UIViewController *) cont;
+(void) hideLoading:(UIViewController *)cont;

+(UIAlertController *) showAlertDialogWithTitle:(NSString * )title Message:(NSString *) msg;
+(void) hideAlert;

+(void) validateTextField:(UITextField * ) tf isValid:(BOOL) val ;



+(BOOL) addIntPref:(NSInteger)i forKey:(NSString *) key;
+(BOOL) addStringPref:(NSString *)str forKey:(NSString *) key;
+(NSString * ) getPrefStringForKey:(NSString * )key;
+(NSInteger) getPrefIntForKey:(NSString * )key ;
+(void) logout;


+(NSString *)urlencode:(NSString *)input ;

+(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize;

+(NSString *) getDateTimeAsString:(NSDate *) date;

+(NSString *) getDateAsString:(NSDate *) date;
+(NSString *) getTimeAsString:(NSDate *) date;
+(NSDate *) getFinalDateTimeAsStringFrom:(NSDate *) date hour:(int) h minutes:(int) m ;
+(NSString *) getDateTimeAsStringForServer:(NSDate *) date;
+(NSDate *) getTime :(NSString *) date;
+(NSDate *) getDate :(NSString *) date;
+(NSString *) getDateAsStringServer:(NSDate *) date;

+(NSString *) convertToEnglishNumber:(NSString *) string;

+(NSInteger) distance:(NSString *) slat slng:(NSString *) slng elat:(NSString *) elat elng:(NSString *) elng;

+(BOOL)isLocationInJordan:(CLLocationCoordinate2D)location;

+(void)openLink:(NSString*)link;

+(NSString*) deviceName;

+(BOOL) isSmallDevise;

    
@end

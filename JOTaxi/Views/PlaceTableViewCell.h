//
//  PlaceTableViewCell.h
//  Momayaz
//
//  Created by Hammoudeh Alamri on 17/06/2017.
//  Copyright © 2017 GCE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaceTableViewCell : UITableViewCell


@property (nonatomic, strong) IBOutlet UILabel *line1;
@property (nonatomic, strong) IBOutlet UILabel *line2;
@property (nonatomic, strong) IBOutlet UIButton *btnFav;


@end

//
//  NearestCarsCell.h
//  JOTaxi
//
//  Created by Rawan Alaeddin on 9/2/17.
//  Copyright © 2017 GCE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NearestCarsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *driverName;
@property (weak, nonatomic) IBOutlet UILabel *duration;

@property (weak, nonatomic) IBOutlet UILabel *carType;
@end

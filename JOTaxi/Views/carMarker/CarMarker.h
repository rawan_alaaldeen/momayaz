//
//  CarMarker.h
//  JOTaxi
//
//  Created by Rawan Alaeddin on 8/28/17.
//  Copyright © 2017 GCE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Car.h"

@interface CarMarker : UIView
@property (weak, nonatomic) IBOutlet UIView *carContainerView;
@property (weak, nonatomic) IBOutlet UIView *timeContainerView;
@property (weak, nonatomic) IBOutlet UIView *driverInfoContainerView;

@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UILabel *driverName;
    @property (weak, nonatomic) IBOutlet UIStackView *ratingStack;

-(void)fillCarData:(Car*)car;
@end

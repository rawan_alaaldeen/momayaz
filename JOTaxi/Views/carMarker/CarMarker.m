//
//  CarMarker.m
//  JOTaxi
//
//  Created by Rawan Alaeddin on 8/28/17.
//  Copyright © 2017 GCE. All rights reserved.
//

#import "CarMarker.h"

@implementation CarMarker

-(void)layoutSubviews{
    _carContainerView.layer.cornerRadius = _carContainerView.frame.size.height/2;
    
    _carContainerView.layer.borderColor = [UIColor blackColor].CGColor;;
    _carContainerView.layer.borderWidth = 1.0f;
    
    _timeContainerView.layer.cornerRadius = _timeContainerView.frame.size.height/2;
    
    _driverInfoContainerView.layer.cornerRadius = _driverInfoContainerView.frame.size.height/2;

}
-(void)fillCarData:(Car*)car{
    self.durationLabel.text = car.duration;
    self.driverName.text = car.driverName;
    
    if (car.driverRate < 0 ){
        car.driverRate = 5;
    }
 
    for(int i=0 ; i<car.driverRate ; i++){
        [self.ratingStack.subviews[i] setHidden:false];
    }

}

@end

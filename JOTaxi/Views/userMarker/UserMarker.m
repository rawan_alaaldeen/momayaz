//
//  UserMarker.m
//  JOTaxi
//
//  Created by Rawan Alaeddin on 8/24/17.
//  Copyright © 2017 GCE. All rights reserved.
//

#import "UserMarker.h"

@implementation UserMarker

-(void)layoutSubviews{
    _userInfoView.layer.borderWidth = 1;
    _userInfoView.layer.borderColor = [UIColor blackColor].CGColor;
    _userInfoView.layer.cornerRadius = _userInfoView.frame.size.height/2;


}

@end

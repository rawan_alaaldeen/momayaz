//
//  UserMarker.h
//  JOTaxi
//
//  Created by Rawan Alaeddin on 8/24/17.
//  Copyright © 2017 GCE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserMarker : UIView
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UIView *userInfoView;

 
@end

//
//  HistoryCell.swift
//  JOTaxi
//
//  Created by Rawan Alaeddin on 9/12/17.
//  Copyright © 2017 GCE. All rights reserved.
//

import UIKit
 protocol HistoryCellDelegate{
    func deleteItem(_ historyItem:HistoryModel)
}

class HistoryCell: UITableViewCell {

    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var destenationLocationLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var sourceLocationLabel: UILabel!
    @IBOutlet weak var destenationPlaceView: UIView!
    var historyItem : HistoryModel!{
        didSet{
            timeLabel.text = historyItem.creationDate
            
            if historyItem.eAddress == "" {
                destenationPlaceView.isHidden = true
            }
            else{
                destenationPlaceView.isHidden = false
                destenationLocationLabel.text = historyItem.eAddress
            }
            
            sourceLocationLabel.text = historyItem.sAddress
            deleteButton.isHidden = !historyItem.isSchaduled
         }
        
    }
    
    
    var delegate:HistoryCellDelegate?
    
     @IBAction func deleteHistoryItem() {
        delegate?.deleteItem(historyItem)
    }
 
}

//
//  FullPopupViewController.swift
//  JOTaxi
//
//  Created by Rawan Alaeddin on 9/8/17.
//  Copyright © 2017 GCE. All rights reserved.
//

import UIKit

@objc protocol FullPopupDelegate {
    func dismissFullPopup(_ type:FullPopupType)
}


@objc enum FullPopupType:Int{
    case noTaxiAlert
    case startJourney
    case journeyDidFinsh
    case journeyDidCancle
    case editProfile

}


class FullPopupView: UIView {

    
    @IBOutlet weak var alertTitle: UILabel!
    @IBOutlet weak var detilsText: UILabel!

    @IBOutlet weak var cancelButton: UIButton!
    
    var delegate:FullPopupDelegate?
    var popupType:FullPopupType =  FullPopupType.noTaxiAlert

    var message:String?
    
    override func didMoveToSuperview() {
        
        cancelButton.layer.borderWidth = 1
        cancelButton.layer.borderColor = UIColor.white.cgColor
        
        
        switch popupType {
        case .noTaxiAlert:
            alertTitle.text = NSLocalizedString("no_taxi_title", comment: "")
            detilsText.text = NSLocalizedString("no_taxi_alert", comment: "")
            
        case .startJourney:
            alertTitle.text = NSLocalizedString("start_journey", comment: "")
            detilsText.text = ""
        case .journeyDidFinsh:
            alertTitle.text = NSLocalizedString("thanks", comment: "")
            detilsText.text = ""
        case .editProfile:
            alertTitle.text = message!
            detilsText.text = NSLocalizedString("thanks", comment: "")

        case .journeyDidCancle:
            alertTitle.text = NSLocalizedString("journey_did_cancle", comment: "")
            detilsText.text = NSLocalizedString("thanks", comment: "")

        }

    }
 
 
    @IBAction func dismiss() {
        self.removeFromSuperview()
        delegate?.dismissFullPopup(popupType)
    }
    
    
    
 }

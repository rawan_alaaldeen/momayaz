//
//  ContactUsViewController.m
//  Momayaz
//
//  Created by Hammoudeh Alamri on 9/4/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import "ContactUsViewController.h"
#import "ContactUs.h" 


@interface ContactUsViewController ()

@end

@implementation ContactUsViewController
{

    HTTPDelegate *http;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     
    if( [GeneralFunc getPrefStringForKey:@"Email"]){
        
        _etxt_Email.text = [GeneralFunc getPrefStringForKey:@"Email"];
        _etxt_Email.enabled=false;
        _etxt_Name.text = [GeneralFunc getPrefStringForKey:@"NameEn"];
        _etxt_Name.enabled=false;
        _etxt_Phone.text = [GeneralFunc getPrefStringForKey:@"Phone"];
        _etxt_Phone.enabled = false;
        
    }

}

-(void)viewWillAppear:(BOOL)animated{
    _submitButton.layer.borderColor = [UIColor whiteColor].CGColor;
    _submitButton.layer.borderWidth = 1;
    
    _cancelButton.layer.borderColor = [UIColor whiteColor].CGColor;
    _cancelButton.layer.borderWidth = 1;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)dosend:(id)sender{

    
    
    BOOL allIsWell= true;
    
    // validate email
    if(![GeneralFunc validateEmail:_etxt_Email.text]){
        
        
        allIsWell =false;
        
        [GeneralFunc validateTextField:_etxt_Email isValid:false];
        
        
    }else{
        [GeneralFunc validateTextField:_etxt_Email isValid:true];
        
    }
    
    
    /// validate phone
    if(![GeneralFunc validatePhone:_etxt_Phone.text]){
        allIsWell =false;
        
        [GeneralFunc validateTextField:_etxt_Phone isValid:false];
        
        
    }else{
        [GeneralFunc validateTextField:_etxt_Phone isValid:true];
        
    }

    
    
    
    
    
    ContactUs *s = [ContactUs new];
    
    s.name = _etxt_Name.text;
    s.email = _etxt_Email.text;
    s.subject = _etxt_Sub.text;
    s.body = _etxt_Body.text;
    s.phone = _etxt_Phone.text;
    s.userID = [NSString stringWithFormat:@"%ld", [GeneralFunc getPrefIntForKey:@"userid"]];
    s.reason = [NSString stringWithFormat:@"%ld", _seg_reason.selectedSegmentIndex+1];
    
    
    http = [[HTTPDelegate alloc] initPOSTWithURL:[NSString stringWithFormat:@"%@%@", URLsMainURL, URLsContactUS] body:[s JSONString]];
    
    http.delegate = self;
    [http startHttp:1];

    
    
    
   

}

- (IBAction)goBack:(id)sender {
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


#pragma mark - HTTP 
-(void)HTTPInProgress{
}

-(void)HTTPJustStart{

    [GeneralFunc showLoading:self];
}
-(void)HTTPDone:(NSData *)data response:(NSURLResponse *)res error:(NSError *)error forID:(int)ID{

    
    [GeneralFunc hideLoading:self] ;
      NSString* messageString =@"Sorry something wrong! Please try again";
    
    if(data){
    
        NSMutableDictionary  *dic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

        
        
        if( [dic objectForKey:@"Status"]){
            if([[dic objectForKey:@"Status"] integerValue] == 1){
                
                messageString =[dic objectForKey:@"Message"];
                
                UIAlertController * al =  [GeneralFunc showAlertDialogWithTitle:@"" Message:[dic objectForKey:@"Message"]];
                
                [al addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction *l ){
                    [al popoverPresentationController];
                    [self dismissViewControllerAnimated:YES completion:nil];
                    
                }]];
                
                [self presentViewController:al animated:YES completion:nil];
                
                
            }else{
                
               
                UIAlertController * al =  [GeneralFunc showAlertDialogWithTitle:@"" Message:[dic objectForKey:@"Message"]];
                
                [al addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction *l ){
                    [al popoverPresentationController];
                    [self dismissViewControllerAnimated:YES completion:nil];
                    
                }]];
                
                [self presentViewController:al animated:YES completion:nil];
                
                
            }
        
    
    }
    
    }
    

}


-(void)HTTPDone:(NSData *)data response:(NSURLResponse *)res error:(NSError *)error{
}


@end

//
//  TYPlaceSearchViewController.m
//  MapkitAPI
//
//  Created by Thabresh on 8/9/16.
//  Copyright © 2016 VividInfotech. All rights reserved.
//

#import "TYPlaceSearchViewController.h"
#import "Consts.h"
#import "JSONParser.h"

@interface TYPlaceSearchViewController ()

//@property (strong, nonatomic) UITableView *autoCompleteTableView;
//@property (strong, nonatomic) UIBarButtonItem *closeButton;
//@property (strong, nonatomic) UISearchBar *searchBar;
@property (strong, nonatomic) UIActivityIndicatorView *searchLoadingActivityIndicator;


@end

@implementation TYPlaceSearchViewController{
    NSTimer *_autoCompleteTimer;
    NSString *_substring;
    NSMutableArray *arr;
    
    HTTPDelegate *http ;
}
-(void)viewWillAppear:(BOOL)animated {

    
    
    
}


- (void)viewDidLoad {
   
     [super viewDidLoad];
    
    
    http = [[HTTPDelegate alloc] initWithURL:@""];
    http.delegate = self;
         
    
    [self.searchBar becomeFirstResponder];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Actions

- (IBAction)onCloseButtonPressed:(UIBarButtonItem *)sender {
    [self.searchBar resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - UISearchBarDelegate Methods

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [_autoCompleteTimer invalidate];
    [self searchAutocompleteLocationsWithSubstring];
    [searchBar resignFirstResponder];
    [self.autoCompleteTableView reloadData];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    NSString *searchWordProtection = [searchBar.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (searchWordProtection.length != 0) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.searchLoadingActivityIndicator startAnimating];
        });
        
        [self runScript];
    } else {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.searchLoadingActivityIndicator stopAnimating];
        });
    }
}

-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    _substring = [NSString stringWithString:searchBar.text];
    _substring= [_substring stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    _substring = [_substring stringByReplacingCharactersInRange:range withString:text];
    
    if ([_substring hasPrefix:@"+"] && _substring.length >1) {
        _substring  = [_substring substringFromIndex:1];
    }
    
    return YES;
}


#pragma mark - Auto Complete Helper Methods
- (void)runScript{
    
    [arr removeAllObjects];
    [_autoCompleteTimer invalidate];
    [http  stopHttp];
    _autoCompleteTimer = [NSTimer scheduledTimerWithTimeInterval:0.65f
                                                          target:self
                                                        selector:@selector(searchAutocompleteLocationsWithSubstring)
                                                        userInfo:_substring
                                                         repeats:NO];
}


#pragma mark - Networking Methods

- (void)searchAutocompleteLocationsWithSubstring {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.searchLoadingActivityIndicator startAnimating];
    });
    
    
    
    NSString *url = [NSString stringWithFormat:@"%@&name=%@&x=%lf&y=%lf" , URLsSearchPlaceByName,[GeneralFunc urlencode:_substring], [[GeneralFunc getPrefStringForKey:@"lng"] doubleValue], [[GeneralFunc getPrefStringForKey:@"lat"] doubleValue]];
    
     http = [[HTTPDelegate alloc] initWithURL:url];
     http.delegate = self;
     [http  startHttp];
    
    
    
}



#pragma mark - Table View Data Source Methods

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return NO;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
   return arr.count + 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row < arr.count) {
        return [self locationSearchResultCellForIndexPath:indexPath];
    } else {
        return [self loadingCell];
     }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 70.0;
}

- (UITableViewCell *)locationSearchResultCellForIndexPath:(NSIndexPath *)indexPath {
    
    
    Place * p = [arr objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [self.autoCompleteTableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        
    }
    
     [cell.textLabel setText:p.nameAr];
     [cell.detailTextLabel setText:[NSString stringWithFormat:@"%@", p.stAr ]];
    
    return cell;
}


#pragma mark - Table View Delegate Methods


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.searchBar resignFirstResponder];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    

    
    [self.delegate searchViewController:self didReturnPlace:[arr objectAtIndex:indexPath.row]];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Properties

- (UITableViewCell *)loadingCell {
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell.contentView addSubview:self.searchLoadingActivityIndicator ];
    
    return cell;
}

-(UIActivityIndicatorView *)searchLoadingActivityIndicator {
    if (!_searchLoadingActivityIndicator) {
        _searchLoadingActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [_searchLoadingActivityIndicator setCenter:CGPointMake(self.view.center.x, 22)];
        [_searchLoadingActivityIndicator setHidesWhenStopped:YES];
    }
    return _searchLoadingActivityIndicator;
}

//-(UITableView *)autoCompleteTableView {
//    if (!_autoCompleteTableView) {
//        _autoCompleteTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
//        [_autoCompleteTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
//        [_autoCompleteTableView setDelegate:self];
//        [_autoCompleteTableView setDataSource:self];
//    }
//    return _autoCompleteTableView;
//}

//-(UISearchBar *)searchBar {
//    if (!_searchBar) {
//        _searchBar = [[UISearchBar alloc] init];
//        [_searchBar setDelegate:self];
//        [_searchBar setPlaceholder:@"Search Location.."];
//    }
//    return _searchBar;
//}






#pragma mark - Helper Methods

- (void) showError:(NSError *)error {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Error"
                                          message:error.localizedDescription
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:nil];
    
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}


#pragma mark - Http 
-(void)HTTPInProgress{

}
-(void)HTTPJustStart{
}

-(void)HTTPDone:(NSData *)data response:(NSURLResponse *)res error:(NSError *)error{

    
    
   arr =  [NSMutableArray arrayWithArray: [JSONParser ParsePlaceByName:data]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.autoCompleteTableView reloadData];
        [self.searchLoadingActivityIndicator stopAnimating];
        
    });
    
   

}

#pragma mark - Lifetime

- (void)dealloc
{
    self.autoCompleteTableView = nil;
}
@end

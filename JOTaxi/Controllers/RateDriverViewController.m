//
//  RateDriverViewController.m
//  Momayaz
//
//  Created by Hammoudeh Alamri on 06/05/2017.
//  Copyright © 2017 GCE. All rights reserved.
//

#import "RateDriverViewController.h"


@interface RateDriverViewController ()

@end

@implementation RateDriverViewController
{
    Order *orderInfo;
    HTTPDelegate *httpDell;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];

    _sendButton.layer.borderColor = [UIColor whiteColor].CGColor;
    _sendButton.layer.borderWidth = 1;
    
    _cancleButton.layer.borderColor = [UIColor whiteColor].CGColor;
    _cancleButton.layer.borderWidth = 1;
    
    
    [self fillData];

}

-(void)viewWillAppear:(BOOL)animated{
    _viRate.value = 3;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)fillData{
    if (_isFromHistory) {
        _lblName.text = _driverName;
        _lblPlate.text = _plateNumber;

    }
    else{
        orderInfo =[JSONParser ParseOrderFromJSON:[GeneralFunc getPrefStringForKey:@"lastOrderInfo"]];
        
        _lblName.text = [GeneralFunc getPrefStringForKey:@"dname"];
        _lblPlate.text = [GeneralFunc getPrefStringForKey:@"dplate"];

    }

}


- (IBAction)actionSend:(id)sender {
    
    [GeneralFunc showLoading:self];
    
    NSString *orderID = 0;
    if  (_isFromHistory){
        orderID = _orderID;
    }
    else{
         orderID = [NSString stringWithFormat:@"%ld",(long)[GeneralFunc getPrefIntForKey:@"NeedToRate"] ];}
    
    httpDell = [[HTTPDelegate alloc] initWithURL:[NSString stringWithFormat:@"%@%@?OrderID=%@&Rate=%d&Comment=%@",URLsMainURL,URLsRateUS,orderID,(int)_viRate.value,[GeneralFunc urlencode: _txtComment.text]]];
    
    httpDell.delegate = self;
    
    [httpDell startHttp:100];
    
    
    
}

- (IBAction)actionBack:(id)sender {
    
    [GeneralFunc addIntPref:0 forKey:@"NeedToRate"];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


#pragma mark- http 
-(void)HTTPJustStart{
}
-(void)HTTPInProgress{}
-(void)HTTPDone:(NSData *)data response:(NSURLResponse *)res error:(NSError *)error{}
-(void)HTTPDone:(NSData *)data response:(NSURLResponse *)res error:(NSError *)error forID:(int)ID{
    [GeneralFunc hideLoading:self];

    if(ID ==100){
    
        [GeneralFunc addIntPref:0 forKey:@"NeedToRate"];
        
        UIAlertController * al = [GeneralFunc showAlertDialogWithTitle:NSLocalizedString(@"", nil) Message:NSLocalizedString(@"Thank you for rating our driver!", nil)];
        
        [al addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];

        }]];
        
        
        [self presentViewController:al animated:YES completion:nil];
        
    
    }
    
    


}
 

@end

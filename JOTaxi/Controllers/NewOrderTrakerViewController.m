//
//  NewOrderTrakerViewController.m
//  Momayaz
//
//  Created by Hammoudeh Alamri on 11/23/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import "NewOrderTrakerViewController.h"

@interface NewOrderTrakerViewController ()

@end

@implementation NewOrderTrakerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    _webview.scalesPageToFit = YES;
    _webview.scrollView.scrollEnabled = NO;
    _webview.allowsInlineMediaPlayback = YES;
    _webview.mediaPlaybackRequiresUserAction = NO;
    
    
    NSString *htmlPath = [[NSBundle mainBundle] pathForResource:@"mapfile"
                                                         ofType:@"html"
                                                    inDirectory:@"/html" ];
    
    NSString *html = [NSString stringWithContentsOfFile:htmlPath
                                               encoding:NSUTF8StringEncoding
                                                  error:nil];
    
    [_webview loadHTMLString:html
                    baseURL:[NSURL fileURLWithPath:
                             [NSString stringWithFormat:@"%@/html/",
                              [[NSBundle mainBundle] bundlePath]]]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end

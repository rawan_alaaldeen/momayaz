//
//  AboutUsViewController.m
//  JOTaxi
//
//  Created by Rawan Alaeddin on 8/31/17.
//  Copyright © 2017 GCE. All rights reserved.
//

#import "AboutUsViewController.h"
#import "GeneralFunc.h"

@interface AboutUsViewController ()

@end

@implementation AboutUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    _titleLabel.text = NSLocalizedString(@"about_us",nil);
    _aboutUsText.text = NSLocalizedString(@"about_us_text",nil);
}

- (IBAction)back {
    [self dismissViewControllerAnimated:true completion:nil];
}

    
- (IBAction)openSMLink:(UIButton*)sender{
    if (sender.tag == 1){
        [GeneralFunc openLink:FBLink];
        
    }
    else if (sender.tag == 2){
        [GeneralFunc openLink:TwitterLink];
        
    }
    else{
        [GeneralFunc openLink:InstagrameLink];
        
    }
}


@end

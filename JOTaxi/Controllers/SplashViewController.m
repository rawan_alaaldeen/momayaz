//
//  SplashViewController.m
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/22/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import "SplashViewController.h"
 #import "MainViewController.h"
#import "MainRootViewController.h"
#import "AppDelegate.h"
#import "HomeViewController.h" 
#import "ViewController.h" 

@interface SplashViewController ()

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];


    
    
//    LandingViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LandingViewController"];

    //HomeViewController *viewController = [[HomeViewController alloc] init];
      ViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    
    MainRootViewController *navigationController = [[MainRootViewController alloc] initWithRootViewController:viewController];
    
    
     RootMainViewController = [[MainViewController alloc] initWithRootViewController:navigationController presentationStyle:LGSideMenuPresentationStyleSlideBelow type:1];
    
    
    
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    
    window.rootViewController = RootMainViewController;
    
    [UIView transitionWithView:window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:nil
                    completion:nil];
    

    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

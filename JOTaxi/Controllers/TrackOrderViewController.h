//
//  TrackOrderViewController.h
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/22/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTTPDelegate.h"
#import "Consts.h"
#import "GeneralFunc.h"
#import <GoogleMaps/GoogleMaps.h>
#import "JSONParser.h"
#import "Order.h"
#import "AppDelegate.h"
#import "CancelOrderView.h"
 


@interface TrackOrderViewController : UIViewController <HTTPDel,CancelOrderPopupDelegate>

 @property (atomic) int currentOrder;
@property (weak, nonatomic) IBOutlet UIImageView *imgDriver;
@property (weak, nonatomic) IBOutlet UILabel *lblDriverName;
@property (weak, nonatomic) IBOutlet UILabel *lblPlateNo;
@property (weak, nonatomic) IBOutlet UIView *findingDriverLoaderView;

@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *cancelOrderButton;

@property (weak, nonatomic) IBOutlet UIButton *btnCallDriver;
@property (weak, nonatomic) IBOutlet UIImageView *imgCall;
@property (weak, nonatomic) IBOutlet UIImageView *imgTaxi;



@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UIView *viwInfo;

@property (weak, nonatomic) IBOutlet GMSMapView *Map;

@property (strong, nonatomic) Order * passedOrder;

@property (weak, nonatomic) IBOutlet UIView *driverInfoView;

@end

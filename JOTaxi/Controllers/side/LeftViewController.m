//
//  LeftViewController.m
//  LGSideMenuControllerDemo
//
//  Created by Grigory Lutkov on 18.02.15.
//  Copyright (c) 2015 Grigory Lutkov. All rights reserved.
//

#import "LeftViewController.h"
#import "AppDelegate.h"
#import "LeftViewCell.h"
#import "ViewController.h"
#import "MainRootViewController.h"

#import "SplashViewController.h"
#import "JOTaxi-Swift.h"



@interface LeftViewController ()

 
@end

@implementation LeftViewController




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
   
    
    if (self = [super initWithNibName:@"SideMenuView" bundle:[NSBundle mainBundle]]) {
        
        [self loadView];
        [self.view layoutIfNeeded];
        
        _menuItems =[ NSMutableArray new];
        
        [self.tableView registerClass:[LeftViewCell class] forCellReuseIdentifier:@"cell"];
       // self.tableView.contentInset = UIEdgeInsetsMake(44.f, 0.f, 44.f, 0.f);
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.bounces=false;
        
    }
    return self;
}



-(void)viewWillAppear:(BOOL)animated{
 

}


-(void) updateContent {

    [self.menuItems removeAllObjects];
    
    
    
    MenuItem *userName = [[MenuItem alloc] initWithTitle:[GeneralFunc getPrefStringForKey:@"NameEn"] andImage:@"profile" andTag:@"name"];

    MenuItem *history = [[MenuItem alloc] initWithTitle:NSLocalizedString(@"history_menu", @"") andImage:@"time" andTag:@"history"];

    MenuItem *editProfile = [[MenuItem alloc] initWithTitle:NSLocalizedString(@"edit_profile", @"") andImage:@"edit_profile_menu" andTag:@"profile"];

    
    MenuItem *call = [[MenuItem alloc] initWithTitle:NSLocalizedString(@"emergency_call_text", @"") andImage:@"call" andTag:@"call"];
    MenuItem *contactUs = [[MenuItem alloc] initWithTitle:NSLocalizedString(@"contact_us", nil) andImage:@"at" andTag:@"contactUs"];
    MenuItem *aboutUs = [[MenuItem alloc] initWithTitle:NSLocalizedString(@"about_us", nil) andImage:@"about_us_menu_icon" andTag:@"aboutUs"];
    MenuItem *signout = [[MenuItem alloc] initWithTitle:NSLocalizedString(@"signout", nil)  andImage:@"signout" andTag:@"signout"];

    [self.menuItems addObject:userName];
    [self.menuItems addObject:call];
    [self.menuItems addObject:editProfile];
    [self.menuItems addObject:history];
    [self.menuItems addObject:contactUs];
    [self.menuItems addObject:aboutUs];
    [self.menuItems addObject:signout];

    [self.tableView reloadData];
    
    [self.view updateConstraints];
    
    

}

#pragma mark - UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _menuItems.count;
}

#pragma mark - UITableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MenuCell"];
    
    if (!cell){
        cell = [[[NSBundle mainBundle]loadNibNamed:@"MenuCell" owner:nil options:nil]lastObject];
    }
    
    MenuItem *item = _menuItems[indexPath.row];
    
    cell.menuItemTitle.text = item.title;
    cell.menuItemImage.image = [UIImage imageNamed:item.imageName];
 
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
      return 44.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MenuItem *selectedItem = _menuItems[indexPath.row];
    
    if ( [selectedItem.tag isEqualToString:@"signout"])
    {
        
        [FBSDKAccessToken setCurrentAccessToken:nil];
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager logOut];
        
        if([self.delegete respondsToSelector:@selector(doAction:)]){
            [self.delegete doAction:@"out"];
        }
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        
    }
    
     else
    {
        if([self.delegete respondsToSelector:@selector(doAction:)]){
            
            [self.delegete doAction:selectedItem.tag];
        }

    }
}

- (IBAction)openSMLink:(UIButton*)sender{
        if (sender.tag == 1){
            [GeneralFunc openLink:FBLink];

        }
        else if (sender.tag == 2){
            [GeneralFunc openLink:TwitterLink];

        }
        else{
            [GeneralFunc openLink:InstagrameLink];

        }
}
    
 
@end

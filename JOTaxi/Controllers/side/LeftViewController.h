//
//  LeftViewController.h
//  LGSideMenuControllerDemo
//
//  Created by Grigory Lutkov on 18.02.15.
//  Copyright (c) 2015 Grigory Lutkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GeneralFunc.h"
#import "ViewController.h"
#import "MenuItem.h"


@protocol leftProtocole <NSObject>

@optional
-(void) doAction:(NSString *)actionName;

@end


@interface LeftViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *wlc;

 
@property (strong, nonatomic) UIColor *tintColor;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet NSMutableArray *menuItems;
@property (weak , nonatomic) id<leftProtocole> delegete;

-(void) updateContent;

@end

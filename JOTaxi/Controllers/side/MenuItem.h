//
//  MenuItem.h
//  JOTaxi
//
//  Created by Rawan Alaeddin on 8/31/17.
//  Copyright © 2017 GCE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuItem : NSObject
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *imageName;
@property (strong, nonatomic) NSString *tag;

- (id)initWithTitle:(NSString*)title andImage:(NSString*)image andTag:(NSString*)tag;
@end

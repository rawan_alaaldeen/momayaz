//
//  MenuItem.m
//  JOTaxi
//
//  Created by Rawan Alaeddin on 8/31/17.
//  Copyright © 2017 GCE. All rights reserved.
//

#import "MenuItem.h"

@implementation MenuItem

- (id)initWithTitle:(NSString*)title andImage:(NSString*)image andTag:(NSString*)tag
{
    self.title = title;
    self.imageName = image;
    self.tag = tag;
    
    return self;
}

@end

//
//  CancelOrderViewController.m
//  JOTaxi
//
//  Created by Rawan Alaeddin on 9/7/17.
//  Copyright © 2017 GCE. All rights reserved.
//

#import "CancelOrderView.h"

@interface CancelOrderView ()
@end


@implementation CancelOrderView{
     NSString *selectedReason;

}


-(void)didMoveToSuperview{
    
    [super didMoveToSuperview];
    
    _cancelButton.layer.borderWidth = 1;
    _cancelButton.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _submitButton.layer.borderWidth = 1;
    _submitButton.layer.borderColor = [UIColor whiteColor].CGColor;

    [self selectReason:_firstReasonButton];
    
}
    
- (IBAction)selectReason:(UIButton*)sender {
    
    for (UIButton* button in self.checkButton){
        [button setImage:[UIImage imageNamed:@"check_not_selected"] forState:UIControlStateNormal];
    }
    
 
    for (UILabel* label in self.reasons){
        if (label.tag == sender.tag){
            selectedReason = label.text;
        }
    }

    [sender setImage:[UIImage imageNamed:@"check_selected"] forState:UIControlStateNormal];
    
}
    
- (IBAction)cancelOrder {
    
    if (![selectedReason isEqualToString:@""] && selectedReason != nil){
        [_delegate cancelOrderWithReason:selectedReason];
        [self dismiss];}
}
    
- (IBAction)dismiss {
    dispatch_async(dispatch_get_main_queue(), ^{
         [self removeFromSuperview];
    });
}

@end

//
//  CancelOrderViewController.h
//  JOTaxi
//
//  Created by Rawan Alaeddin on 9/7/17.
//  Copyright © 2017 GCE. All rights reserved.
//

#import <UIKit/UIKit.h>

 
@protocol CancelOrderPopupDelegate <NSObject>
    
-(void)cancelOrderWithReason:(NSString*)reason;
    
@end


@interface CancelOrderView : UIView
@property (weak, nonatomic) IBOutlet UIButton *firstReasonButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (nonatomic, weak) id <CancelOrderPopupDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIStackView *stackView;
@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *checkButton;
    @property (nonatomic, strong) IBOutletCollection(UILabel ) NSArray *reasons;

@end

//
//  BaseController.h
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/13/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "LGSideMenuController.h"
#import "HTTPDelegate.h"
#import "GeneralFunc.h"
#import "Consts.h"
#import "JSONParser.h"


#define kMainViewController  (MainViewController *)[UIApplication sharedApplication].delegate.window.rootViewController



@protocol SelectViewControllerDelegate <NSObject>
-(void)searchViewControllerDidReturnPlace:(Place *)place;
 @end


@interface BaseController : UIViewController <UITextFieldDelegate>



@property (weak, atomic) IBOutlet UIScrollView *scrolview;



@end

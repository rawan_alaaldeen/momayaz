//
//  BaseController.m
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/13/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import "BaseController.h"
#import "MainViewController.h"


#define GOBACK [self.navigationController popViewControllerAnimated:YES];

@implementation BaseController


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
 

}

-(IBAction)showLeftMenu:(id)sender {

   
   
    [kMainViewController showLeftViewAnimated:YES completionHandler:nil];

}


-(IBAction)goBack:(id)sender {
    GOBACK ;
}

- (void)viewWillAppear:(BOOL)animated
{
    
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden=YES;
    
 
    
    UITapGestureRecognizer *tapit = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollTap:)];
    [_scrolview addGestureRecognizer:tapit];

    
}


- (void)scrollTap:(UIGestureRecognizer*)gestureRecognizer {
    
    [self.view endEditing:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
     
}


 

@end

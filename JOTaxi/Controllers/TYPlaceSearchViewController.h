//
//  TYPlaceSearchViewController.h
//  MapkitAPI
//
//  Created by Thabresh on 8/9/16.
//  Copyright © 2016 VividInfotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Place.h"
#import "HTTPDelegate.h"

@class TYPlaceSearchViewController;

@protocol TYPlaceSearchViewControllerDelegate <NSObject>
-(void)searchViewController:(UIViewController *)controller didReturnPlace:(Place *)place;
@end

@interface TYPlaceSearchViewController : UIViewController <HTTPDel,UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (nonatomic) id<TYPlaceSearchViewControllerDelegate> delegate;

@property(weak ,nonatomic) IBOutlet  UITableView * autoCompleteTableView;
@property(weak ,nonatomic) IBOutlet  UISearchBar *  searchBar;

@end

//
//  TrackOrderViewController.m
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/22/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import "TrackOrderViewController.h"
#import "RateDriverViewController.h"
#import "JOTaxi-Swift.h"

@import NVActivityIndicatorView;
 
@interface TrackOrderViewController () <CMReceiver,FullPopupDelegate>
@property (weak, nonatomic) IBOutlet UIView *loaderView;

@end

@implementation TrackOrderViewController
{
    
    Order *thisorder ;
    HTTPDelegate *loadNearby ;
    NSArray * cars ;
    TaxiInfo * current ;
    
    HTTPDelegate *findPath ;
    HTTPDelegate *postOrderAgain ;
    HTTPDelegate *sendOrder  ;
    HTTPDelegate *cancelOrder  ;
    HTTPDelegate *checkOrderStatus  ;
    BOOL showLoading ;
    GMSMarker *UserMarker;
    GMSMarker *CarMarker;
    
    AppDelegate * app ;
    
    BOOL pickUpMode;
    
    int pushOrderTimes ;
    BOOL isTimerAvailabe;
    NSTimer *timeOutTimer;
    NSTimer *pushOrderTimer;
    NSString *cancelOrderReson;
    BOOL isComeFromPopup;
    
    IBOutlet NVActivityIndicatorView *activityIndecator;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    app =  (AppDelegate *) [[UIApplication sharedApplication]delegate];
    
    thisorder = [JSONParser ParseOrderFromJSON:[GeneralFunc getPrefStringForKey:@"lastOrderInfo"]];
    
    
    loadNearby = [[HTTPDelegate alloc] initWithURL:[NSString stringWithFormat:@"%@%@Longitude=%@&Latitude=%@",URLsMainURL,URLsNearby, thisorder.SLng,thisorder.SLat]];
    
    loadNearby.delegate = self;
    [loadNearby startHttp:1];
    
    GMSCameraPosition *cm = [GMSCameraPosition cameraWithLatitude:[thisorder.SLat doubleValue]
                                                        longitude:[thisorder.SLng doubleValue]
                                                             zoom:13];
    _Map.camera =cm;
    
    
    [self addUserPickupPin];
    showLoading = false;
    
    
    _findingDriverLoaderView.hidden=NO;
    [activityIndecator startAnimating];
    [_loaderView setHidden:false];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkorderStatus)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkorderStatus)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    [self styleTheMap];
    
    
 
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    if (!isComeFromPopup){

    if([GeneralFunc getPrefIntForKey:@"orderStatus"] ==5 ){
        
        [self.navigationController popViewControllerAnimated:YES];
        [GeneralFunc addIntPref:0 forKey:@"orderStatus"];
        
        return;
    }else if([GeneralFunc getPrefIntForKey:@"orderStatus"]>5){
        
        // _btnCancel.hidden=true;
        
    }
        
        [self createOrder];

    }
    
    [AppDelegate setDelegate:self];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    if (!isComeFromPopup){
        [self.driverInfoView setHidden:true];

    }

    
}


-(void)viewWillDisappear:(BOOL)animated{
    
    [AppDelegate removeDelegate];
    [self stopTimer];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // [SignalRMan removeDelegate];
    // [SignalRMan stopConnection];
}



-(void)createOrder{
    NSString * token = [[FIRInstanceID instanceID] token];
    NSLog(@"create order token %@",[token stringByReplacingOccurrencesOfString:@" " withString:@""]);
    NSLog(@"create order URL %@",[NSString stringWithFormat:@"%@%@customerName=%@&customerPhone=%@&customerGCM=%@", URLsMainURL, URLsNewOrder ,  [GeneralFunc urlencode:  [GeneralFunc getPrefStringForKey:@"NameAR"] ],[GeneralFunc urlencode: [GeneralFunc getPrefStringForKey:@"Phone"]],[token stringByReplacingOccurrencesOfString:@" " withString:@""] ]);

    _passedOrder.GCMTokenID = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *json = [GeneralFunc convertToEnglishNumber: [_passedOrder JSONString]];
    sendOrder = [[HTTPDelegate alloc] initPOSTWithURL:[NSString stringWithFormat:@"%@%@customerName=%@&customerPhone=%@&customerGCM=%@", URLsMainURL, URLsNewOrder ,  [GeneralFunc urlencode:  [GeneralFunc getPrefStringForKey:@"NameAR"] ],[GeneralFunc urlencode: [GeneralFunc getPrefStringForKey:@"Phone"]],[token stringByReplacingOccurrencesOfString:@" " withString:@""] ] body:json];
    sendOrder.delegate = self;
    
    
    [sendOrder startHttp:100];
    
    
}

-(void)schedualTimer{
    pushOrderTimer =  [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(refreshOrder) userInfo:nil repeats:false];
    
    isTimerAvailabe = true;
}

-(void)stopTimer{
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(refreshOrder) object:nil];
    [pushOrderTimer invalidate];
    pushOrderTimer = nil;
}

-(void)refreshOrder{
    
    if (isTimerAvailabe){
        if (pushOrderTimes ==  2){
            [self stopTimer];
            [self showNoTaxiAlert];}
        
        else{
            //** to stop calling push order (should called two times only)
            pushOrderTimes += 1;
            
            postOrderAgain = [[HTTPDelegate alloc] initWithURL:[NSString stringWithFormat:@"%@%@orderID=%@", URLsMainURL, @"Order/PushOrder?",[GeneralFunc getPrefStringForKey:@"lastOrder"]  ]];
            
            postOrderAgain.delegate = self;
            [postOrderAgain startHttp:120];}
    }
}


-(void)postOrder{
    
    
    _passedOrder.Preferences = [NSString stringWithFormat:@"%ld",[_passedOrder.Preferences integerValue]+16];
    _passedOrder.OrderID=@"0";
    
    postOrderAgain = [[HTTPDelegate alloc] initPOSTWithURL:[NSString stringWithFormat:@"%@%@", URLsMainURL, URLsNewOrder] body:[_passedOrder JSONString]];
    
    postOrderAgain.delegate = self;
    [postOrderAgain startHttp:101];
    
    
}

-(void)checkorderStatus{
    checkOrderStatus = [[HTTPDelegate alloc] initWithURL:[NSString stringWithFormat:@"%@%@orderID=%@", URLsMainURL, @"Order/GetOrderByID?",[GeneralFunc getPrefStringForKey:@"lastOrder"]]];
    
    checkOrderStatus.delegate = self;
    [checkOrderStatus startHttp:130];
    
}


-(void) addUserPickupPin{
    
    UserMarker=[[GMSMarker alloc]init];
    UserMarker.position=CLLocationCoordinate2DMake([thisorder.SLat doubleValue], [thisorder.SLng doubleValue]);
    
    UserMarker.icon=  [UIImage imageNamed:@"user_pin"]  ;
    UserMarker.map=_Map;
}

-(void) addUserCurrentCar{
    
    CarMarker=[[GMSMarker alloc]init];
    CarMarker.position=CLLocationCoordinate2DMake([current.Latitude doubleValue], [current.Longitude doubleValue]);
    CarMarker.rotation=  [current.Angle floatValue]  ;
    
    CarMarker.icon=  [UIImage imageNamed:@"car_marker_icon"]  ;
    CarMarker.title = [NSString stringWithFormat:@"%@ : %@ \n %@ : %@" ,NSLocalizedString(@"Driver :", @""),current.Driver, NSLocalizedString(@"Number :", @""),current.PlateNumber];
    
    CarMarker.map=_Map;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}







-(IBAction)close:(id)sender{
    
    if(timeOutTimer)
        [timeOutTimer invalidate];
    
    [GeneralFunc addStringPref:@"0" forKey:@"lastOrderTime"];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        CancelOrderView *cancelOrderView = [[NSBundle mainBundle] loadNibNamed:@"CancelOrderView" owner:nil options:nil].lastObject;
        cancelOrderView.frame = self.view.bounds;
        cancelOrderView.delegate = self;
        
        cancelOrderView.alpha = 0;
        [self.view addSubview:cancelOrderView];
        [cancelOrderView fadeInWithDuration:1];
    });

    
    
}

    
-(void)cancelOrderWithReason:(NSString *)reason{
    cancelOrder = [[HTTPDelegate alloc] initWithURL:[NSString stringWithFormat:@"%@%@OrderID=%@&Reason=%@&isDriver=false", URLsMainURL, URLsCancelOrder,[GeneralFunc getPrefStringForKey:@"lastOrder"],[GeneralFunc urlencode: reason]]];
    
    cancelOrder.delegate =self;
    [cancelOrder startHttp:102];
   
}

-(void)cancelOrder{
    cancelOrder = [[HTTPDelegate alloc] initWithURL:[NSString stringWithFormat:@"%@%@OrderID=%@&Reason=%@&isDriver=false", URLsMainURL, URLsCancelOrder,[GeneralFunc getPrefStringForKey:@"lastOrder"],cancelOrderReson]];
    
    cancelOrder.delegate =self;
    [cancelOrder startHttp:102];
    
    
}



-(void) updateDriverInfo:(NSString *) lat lng:(NSString *)lng   {
    
    _findingDriverLoaderView.hidden=YES;
    [_loaderView setHidden:true];
    [activityIndecator stopAnimating];

    [self.driverInfoView setHidden:false];
    
    
    _lblPlateNo.text=current.PlateNumber;
    _lblPlateNo.layer.cornerRadius = 3;
    _lblPlateNo.layer.borderColor = [UIColor blackColor].CGColor;
    _lblPlateNo.layer.borderWidth = 1;
    _lblDriverName.text=current.Driver;
 
    current.Latitude = lat;
    current.Longitude = lng ;
    
    
    
    _viwInfo.hidden=NO;
    _imgDriver.hidden=NO;
    _imgCall.hidden=NO;
    _imgTaxi.hidden=NO;
    _btnCallDriver.hidden=NO;
 
    
    if(current.Longitude&& current.Latitude && !pickUpMode){
        
        NSString * url = [NSString stringWithFormat:@"%@&point=%@,%@&point=%@,%@",URLsGetPath, thisorder.SLat,thisorder.SLng , current.Latitude, current.Longitude];
        
        findPath = [[HTTPDelegate alloc] initWithURL:url];
        
        findPath.delegate = self;
        [findPath startHttp:2];
    }
    
    
}


-(void) styleTheMap{
    
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSURL *styleUrl = [mainBundle URLForResource:@"googlemapstyle" withExtension:@"json"];
    NSError *error;
    
    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];
    
    if (!style) {
        NSLog(@"The style definition could not be loaded: %@", error);
    }
    
    _Map.mapStyle = style;
    
    
}



#pragma mark - http dele

-(void)HTTPJustStart{
    
    if(showLoading)
        [GeneralFunc showLoading:self];
}

-(void)HTTPInProgress{
}

-(void)HTTPDone:(NSData *)data response:(NSURLResponse *)res error:(NSError *)error{
}
-(void)HTTPDone:(NSData *)data response:(NSURLResponse *)res error:(NSError *)error forID:(int)ID{
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [GeneralFunc hideLoading:self];
        if( ID ==100){
            
            if(!data) {  [self.navigationController popViewControllerAnimated:YES];  return; }
            
            NSLog(@"New Order here %@ " , [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            
            // -2 means No Taxi available
            if( [[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@" "  withString:@""] isEqualToString:@"-2"]){
                
                
                [_loaderView setHidden:true];
               [self showNoTaxiAlert];
                return;
            }
            
            
            NSDictionary *d = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            
            
            if([d objectForKey:@"Message"]){
                
                return ;
            }
            
            // get order id
            [GeneralFunc addStringPref:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] forKey:@"lastOrder"];
            
            [GeneralFunc addStringPref:[NSString stringWithFormat:@"%ld",(long)[NSDate date].timeIntervalSince1970] forKey:@"lastOrderTime"];
            
            [self schedualTimer];
            
        }
        else if (ID == 120){
            [self schedualTimer];
        }
        
        else if (ID == 130){
            /*   Assigned = 1, //accepted and assign to a driver.
             Canceled = 2,
             Closed = 3,
             Open = 4, // no one accept the order yet.(finding driver popup).
             DriverWithCustomerOnBoard = 5 //picked up*/
            
            NSLog(@"STATUS %@ " , [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            if([response objectForKey:@"Status"]){
                NSInteger status = [[response objectForKey:@"Status"] integerValue];
                if (status == 3){
                    
                    [self journeyDidFinish];
                    
                }
                else  if (status == 2){
                    [self orderDidCancel];
                }
            }
 
        }
        else if( ID ==1){ // load nearby
            
            if(data){
                
                int maxCars= 20 ;
                
                cars  = [JSONParser ParseNearby:data];
                
                if (cars.count>0)[_Map clear];
                
                
                
                for (Car *c in cars) {
                    GMSMarker *marker=[[GMSMarker alloc]init];
                    marker.position=CLLocationCoordinate2DMake([c.Latitude doubleValue], [c.Longitude doubleValue]);
                    marker.rotation=  [c.Angle floatValue]  ;
                    marker.icon=  [UIImage imageNamed:@"car_marker_icon"]  ;
                    marker.map=_Map;
                    
                    if(maxCars--<1)break;
                    
                }
                
                [self addUserPickupPin];
                
            }
            
        }else  if( ID ==2){ // draw Path
            showLoading =false;
            
            [self drawLine: [JSONParser decodedpath:data]];
            NSDictionary *d = [JSONParser decodedInfo:data];

            _lblDistance.text = (d)?[NSString stringWithFormat:@"%.2lf %@ %@",[d[@"dis"] doubleValue]/1000 , NSLocalizedString(@"KM", ""),NSLocalizedString(@"away_from_you", "")]:@"";

            _lblTime.text = (d)?[NSString stringWithFormat:@"%@ %ld %@",NSLocalizedString(@"pickup_duration_text", ""),[d[@"time"] integerValue]/60000,NSLocalizedString(@"Minutes", "")]:@"";
            
            
        }else if (101 == ID ){
            
            
            NSLog(@"New Order here %@ " , [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            
            
            
            UIAlertController * c = [GeneralFunc showAlertDialogWithTitle:NSLocalizedString(@"No taxi available now",@"") Message:NSLocalizedString(@"Your order sent, our call center agent will call you before your taxi time. Thank you ", @"")];
            
            
            [c addAction:[UIAlertAction actionWithTitle:@"Ok " style:UIAlertActionStyleDefault handler:^(UIAlertAction *ac ){
                
                [GeneralFunc addIntPref:0 forKey:@"Reset"];
                
                [self dismissViewControllerAnimated:YES completion:nil];
                
                
            }]];
            
            [self presentViewController:c animated:YES completion:nil];
            
            
        }else if(ID ==102){
            
            [GeneralFunc addIntPref:1 forKey:@"Reset"];
            [self dismissViewControllerAnimated:false completion:^{
             }];
        }
        
        
    });
    
    
}

-(void)showNoTaxiAlert{
    
    FullPopupView *fullPopupView = [[NSBundle mainBundle] loadNibNamed:@"FullPopupView" owner:nil options:nil].lastObject;
    fullPopupView.frame = self.view.bounds;
    fullPopupView.delegate = self;
    fullPopupView.popupType = FullPopupTypeNoTaxiAlert;

    fullPopupView.alpha = 0;
    [self.view addSubview:fullPopupView];
    [fullPopupView fadeInWithDuration:1];
    
}

-(void)dismissFullPopup:(enum FullPopupType)type{
    
    isComeFromPopup = true;
    
    if (type == FullPopupTypeNoTaxiAlert || type == FullPopupTypeJourneyDidFinsh || type ==FullPopupTypeJourneyDidCancle){
        [self dismissViewControllerAnimated:false completion:^{
         }];
    }
    else if  (type == FullPopupTypeStartJourney){
        pickUpMode = true;
        [_Map clear];
        [self addUserCurrentCar];
     }
  
}

#pragma mark - methods

-(IBAction)callPhone:(id)sender{
    
    
    
    NSString *phNo = current.Phone;
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"tel:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        
    }
    
}


-(void) drawLine:(NSString *) path{
    
    if(!path) return;
    @try {
        
        [_Map clear];
        [self addUserPickupPin];
        [self addUserCurrentCar];
        
        GMSPolyline *polyline = [[GMSPolyline alloc] init];
        polyline.path = [GMSPath pathFromEncodedPath:path];
        polyline.strokeColor = [UIColor colorWithRed:65.0 green:134.0 blue:176.0 alpha:1.0];
        polyline.strokeWidth = 2;
        
        polyline.map = _Map;
        
        
        
        GMSCoordinateBounds *b = [[GMSCoordinateBounds alloc] initWithPath:polyline.path];
        
        GMSCameraUpdate *up =  [GMSCameraUpdate fitBounds:b];
        [_Map animateWithCameraUpdate:up];
        
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
}



/*
 
 Message Types
 AcceptOrder = 3,
 CancelOrder = 5,
 Pickup = 6,
 Tripend = 7,
 Arriving = 9,
 Arrived = 10,
 DriverLocation = 11
 */

-(void)MessageReceived:(NSDictionary *)msg{
    
    
    [GeneralFunc addIntPref:1 forKey:@"Reset"];
    
    if(msg[@"id"]){
        
        switch ([msg [@"type"] integerValue]) {
            case 3:{
                 if([msg[@"id"] integerValue] ==
                   [[GeneralFunc getPrefStringForKey:@"lastOrder"] integerValue]){
                    
                    isTimerAvailabe = false;
                    
                    current = [TaxiInfo new];
                    current.Driver = msg[@"name"];
                    current.PlateNumber = msg[@"pn"];
                    current.Latitude = msg[@"lat"];
                    current.Phone = msg[@"phone"];
                    
                    
                    [GeneralFunc addStringPref:current.Driver forKey:@"dname"];
                    [GeneralFunc addStringPref:current.PlateNumber forKey:@"dplate"];
                    
                    [self updateDriverInfo:msg[@"lat"] lng:msg[@"lng"]];
                    
                    if(timeOutTimer) [timeOutTimer invalidate];
                    
                }
            }  break;
                
                
            case 5:{
                [self orderDidCancel];
            }break;
                
                //** when driver press "استلام العميل"
            case 6:{
                
                isTimerAvailabe = false;
                
             //   [_btnCancel setHidden:true];
                [_driverInfoView setHidden:true];
                [_cancelOrderButton setHidden:true];
                
                FullPopupView *fullPopupView = [[NSBundle mainBundle] loadNibNamed:@"FullPopupView" owner:nil options:nil].lastObject;
                fullPopupView.frame = self.view.bounds;
                fullPopupView.delegate = self;
                fullPopupView.popupType = FullPopupTypeStartJourney;
                
                fullPopupView.alpha = 0;
                [self.view addSubview:fullPopupView];
                [fullPopupView fadeInWithDuration:1];
                
                
                
            }break;
                
            case 7:
                
                //** when journey finished
            case 9:
            {
                
                [self journeyDidFinish];
                break;
            }
                
            case 10 :{
                
                [GeneralFunc addIntPref:[[GeneralFunc getPrefStringForKey:@"lastOrder"] integerValue] forKey:@"NeedToRate"];
                break;
            }
            case 11 : {
                if(pickUpMode){
                    
                    [_Map clear];
                    [self addUserCurrentCar];
                 }
                current.Angle = msg[@"angle"];
                [self updateDriverInfo:msg[@"lat"] lng:msg[@"lng"]];
                
            }break;
                
                
            default:
                break;
        }
        
        
    }
    
    
    
}


-(void)journeyDidFinish{
    
    [GeneralFunc addIntPref:[[GeneralFunc getPrefStringForKey:@"lastOrder"] integerValue] forKey:@"NeedToRate"];

   
    FullPopupView *fullPopupView = [[NSBundle mainBundle] loadNibNamed:@"FullPopupView" owner:nil options:nil].lastObject;
    fullPopupView.frame = self.view.bounds;
    fullPopupView.delegate = self;
    fullPopupView.popupType = FullPopupTypeJourneyDidFinsh;
    
    fullPopupView.alpha = 0;
    [self.view addSubview:fullPopupView];
    [fullPopupView fadeInWithDuration:1];

}


-(void)orderDidCancel{
    
    isComeFromPopup = true;

    
    FullPopupView *fullPopupView = [[NSBundle mainBundle] loadNibNamed:@"FullPopupView" owner:nil options:nil].lastObject;
    fullPopupView.frame = self.view.bounds;
    fullPopupView.delegate = self;
    fullPopupView.popupType = FullPopupTypeJourneyDidFinsh;
    
    fullPopupView.alpha = 0;
    [self.view addSubview:fullPopupView];
    [fullPopupView fadeInWithDuration:1];
    

    
}

@end

//
//  ContactUsViewController.h
//  Momayaz
//
//  Created by Hammoudeh Alamri on 9/4/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import "ViewController.h"
#import "HTTPDelegate.h"

@interface ContactUsViewController : UIViewController <HTTPDel>
@property (weak, nonatomic) IBOutlet UITextField *etxt_Sub;
@property (weak, nonatomic) IBOutlet UITextField *etxt_Name;
@property (weak, nonatomic) IBOutlet UITextField *etxt_Email;
@property (weak, nonatomic) IBOutlet UITextField *etxt_Phone;
@property (weak, nonatomic) IBOutlet UITextView *etxt_Body;
@property (weak, nonatomic) IBOutlet UISegmentedControl *seg_reason;

@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@end

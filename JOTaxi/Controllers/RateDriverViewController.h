//
//  RateDriverViewController.h
//  Momayaz
//
//  Created by Hammoudeh Alamri on 06/05/2017.
//  Copyright © 2017 GCE. All rights reserved.
//

#import "BaseController.h"
#import "HCSStarRatingView.h"


@interface RateDriverViewController : BaseController<HTTPDel> 
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblPlate;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *viRate;
@property (weak, nonatomic) IBOutlet UITextView *txtComment;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *cancleButton;
@property  BOOL isFromHistory;

@property (strong, nonatomic)  NSString *driverName;
@property (strong, nonatomic)  NSString *plateNumber;
@property (strong, nonatomic)  NSString *orderID;


@end

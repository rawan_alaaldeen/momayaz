//
//  MainRootViewController.h
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/15/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"

@interface MainRootViewController : UINavigationController

@property(weak) MainViewController * mainViewController;
@end

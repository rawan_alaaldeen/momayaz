//
//  RegisterViewController.h
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/15/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import "BaseController.h"
 

@interface RegisterViewController : UIViewController <HTTPDel,UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet UITextField *etxtUsername_reg;
@property (weak, nonatomic) IBOutlet UITextField *etxtPass1_reg;
@property (weak, nonatomic) IBOutlet UITextField *etxtPass2_reg;
@property (weak, nonatomic) IBOutlet UITextField *etxtEmail_reg;
@property (weak, nonatomic) IBOutlet UITextField *etxtPhone_reg;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segGender;
@property (atomic) int UserID;
@property (atomic) BOOL isSMUser;
@property (weak, nonatomic) IBOutlet UIStackView *stackView;

@end

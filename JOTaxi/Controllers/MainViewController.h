//
//  MainViewController.h
//
//  Created by Grigory Lutkov on 25.04.15.
//  Copyright (c) 2015 Grigory Lutkov. All rights reserved.
//

#import "LGSideMenuController.h"
#import "LeftViewController.h"

@interface MainViewController : LGSideMenuController <leftProtocole>

@property (strong, nonatomic) LeftViewController *leftViewController;

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController
                         presentationStyle:(LGSideMenuPresentationStyle)style
                                      type:(NSUInteger)type;

@end

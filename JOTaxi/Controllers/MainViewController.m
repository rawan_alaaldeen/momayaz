//
//  MainViewController.m
//  LGSideMenuControllerDemo
//
//  Created by Grigory Lutkov on 25.04.15.
//  Copyright (c) 2015 Grigory Lutkov. All rights reserved.
//

#import "MainViewController.h"
#import "ViewController.h"
#import "LeftViewController.h"
 #import "AppDelegate.h"
#import "MainRootViewController.h"
#import "JOTaxi-Swift.h"

@interface MainViewController ()

@property (assign, nonatomic) NSUInteger type;

@end

@implementation MainViewController


- (instancetype)initWithRootViewController:(UIViewController *)rootViewController
                         presentationStyle:(LGSideMenuPresentationStyle)style
                                      type:(NSUInteger)type
{
    
    
    self = [super initWithRootViewController:rootViewController];
    
    
    if (self)
    {
        _type = type;

        // -----

        _leftViewController = [[LeftViewController alloc] initWithNibName:@"SideMenuView" bundle:nil];
        
        _leftViewController.delegete = self; 
 
        // -----

        if (type == 0)
        {
            [self setLeftViewEnabledWithWidth:250.f
                            presentationStyle:style
                         alwaysVisibleOptions:LGSideMenuAlwaysVisibleOnNone];

            self.leftViewStatusBarStyle = UIStatusBarStyleDefault;
            self.leftViewBackgroundImage = [UIImage imageNamed:@"menu_bg"];
            self.leftViewStatusBarVisibleOptions = LGSideMenuStatusBarVisibleOnNone;
 

            if (style == LGSideMenuPresentationStyleScaleFromBig)
            {
              //  self.leftViewBackgroundImage = [UIImage imageNamed:@"image"];
                self.leftViewBackgroundColor = [UIColor colorWithWhite:1.f alpha:0.9];
                

            }
            else if (style == LGSideMenuPresentationStyleSlideAbove)
            {
                self.leftViewBackgroundColor = [UIColor colorWithWhite:1.f alpha:0.9];

             
                
            }
            else if (style == LGSideMenuPresentationStyleSlideBelow)
            {
              //  self.leftViewBackgroundImage = [UIImage imageNamed:@"image"];
                self.leftViewBackgroundColor = [UIColor colorWithWhite:1.f alpha:0.9];
                

                
            }
            else if (style == LGSideMenuPresentationStyleScaleFromLittle)
            {
              //  self.leftViewBackgroundImage = [UIImage imageNamed:@"image"];
               
                  self.leftViewBackgroundColor = [UIColor colorWithWhite:1.f alpha:0.9];
                
 
            }
        }
        else if (type == 1)
        {
            [self setLeftViewEnabledWithWidth:250.f
                            presentationStyle:style
                         alwaysVisibleOptions:LGSideMenuAlwaysVisibleOnPhoneLandscape|LGSideMenuAlwaysVisibleOnPadLandscape];

            self.leftViewStatusBarStyle = UIStatusBarStyleDefault;
            self.leftViewStatusBarVisibleOptions = LGSideMenuAlwaysVisibleOnPadLandscape;
        //    self.leftViewBackgroundImage = [UIImage imageNamed:@"image"];
              self.leftViewBackgroundColor = [UIColor colorWithWhite:1.f alpha:0.9];
            

                    }
        else if (type == 2)
        {
            [self setLeftViewEnabledWithWidth:250.f
                            presentationStyle:style
                         alwaysVisibleOptions:LGSideMenuAlwaysVisibleOnNone];

            self.leftViewStatusBarStyle = UIStatusBarStyleDefault;
            self.leftViewStatusBarVisibleOptions = LGSideMenuStatusBarVisibleOnAll;
            self.leftViewBackgroundColor = [UIColor colorWithWhite:1.f alpha:0.9];

         
            
        }
        else if (type == 3)
        {
            [self setLeftViewEnabledWithWidth:250.f
                            presentationStyle:style
                         alwaysVisibleOptions:LGSideMenuAlwaysVisibleOnNone];

            self.leftViewStatusBarStyle = UIStatusBarStyleLightContent;
            self.leftViewStatusBarVisibleOptions = LGSideMenuStatusBarVisibleOnAll;
            self.leftViewBackgroundColor = [UIColor colorWithWhite:0.f alpha:0.5];

            
            
        }
        else if (type == 4)
        {
            self.swipeGestureArea = LGSideMenuSwipeGestureAreaFull;
            self.rootViewCoverColorForLeftView = [UIColor colorWithRed:0.f green:1.f blue:0.5 alpha:0.3];
            self.rootViewScaleForLeftView = 0.6;
            self.rootViewLayerBorderWidth = 3.f;
            self.rootViewLayerBorderColor = [UIColor whiteColor];
            self.rootViewLayerShadowRadius = 10.f;
            
            [self setLeftViewEnabledWithWidth:250.f
                            presentationStyle:LGSideMenuPresentationStyleScaleFromBig
                         alwaysVisibleOptions:LGSideMenuAlwaysVisibleOnNone];

            self.leftViewAnimationSpeed = 0.4;
            self.leftViewStatusBarStyle = UIStatusBarStyleDefault;
            self.leftViewStatusBarVisibleOptions = LGSideMenuStatusBarVisibleOnNone;
         //   self.leftViewBackgroundImage = [UIImage imageNamed:@"image"];
             self.leftViewBackgroundColor = [UIColor colorWithWhite:1.f alpha:0.9];
            self.leftViewStatusBarVisibleOptions = LGSideMenuStatusBarVisibleOnPadLandscape;
            self.leftViewBackgroundImageInitialScale = 1.5;
            self.leftViewInititialOffsetX = -200.f;
            self.leftViewInititialScale = 1.5;

            _leftViewController.tableView.backgroundColor = [UIColor clearColor];
            _leftViewController.tintColor = [UIColor whiteColor];

            
          
        }

        
        [_leftViewController.tableView reloadData];
        [self.leftView addSubview:_leftViewController.view];

        
    }
    
    
  
    
    return self;
}


-(void)viewWillAppear:(BOOL)animated{

    
    
    [_leftViewController updateContent];

}

- (void)leftViewWillLayoutSubviewsWithSize:(CGSize)size
{
    [super leftViewWillLayoutSubviewsWithSize:size];

  //  if (![UIApplication sharedApplication].isStatusBarHidden && (_type == 2 || _type == 3))
      //  _leftViewController.tableView.frame = CGRectMake(0.f , 20.f, size.width, size.height-20.f);
    //else
       // _leftViewController.tableView.frame = CGRectMake(0.f , 0.f, size.width, size.height);
    
    [_leftViewController.tableView updateConstraints];
    
}

-(void)doAction:(NSString *)actionName{

    
    if([actionName isEqualToString:@"out"]){
    [GeneralFunc logout];
        
    [self.navigationController popToRootViewControllerAnimated:YES];
    
        [self showHideLeftViewAnimated:YES completionHandler:nil];
        
        
        ViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"ViewController"];
        
        MainRootViewController *navigationController = [[MainRootViewController alloc] initWithRootViewController:viewController];
        
        RootMainViewController = [[MainViewController alloc] initWithRootViewController:navigationController presentationStyle:LGSideMenuPresentationStyleSlideBelow type:1];
        
        
        
        UIWindow *window = [UIApplication sharedApplication].delegate.window;
        
        window.rootViewController = RootMainViewController;
        
        [UIView transitionWithView:window
                          duration:0.5
                           options:UIViewAnimationOptionTransitionCurlDown
                        animations:nil
                        completion:nil];

        
        
        [_leftViewController updateContent];
        
        
    }
    
    else if([actionName isEqualToString:@"profile"]){
        
        
        UIStoryboard *editProfileViewController = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        [self presentViewController:[editProfileViewController instantiateViewControllerWithIdentifier:@"EditProfileViewController"] animated:YES completion:^{
            [self showHideLeftViewAnimated:YES completionHandler:nil];

        }];
        
         
        
    }
 
    else if([actionName isEqualToString:@"contactUs"]){
    
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        [self presentViewController:[sb instantiateViewControllerWithIdentifier:@"ContactUsViewController"] animated:YES completion:^{
            [self showHideLeftViewAnimated:YES completionHandler:nil];
            
        }];
        
        
        
    }
    
    else if  ([actionName isEqualToString:@"history"]){
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        [self presentViewController:[sb instantiateViewControllerWithIdentifier:@"HistroyViewController"] animated:YES completion:^{
            [self showHideLeftViewAnimated:YES completionHandler:nil];
            
        }];
        

    }
    else if  ([actionName isEqualToString:@"call"]){

        [self showHideLeftViewAnimated:YES completionHandler:^{
 
            NSURL *phoneNumber = [NSURL URLWithString:@"tel:090009000"];
            if (phoneNumber){
                [[UIApplication sharedApplication] openURL:phoneNumber];
            }
        }];
        
     }

    else if([actionName isEqualToString:@"aboutUs"]){
    
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        [self presentViewController:[storyboard instantiateViewControllerWithIdentifier:@"AboutUsViewController"] animated:YES completion:^{
            [self showHideLeftViewAnimated:YES completionHandler:nil];
            
        }];

    
    }
    
   
    

}
@end

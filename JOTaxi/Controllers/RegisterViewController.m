//
//  RegisterViewController.m
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/15/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import "RegisterViewController.h"
#import "JOTaxi-Swift.h"

@import Firebase;
@import FirebaseMessaging;

@interface RegisterViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpaceEmailAndNameConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpaceEmailAndPasswordConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *stackViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *stackViewSmalliPhoneHeightConstrant;


@end

@implementation RegisterViewController
{
      HTTPDelegate * dl ;
}
- (void)viewDidLoad {
     [super viewDidLoad];
    
    
    
    if( [GeneralFunc getPrefStringForKey:@"Email"]){
    
        _etxtEmail_reg.text = [GeneralFunc getPrefStringForKey:@"Email"];
        _etxtUsername_reg.text = [GeneralFunc getPrefStringForKey:@"NameEn"];
        
        if([[GeneralFunc getPrefStringForKey:@"Gender"]isEqualToString:@"female"]){
            _segGender.selected =1;
        }else{
             _segGender.selected =0;
        }
        
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated{

    
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}




-(void)viewWillDisappear:(BOOL)animated{

    self.navigationController.navigationBarHidden = YES;
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    [self updateConstraint];

    
    if (_isSMUser){
        for (UIView *sub in _stackView.subviews){
            if (sub.tag ==  10) {
                sub.hidden = true;
            }
        }        _etxtPass1_reg.hidden = true;
        _etxtPass2_reg.hidden = true;
    }

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateConstraint{
    
    if ([GeneralFunc isSmallDevise]){
        _stackViewSmalliPhoneHeightConstrant.active = true;
        _stackViewHeight.active = false;
    }
    else{
        _stackViewSmalliPhoneHeightConstrant.active = false;
        _stackViewHeight.active = true;
        
    }
}

- (IBAction)goBack:(id)sender {

    
    [self dismissViewControllerAnimated:false completion:nil];
    
}

- (IBAction)actionDoReg:(id)sender {
    
    if(![AppHelper isThereInternetConnection]){
        [self showCallAlert];
    }
    
    else{
    BOOL allIsWell= true;
    
    
    // validate email
    if(![GeneralFunc validateEmail:_etxtEmail_reg.text]){
    
        
        allIsWell =false;
        
        [GeneralFunc validateTextField:_etxtEmail_reg isValid:false];
        
        
    }else{
        [GeneralFunc validateTextField:_etxtEmail_reg isValid:true];

    }
    
    
    /// validate phone
    NSString *phoneNumber = _etxtPhone_reg.text;
    if (![phoneNumber hasPrefix:@"0"]){
        phoneNumber = [NSString stringWithFormat:@"%@%@",@"0",phoneNumber];
    }
    
    if ([phoneNumber length] != 10 ){
        allIsWell =false;
        [GeneralFunc validateTextField:_etxtPhone_reg isValid:false];
    }
    else{
        if(![GeneralFunc validatePhone:phoneNumber]){
            allIsWell =false;
            [GeneralFunc validateTextField:_etxtPhone_reg isValid:false];
        }
        else{
            [GeneralFunc validateTextField:_etxtPhone_reg isValid:true];
            
        }
 
    }
    
    
    
    //** check password filed just when its normal registration
    NSString *password = @"";
    if (!_isSMUser){
    if(([_etxtPass1_reg.text stringByReplacingOccurrencesOfString:@" " withString:@""].length<1 || [_etxtPass2_reg.text stringByReplacingOccurrencesOfString:@" " withString:@""].length<1) || ![_etxtPass1_reg.text isEqualToString:_etxtPass2_reg.text] ){
        
        allIsWell =false;
        [GeneralFunc validateTextField:_etxtPass1_reg isValid:false];
        [GeneralFunc validateTextField:_etxtPass2_reg isValid:false];
        
    }else{
        password = _etxtPass1_reg.text;
        [GeneralFunc validateTextField:_etxtPass1_reg isValid:true];
        [GeneralFunc validateTextField:_etxtPass2_reg isValid:true];
    }
    }
    else{
        password = @"GCEO@A";
    }
    
    
    if([_etxtUsername_reg.text stringByReplacingOccurrencesOfString:@" " withString:@""].length<1){
        
        allIsWell =false;
        [GeneralFunc validateTextField:_etxtUsername_reg isValid:false];
        
    }else{
        [GeneralFunc validateTextField:_etxtUsername_reg isValid:true];
        
    }
    
    if(allIsWell){
    
        [GeneralFunc showLoading:self];

        
    }else{
    
        return;
    }

   
    
    NSString * url = [NSString stringWithFormat:@"%@%@userID=%d&NameEn=%@&NameAR=%@&Password=%@&Phone=%@&Email=%@&Gender=%d&verified=%d&GCMTokenID=%@", URLsMainURL,URLsAddUser, _UserID , [GeneralFunc urlencode: _etxtUsername_reg.text],[GeneralFunc urlencode: _etxtUsername_reg.text],password, phoneNumber, _etxtEmail_reg.text,(int)_segGender.selectedSegmentIndex,0, [[FIRInstanceID instanceID] token]];

    
    
    dl = [[HTTPDelegate alloc] initWithURL:url];
    dl.delegate =self;
    [dl startHttp];
    }
    
    
}

-(void)showCallAlert{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        CallPopup *popup = [[NSBundle mainBundle] loadNibNamed:@"CallPopup" owner:nil options:nil].lastObject;
        popup.frame = self.view.bounds;
        popup.alpha = 0;
        [self.view addSubview:popup];
        
        [popup fadeInWithDuration:0.5];
    });
    
}

#pragma mark - Http

-(void)HTTPJustStart{
    
    [GeneralFunc showLoading:self];
}
-(void)HTTPDone:(NSData *)data response:(NSURLResponse *)res error:(NSError *)error{
 
    if(!error){
    dispatch_async(dispatch_get_main_queue(), ^{
        [GeneralFunc hideLoading:self];
        
 
//        error: -1
//        email : -2
//        phone : -3
//        success : userID
        
        NSInteger res = [JSONParser ParseRegRes:data];
        
        
        UIAlertController * al = [GeneralFunc showAlertDialogWithTitle:NSLocalizedString(@"", @"") Message:NSLocalizedString(@"Done successfully!", @"") ];
        
                
        switch (res) {
            case -1:{
                al.title =NSLocalizedString(@"Error", @"");
                 al.message =NSLocalizedString(@"Sorry!! something wrong, please try again!", @"");
                [al addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction *l ){
                    [al popoverPresentationController];
                    
                    
                   
                }]];
            }
                break;
                
            case -2:
            {
                
                al.title =NSLocalizedString(@"", @"");
                al.message =NSLocalizedString(@"Email is already register", @"");
                
                
                [al addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction *l ){
                    [al popoverPresentationController];
                    
                }]];
            }

                
                break;
            case -3:
            {
                
                al.title =NSLocalizedString(@"", @"");
                al.message =NSLocalizedString(@"Phone is already register ", @"");
                

                [al addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction *l ){
                    [al popoverPresentationController];
                    
                }]];
            }

                break;
                
            default:
            {
                
                 
                [al addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction *l ){
                    [self dismissViewControllerAnimated:YES completion:nil];
                }]];
            
            }
                break;
        }
        
        [self presentViewController:al animated:YES completion:nil];

    });}
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [GeneralFunc hideLoading:self];
            NSLog(@"Register error : %@",error.description);
        });
    }
}

-(void)HTTPInProgress{
}

#pragma mark:- Text field delegate
    
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if (textField == _etxtUsername_reg){
        [_etxtPass1_reg becomeFirstResponder];
    }
    else if (textField == _etxtPass1_reg){
        [_etxtPass2_reg becomeFirstResponder];
    }
    else if (textField == _etxtPass2_reg){
        [_etxtPhone_reg becomeFirstResponder];
    }
    else if (textField == _etxtPhone_reg){
        [_etxtEmail_reg becomeFirstResponder];
    }
    else if (textField == _etxtEmail_reg){

        [textField resignFirstResponder];}
    return true;
}
@end

//
//  HistroyViewController.swift
//  JOTaxi
//
//  Created by Rawan Alaeddin on 9/12/17.
//  Copyright © 2017 GCE. All rights reserved.
//

import UIKit

let  mainURL = "http://188.247.86.76:8890/api/"
class HistroyViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.tableFooterView = UIView()
        }
    }
    
    @IBOutlet weak var tabsStachView: UIStackView!
    @IBOutlet weak var loadingIndecator: UIActivityIndicatorView!
    
    @IBOutlet weak var bookTripButton: UIButton!
    @IBOutlet weak var noTripsView: UIView!
    var dataArray:[HistoryModel]?
    
    var isScheduled = true
    var isLoading = false
    var currentPage = 0
    var deletedItemId:Int?
    var cancelOrderView:CancelOrderView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getHistoryList()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        bookTripButton.isHidden = true
        noTripsView.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tabDidSelect(_ sender: UIButton) {
        
        for subView in tabsStachView.subviews{
            if subView.tag == sender.tag{
                subView.backgroundColor = UIColor.white
            }
            else{
                subView.backgroundColor =  UIColor(hexString: "FFCD07")
            }
        }
        
        if sender.tag == 1{
            isScheduled = true
        }
        else{
            isScheduled = false
        }
        
        resetTable()
     }
    
    
    func  getHistoryList() {
        
         loadingIndecator.startAnimating()
        let path  = mainURL+"Location/GetOrdersHistory?UserID="+GeneralFunc.getPrefString(forKey: "userid")+"&PageNo="+"\(currentPage)"+"&IsScheduled=\(isScheduled)"
        
        isLoading = true
        APIManager.get(path: path) { (sucess, jsonData) in
            
            self.hideLoadingIndecator()
            if sucess && jsonData != nil {
                if self.dataArray == nil {
                    self.dataArray = [HistoryModel]()
                }
                self.dataArray! += HistoryModel.getArray(fromData: jsonData!)
                self.isLoading = false
                
                if self.dataArray!.count != 0 {
                    self.tableView.reloadData()
                }
                else{
                    self.showNoTripView()
                }
            }
        }
    }
    
     @IBAction func dismiss() {
        
        DispatchQueue.main.async {
            
            self.dismiss(animated: true, completion: nil)}
    }
    
    
    private func showNoTripView(){
        self.noTripsView.isHidden = false
        self.bookTripButton.isHidden = false

    }

    fileprivate func resetTable(){
        currentPage = 0
        dataArray = [HistoryModel]()
        tableView.reloadData()
        self.getHistoryList()

        
    }
 
    private func hideLoadingIndecator(){
        DispatchQueue.main.async {
            
         self.loadingIndecator.stopAnimating()
        }

    }
    
    
    @IBAction func bookNewTrip() {
        dismiss()
    }


}

extension HistroyViewController:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell") as? HistoryCell
        
        if (cell == nil){
            cell = Bundle.main.loadNibNamed("HistoryCell", owner: nil, options: nil)?.last as? HistoryCell
            cell?.delegate = self
        }
        dataArray![indexPath.row].isSchaduled = isScheduled
        cell?.historyItem = dataArray![indexPath.row]
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "OldJourneyViewController") as! OldJourneyViewController
        
        viewController.historyModel = dataArray![indexPath.row]
        
        present(viewController, animated: false, completion: nil)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = CGFloat(offset.y + bounds.size.height - inset.bottom)
        let h = CGFloat(size.height)
        
        let reload_distance = CGFloat(20)
        if(y > (h + reload_distance) && !isLoading) {
            isLoading = true
            currentPage += 1
            getHistoryList()
        }
    }
}

extension HistroyViewController:HistoryCellDelegate{
    func deleteItem(_ historyItem: HistoryModel) {
        
        deletedItemId = historyItem.orderID
        DispatchQueue.main.async {
            
            if self.cancelOrderView == nil {
                self.cancelOrderView = Bundle.main.loadNibNamed("CancelOrderView", owner: nil, options: nil)?.last as? CancelOrderView }
            
            self.cancelOrderView!.frame = self.view.bounds;
            self.cancelOrderView!.delegate = self;
            self.cancelOrderView!.alpha = 0;
            self.view.addSubview(self.cancelOrderView!)
            self.cancelOrderView!.fadeIn(withDuration: 1)
            
        }
    }
}


extension HistroyViewController:CancelOrderPopupDelegate{
    func cancelOrder(withReason reason: String) {
        
        if deletedItemId != nil  {
            let url = mainURL+URLsCancelOrder+"OrderID="+"\(deletedItemId!)"+"&Reason="+"\(GeneralFunc.urlencode(reason)!)"+"&isDriver=false"
            APIManager.get(path: url) { (success, data) in
                if success {
                    DispatchQueue.main.async {
                        self.cancelOrderView!.removeFromSuperview()
                        self.resetTable()
                    }
                }
            }
            
        }
    }
}

//
//  ExtraFeesPopup.m
//  JOTaxi
//
//  Created by Rawan Alaeddin on 9/3/17.
//  Copyright © 2017 GCE. All rights reserved.
//

#import "CreateOrderPopup.h"

@interface CreateOrderPopup ()

@end

@implementation CreateOrderPopup

 
-(void)didMoveToSuperview{
    
    _submitButton.layer.borderWidth = 1;
    _submitButton.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _cancelButton.layer.borderWidth = 1;
    _cancelButton.layer.borderColor = [UIColor whiteColor].CGColor;
    
    
    if (_popupType == extraFeesPopup){
        _titleLabel.text = NSLocalizedString(@"extra_fees", @"");
        _detailsLabel.text = NSLocalizedString(@"thanks", @"");
        [_submitButton setTitle:NSLocalizedString(@"confirm", @"") forState:UIControlStateNormal];
        [_cancelButton setTitle:NSLocalizedString(@"cancel", @"") forState:UIControlStateNormal];
        
        
    }
    else{
        _titleLabel.text = NSLocalizedString(@"order_again_text", @"");
        _detailsLabel.text = @"";
        
        [_submitButton setTitle:NSLocalizedString(@"order_again", @"") forState:UIControlStateNormal];
        [_cancelButton setTitle:NSLocalizedString(@"cancel", @"") forState:UIControlStateNormal];
        
        
    }
    
    
}
- (IBAction)back {
    [self removeFromSuperview];
}

- (IBAction)book {
    
    [self removeFromSuperview];

 
    [_delegate openBookingScreen];
}

 @end

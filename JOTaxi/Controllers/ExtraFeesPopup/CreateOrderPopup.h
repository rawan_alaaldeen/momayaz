//
//  ExtraFeesPopup.h
//  JOTaxi
//
//  Created by Rawan Alaeddin on 9/3/17.
//  Copyright © 2017 GCE. All rights reserved.
//

#import <UIKit/UIKit.h>

#define orderAgainPopup 1
#define extraFeesPopup 0


@protocol CreateOrderPopupDelegate <NSObject>

-(void)openBookingScreen;

@end

@interface CreateOrderPopup : UIView
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailsLabel;
 
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@property int popupType; // 0 when extra fees , 1 when order again

@property (nonatomic, weak) id <CreateOrderPopupDelegate> delegate;

@end

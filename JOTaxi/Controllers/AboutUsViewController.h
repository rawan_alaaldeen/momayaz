//
//  AboutUsViewController.h
//  JOTaxi
//
//  Created by Rawan Alaeddin on 8/31/17.
//  Copyright © 2017 GCE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutUsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *aboutUsText;

@end

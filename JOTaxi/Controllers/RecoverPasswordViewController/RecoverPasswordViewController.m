//
//  RecoverPasswordViewController.m
//  JOTaxi
//
//  Created by Rawan Alaeddin on 9/2/17.
//  Copyright © 2017 GCE. All rights reserved.
//

#import "RecoverPasswordViewController.h"
#import "Consts.h"

@interface RecoverPasswordViewController ()
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@end

@implementation RecoverPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    _submitButton.layer.borderWidth = 1;
    _submitButton.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _cancelButton.layer.borderWidth = 1;
    _cancelButton.layer.borderColor = [UIColor whiteColor].CGColor;

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)submit {
    
     NSString *Identifier = _emailTextField.text;
     
     NSString * url = [NSString stringWithFormat:@"%@%@Email=%@", URLsMainURL ,@"User/ForgetPassword?",Identifier];
     
     HTTPDelegate *dl = [[HTTPDelegate alloc] initWithURL:url];
     dl.delegate = self;
     [dl startHttp:102];
}

- (IBAction)back {
    [self dismissViewControllerAnimated:true completion:nil];
}


-(void)HTTPJustStart{
    
    [GeneralFunc showLoading:self];
}

-(void)HTTPDone:(NSData *)data response:(NSURLResponse *)res error:(NSError *)error{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [GeneralFunc hideLoading:self];
    });
}

-(void)HTTPDone:(NSData *)data response:(NSURLResponse *)res error:(NSError *)error forID:(int)ID{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [GeneralFunc hideLoading:self];
    });

    if(ID ==102){ // change password
        
        
        UIAlertController * al=nil;
        
        NSString * s = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        if( [[s stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:@"1"]){
            al = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Password recovery", @"")
                                                     message:NSLocalizedString(@"Your new password has been sent. Please check your email.",@"")
                                              preferredStyle:UIAlertControllerStyleAlert];
            
        }
        else if( [s isEqualToString:@"-1"]){
            al = [UIAlertController alertControllerWithTitle:@""
                                                     message:NSLocalizedString(@"email not found",@"")
                                              preferredStyle:UIAlertControllerStyleAlert];
            
        }
        else{
            
            
            al = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Password recovery", @"")
                                                     message:NSLocalizedString(@"Some error occur, please try again",@"")
                                              preferredStyle:UIAlertControllerStyleAlert];
            
        }
        
        
        
        if(al){
            
            [al addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleCancel handler:^(UIAlertAction *l ){                     [al popoverPresentationController];
                
            }]];
            
            [self presentViewController:al animated:YES completion:nil];
            
        }
        
        
    }
}

@end

//
//  RecoverPasswordViewController.h
//  JOTaxi
//
//  Created by Rawan Alaeddin on 9/2/17.
//  Copyright © 2017 GCE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTTPDelegate.h"


@interface RecoverPasswordViewController : UIViewController<HTTPDel>

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
 
@end

//
//  OldJourneyViewController.swift
//  JOTaxi
//
//  Created by Rawan Alaeddin on 9/15/17.
//  Copyright © 2017 GCE. All rights reserved.
//

import UIKit
import GoogleMaps

class OldJourneyViewController: UIViewController {


    @IBOutlet weak var map: GMSMapView!

    @IBOutlet weak var ratingButton: UIButton!
    @IBOutlet weak var startLocationLabel: UILabel!
    @IBOutlet weak var endLocationLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var starsStackView: UIStackView!

    
    @IBOutlet weak var endPlaceView: UIView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var driverNameLabel: UILabel!
    
    var historyModel:HistoryModel?
    var jourenyData:JourneyModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        startLocationLabel.text = historyModel?.sAddress ?? ""
        
        if historyModel?.eAddress == nil  || historyModel?.eAddress == "" {
            endPlaceView.isHidden = true
        }
        else{
            endPlaceView.isHidden = false
            endLocationLabel.text = historyModel?.eAddress!
        }

        dateLabel.text = historyModel?.creationDate ?? ""
        
        if let sourceLocation  = historyModel?.location {
            let sourceMarker =  GMSMarker()
            sourceMarker.icon = #imageLiteral(resourceName: "user_pin")
            sourceMarker.position = sourceLocation
            sourceMarker.map = map

            let camera = GMSCameraPosition(target: sourceLocation, zoom: 16, bearing: 0, viewingAngle: 0)
            map.animate(to: camera)
         }

     }
 
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        ratingButton.isHidden = true
        getJourneyData()
    }
    
    private func getJourneyData(){
        
         guard historyModel != nil else{
            return
        }
        
        let path  = mainURL+"Location/GetOrdersHistoryDetails?OrderID="+"\(historyModel!.orderID!)"
        
        APIManager.get(path: path) { (sucess, jsonData) in 
            if sucess && jsonData != nil {
 
                self.jourenyData  = JourneyModel(jsonData!)
                self.fillData()
            }
        }
    }
 
    private func fillData(){

        if jourenyData != nil {
            
            driverNameLabel.text = (historyModel?.driverName ?? "")+"  "+(jourenyData!.carType ?? "")

            durationLabel.text = "\(String(describing: jourenyData!.duration!))"
            
            distanceLabel.text = "\(String(describing: jourenyData!.distance!))"
 
            // driver rate
            if jourenyData!.driverRating! == -1 {
                ratingButton.isHidden = false
            }
            
            else{
                   ratingButton.isHidden = true

                   for i in stride(from: 5, to: 5-jourenyData!.driverRating!, by: -1){
                    let image  = starsStackView.subviews[i-1] as! UIImageView
                    image.image = #imageLiteral(resourceName: "yellow_star")
                 }
            }
         }
        
    }

    @IBAction func rate() {
        let ratingViewController = RateDriverViewController()
        ratingViewController.isFromHistory = true
        ratingViewController.driverName = historyModel?.driverName ?? ""
        ratingViewController.plateNumber = jourenyData?.plateNumber ?? ""
        ratingViewController.orderID = "\(historyModel!.orderID!)"
        present(ratingViewController, animated: true, completion: nil)
 
    }
    
    @IBAction func dismiss() {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func callDriver() {
    }

}

//
//  NewOrderTrakerViewController.h
//  Momayaz
//
//  Created by Hammoudeh Alamri on 11/23/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewOrderTrakerViewController : UIViewController <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webview;

@end

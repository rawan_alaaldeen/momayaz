//
//  SearchLocationViewController.m
//  JO Taxi
//
//  Created by Hammoudeh Alamri on 19/04/2017.
//  Copyright © 2017 iTeks. All rights reserved.
//

#import "SearchLocationViewController.h"
#import <IQKeyboardManager/IQKeyboardManager.h> 
#import "IQUIView+Hierarchy.h"
#import "IQUIView+IQKeyboardToolbar.h"
#import "PlaceTableViewCell.h" 
#import "JOTaxi-Swift.h"

@interface SearchLocationViewController ()

@end

@implementation SearchLocationViewController
{
    
    __weak IBOutlet UIStackView *stackView;
    NSTimer *_autoCompleteTimer;
    NSString *_substring;
    NSMutableArray *arr;
    NSMutableArray *favArr;

    BOOL isextend;
    
    HTTPDelegate *http ;
    
    int currentPage ;
    
    BOOL isWorking;
    TZSegmentedControl *tabs;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(_seachMode ==0){
        
        _constBuildingWidth.constant =0;
        [self.view layoutIfNeeded];
        [self.view updateConstraints];
        
//        _viBuildingSelect.hidden= true;
//        _viPersonalSelect.hidden= false;
//        _viFavSelect.hidden= true;
    }
    
    favArr = [NSMutableArray new];
    arr= [NSMutableArray new];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"PlaceTableViewCell" bundle:nil] forCellReuseIdentifier:@"Cell"];

    
    
    
    [self prepareTabs];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    UITapGestureRecognizer *tapOnView = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped)];
    [self.view addGestureRecognizer:tapOnView];
        
}


-(void)viewDidAppear:(BOOL)animated{

     self.constBuildingWidth.constant=0;
    _etSearch.placeholder= NSLocalizedString(@"Search", nil);
   
    
   
    
}

-(void)viewTapped{
    [self.view endEditing:true];
}

-(void)prepareTabs{
    
    int imgSize =30;
    int imgSelectedSize =40;
    

    tabs = [[TZSegmentedControl alloc] initWithSectionTitles:@[NSLocalizedString(@"Nearby", nil), NSLocalizedString(@"Favorites", nil), NSLocalizedString(@"Recent", nil),NSLocalizedString(@"Buildings", nil), NSLocalizedString(@"Map", nil)]
                                               sectionImages:@[[GeneralFunc imageResize:[UIImage imageNamed:@"personal.png" ] andResizeTo:CGSizeMake(imgSize, imgSize)]  ,[GeneralFunc imageResize:[UIImage imageNamed:@"fav_sel.png" ]andResizeTo:CGSizeMake(imgSize, imgSize)],[GeneralFunc imageResize:[UIImage imageNamed:@"ic_history_48px.png" ]andResizeTo:CGSizeMake(imgSize, imgSize)],[GeneralFunc imageResize:[UIImage imageNamed:@"building_search.png" ]andResizeTo:CGSizeMake(imgSize, imgSize)],[GeneralFunc imageResize:[UIImage imageNamed:@"map_search.png"]andResizeTo:CGSizeMake(imgSize, imgSize)]]
                                              selectedImages:@[[GeneralFunc imageResize:[UIImage imageNamed:@"personal.png" ] andResizeTo:CGSizeMake(imgSelectedSize, imgSelectedSize)]  ,[GeneralFunc imageResize:[UIImage imageNamed:@"fav_sel.png" ]andResizeTo:CGSizeMake(imgSelectedSize, imgSelectedSize)],[GeneralFunc imageResize:[UIImage imageNamed:@"ic_history_48px.png" ]andResizeTo:CGSizeMake(imgSelectedSize, imgSelectedSize)],[GeneralFunc imageResize:[UIImage imageNamed:@"building_search.png" ]andResizeTo:CGSizeMake(imgSelectedSize, imgSelectedSize)],[GeneralFunc imageResize:[UIImage imageNamed:@"map_search.png"]andResizeTo:CGSizeMake(imgSelectedSize, imgSelectedSize)]]];
    
    tabs.frame = CGRectMake(0, 0, _viTabs.frame.size.width/1.5, _viTabs.frame.size.height);
    tabs.center = CGPointMake(_viTabs.frame.size.width/3,  _viTabs.frame.size.height / 2);
    
    tabs.backgroundColor = [UIColor clearColor];
    
    tabs.titleTextAttributes  =@{NSForegroundColorAttributeName:UIColorFromRGB(0x114577)};
    tabs.borderWidth = 0.5;
    tabs.verticalDividerEnabled = true;
    tabs.verticalDividerWidth = 1.0;
    tabs.selectionIndicatorHeight = 0.0;
    tabs.indicatorWidthPercent = 1;
    tabs.selectionIndicatorColor = [UIColor grayColor];
 
    tabs.segmentWidth = 25; //tabs.bounds.size.width/(tabs.sectionTitles.count+3);
    tabs.segmentWidthStyle =TZSegmentedControlSegmentWidthStyleFixed ;
    tabs.edgeInset = UIEdgeInsetsMake(0, 20, 0, 20);
    [_viTabs addSubview:tabs];
    
    
    [tabs setIndexChangeBlock:^(NSInteger i){
        NSLog(@" Index %ld ", i );
        
        if(i==0){
            currentPage = 1;
            _seachMode =0;
            self.constBuildingWidth.constant=0;
            _etSearch.placeholder= NSLocalizedString(@"Search", nil);
            
        }
        
        // fav
        else if(i==1){
          self.etSearch.enabled =false;
            _seachMode =3;
             [self loadFav:0];
            
        }
    
        // nearby
        else if(i==2){
            _seachMode =2;
             [self loadRecent:1];
        }
        
        else if(i==3){
            _seachMode = 1;
            _constBuildingWidth.constant=120;
            _etSearch.placeholder= NSLocalizedString(@"Street Name", nil);
            _etBuilding.placeholder =NSLocalizedString(@"Building No.", nil);
        }
        
        // map
        else if(i==4){
            
            SelectFromMapViewController * mapc = [SelectFromMapViewController new];
            mapc.delegate = self.delegate;
            mapc.seachType = _seachType;
            [self.navigationController pushViewController:mapc animated:YES];
        }
        
        [arr removeAllObjects];
        [_tableView reloadData];
        _etBuilding.text=@"";
        _etSearch.text=@"";
        
        
    }];


}
- (IBAction)back:(id)sender {
    [self.view endEditing:true];
     [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)changeSearchMode:(id)sender {

    

    UIButton *btn = (UIButton *) sender;
    
    for (UIView *subView in stackView.subviews) {
        if (subView.tag == btn.tag){
            subView.backgroundColor = UIColorFromRGB(0xFFCD07);
        }
        else{
            subView.backgroundColor = [UIColor clearColor];
        }
    }

     self.etSearch.enabled =true;
    _etSearch.hidden =  false;
    _constBuildingWidth.constant = 0 ;

    // building
    if(btn.tag ==1){
        _seachMode = 1;
        currentPage = 1;
        [self startSearchItems:1 extend:false];
        _constBuildingWidth.constant=_etSearch.frame.size.width;
        _etSearch.hidden =  true;
        _viBuildingSelect.hidden= false;
         _viPersonalSelect.hidden= true;
         _viFavSelect.hidden= true;
        
    }
    
    // history
    else  if(btn.tag ==2){
        
        _etSearch.hidden =  true;
        _seachMode = 4;
        _constBuildingWidth.constant=100;
        _viBuildingSelect.hidden= false;
        _viPersonalSelect.hidden= true;
        _viFavSelect.hidden= true;
        currentPage = 0;
        [self loadRecent:currentPage];
        
    }
    
    // nearby
    else  if(btn.tag ==0){

         _seachMode =0;
        currentPage = 1;
        [self startSearchItems:1 extend:false];
         _viBuildingSelect.hidden= true;
         _viFavSelect.hidden= true;
        _viPersonalSelect.hidden= false;
    
    }
    
    
    [arr removeAllObjects];
    [_tableView reloadData];
    _etBuilding.text=@"";
    _etSearch.text=@"";
    
    // fav
    if(btn.tag ==3){
    

         self.etSearch.enabled =false;
        _seachMode =3;
        _constBuildingWidth.constant=0;
        _viBuildingSelect.hidden= true;
        _viFavSelect.hidden= false;
        _viPersonalSelect.hidden= true;
        
        [self loadFav:0];
        
    }
    
}


-(void) loadRecent:(int)page{

    NSString *url= [NSString stringWithFormat:@"%@%@&UserID=%ld&PageNo=%d&type=%d",URLsMainURL, URLsLoadRecent,[GeneralFunc getPrefIntForKey:@"userid"],page,_seachType];
    
    http = [[HTTPDelegate alloc] initWithURL:url];
    http.delegate=self;
    
    [http startHttp:106];
    self.loading.hidden = NO;
    [self.loading startAnimating];
  

}
-(void) loadFav:(int)page{

    
    NSString *url= [NSString stringWithFormat:@"%@%@&UserID=%ld&PageNo=%d",URLsMainURL, URLsLoadFav,[GeneralFunc getPrefIntForKey:@"userid"],page];
    
 
    http = [[HTTPDelegate alloc] initWithURL:url];
    http.delegate=self;
    
    [http startHttp:105];
    self.loading.hidden = NO;
    [self.loading startAnimating];

}


#pragma mark - Actions

- (IBAction)onCloseButtonPressed:(id )sender {
    [_etSearch resignFirstResponder];
    [_etBuilding resignFirstResponder];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}



-(IBAction)openMap:(id)sender{

    
    SelectFromMapViewController * mapc = [SelectFromMapViewController new];
    mapc.delegate = self.delegate;
    mapc.seachType = _seachType; 
    [self.navigationController pushViewController:mapc animated:YES];

}

-(void)viewWillAppear:(BOOL)animated{
    
    

    // select first tap
    for (UIView *subView in stackView.subviews){
        if (subView.tag == 0){
            subView.backgroundColor = UIColorFromRGB(0xFFCD07);}
        else{
            subView.backgroundColor = [UIColor clearColor];
        }
        
    }
    _seachMode =0;
    
    currentPage = 1;
    [self startSearchItems:1 extend:false];

     [_etSearch addDoneOnKeyboardWithTarget:self action:@selector(startsearch)];
    [_etBuilding addDoneOnKeyboardWithTarget:self action:@selector(startsearch)];
    
    if (_seachType == 2){
        _titleLabel.text =  NSLocalizedString(@"where_to_go", nil);
    }
    else{
        _titleLabel.text =  NSLocalizedString(@"pick_up_location", nil);
    }
    
}

-(void)viewWillDisappear:(BOOL)animated{

  //  [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    
}


-(void)startsearch {
    
    
    BOOL allisWell = true;
    
    currentPage=1;
    
    //** search by bulidng number
    if(_seachMode==1){
    if([_etBuilding.text stringByReplacingOccurrencesOfString:@" " withString:@""].length<1){
        [_etBuilding resignFirstResponder];
        allisWell=false;
        
    }else{
        [GeneralFunc validateTextField:_etBuilding isValid:true];
    }
    }
    
    //** search in by name
    else{
    if([_etSearch.text stringByReplacingOccurrencesOfString:@" " withString:@""].length<1){
        [_etSearch resignFirstResponder];
         allisWell=false;
        
    }else{
        [GeneralFunc validateTextField:_etSearch isValid:true];
    }
}
    
    if(allisWell){
        _etSearch.enabled =false;
        _etBuilding.enabled =false;
        
        _substring = [NSString stringWithString:_etSearch.text];
        
        [self startSearchItems:1 extend:false ];
        
    }
}


-(void)startSearchItems:(int)page extend:(BOOL )  extend{

     isextend = extend;
    
    if(!_substring)
        _substring=@"";
    
    NSString *url = [NSString stringWithFormat:@"%@&name=%@&x=%lf&y=%lf&Page=%d" , URLsSearchPlaceByName,[GeneralFunc urlencode:_substring], [[GeneralFunc getPrefStringForKey:@"lng"] doubleValue], [[GeneralFunc getPrefStringForKey:@"lat"] doubleValue],page];
    
    if(_seachMode==1){
        url= [NSString stringWithFormat:@"%@&name=%@&num=%@&x=%lf&y=%lf&Page=%d",URLsSearchPlaceByBuilding,[GeneralFunc urlencode:_substring],[GeneralFunc urlencode:[GeneralFunc convertToEnglishNumber: _etBuilding.text]],[[GeneralFunc getPrefStringForKey:@"lng"] doubleValue], [[GeneralFunc getPrefStringForKey:@"lat"] doubleValue],page];
    }
    
    
    http = [[HTTPDelegate alloc] initWithURL:url];
    http.delegate=self;
    
    [http startHttp:100];
    self.loading.hidden = NO;
    [self.loading startAnimating];


}


-(void)addToFav: (UIButton *)btn{
    
    Place * p = [arr objectAtIndex:btn.tag];
    NSString *url =@"";
    
    
    if(!btn.isSelected){
        NSString *type = @"";
        if (_seachMode == 1){
            type = @"Building";
        }
        else{
            type = p.TypeAr;
        }
        url= [NSString stringWithFormat:@"%@%@&UserID=%ld&Longitude=%@&Latitude=%@&AddressName=%@&AddressType=%@&StreetName=%@",URLsMainURL, URLsAddFav,[GeneralFunc getPrefIntForKey:@"userid"], p.lng,p.lat,[GeneralFunc urlencode:p.BNum],[GeneralFunc urlencode:type],[GeneralFunc urlencode:p.stAr] ];
        
        p.isFav = true;
        
         [favArr addObject:[NSNumber numberWithInteger:btn.tag]];
        
        http = [[HTTPDelegate alloc] initWithURL:url];
        http.delegate=self;
        [http startHttp:103];
    
    }else{

        url= [NSString stringWithFormat:@"%@%@ID=%ld",URLsMainURL, URLsRemFav,p.ID];
        
       // [favArr addObject:[NSNumber numberWithInteger:btn.tag]];
        http = [[HTTPDelegate alloc] initWithURL:url];
        http.delegate=self;
        [http startHttp];
        
        if(_seachMode==3){
            [arr removeObjectAtIndex:btn.tag];
            [_tableView reloadData];
        }
        
        for(int i =0; i<favArr.count ; i++){
            
            
            if(btn.tag == [[favArr objectAtIndex:i] integerValue] ){
                [favArr removeObjectAtIndex:i];
            }
            
        }

        
    }
    
    btn.selected = !btn.isSelected;
   
    
    

}

#pragma mark - text

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    
    [arr removeAllObjects];
    [_tableView reloadData];
    
    currentPage =1;
    [self startsearch];
    
    return true;

}


#pragma mark - table
#pragma mark - Table View Data Source Methods



-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return NO;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arr.count  ;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [self locationSearchResultCellForIndexPath:indexPath];
    
    
}


- (UITableViewCell *)locationSearchResultCellForIndexPath:(NSIndexPath *)indexPath {
    
    
    Place * p = [arr objectAtIndex:indexPath.row];
    
    PlaceTableViewCell *cell = [self.tableView  dequeueReusableCellWithIdentifier:@"PlaceTableViewCell"];
    
    if (!cell){
        cell = [[NSBundle mainBundle] loadNibNamed:@"PlaceTableViewCell" owner:nil  options: nil].lastObject;
    }
    
    if(_seachMode==1){
        cell.line1.text = p.stAr;
        cell.line2.text = [NSString stringWithFormat:@"%@ : %@ ",NSLocalizedString(@"Building No.", @""), p.BNum];
        cell.btnFav.selected = false;
        
    }else if(_seachMode==0){
        cell.line1.text = p.nameAr;
        cell.line2.text = [NSString stringWithFormat:@"%@", p.stAr ];
        cell.btnFav.selected = p.isFav;
        
    }else if(_seachMode==3){
        if ([p.TypeAr isEqualToString:@"Building"]){
            cell.line1.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Building No.", @""), p.nameAr ];}
        else{
            cell.line1.text = p.nameAr;}
        
        cell.line2.text = [NSString stringWithFormat:@"%@", p.stAr ];
        cell.btnFav.selected = true;
        
    }
    
    else if(_seachMode==2){
        
        cell.line1.text = p.nameAr;
        cell.line2.text = [NSString stringWithFormat:@" "];
        cell.btnFav.selected = false;
        
        
    }
    
    if(_seachMode>2)
        for(int i =0; i<favArr.count ; i++){
        
        if(indexPath.row == i ){
            cell.btnFav.selected = true;
        }else{
            cell.btnFav.selected = false;
        }
        
    }
    
    cell.btnFav.tag =indexPath.row;
    
    [cell.btnFav addTarget:self action:@selector(addToFav:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}




#pragma mark - Table View Delegate Methods


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.etSearch resignFirstResponder];
    [self.etBuilding resignFirstResponder];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Place *p = [arr objectAtIndex:indexPath.row];
    
    [self.delegate searchViewControllerDidReturnPlace:p];
    [self.view endEditing:true];
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView_
{
    CGFloat actualPosition = scrollView_.contentOffset.y;
    CGFloat contentHeight = scrollView_.contentSize.height - _tableView.frame.size.height;
    if (actualPosition >= contentHeight) {
        if(!isWorking && arr.count>0){
            if (_seachMode == 4 ){
                currentPage++;
                [self loadRecent:currentPage];
            }
            else{
                [self startSearchItems:++currentPage extend:YES];
            }

        }
    }
}

#pragma mark - Http
-(void)HTTPInProgress{
    isWorking=true;
    
}
-(void)HTTPJustStart{
    isWorking = true;
}

-(void)HTTPDone:(NSData *)data response:(NSURLResponse *)res error:(NSError *)error{
    
    isWorking = false;
    
}

-(void)HTTPDone:(NSData *)data response:(NSURLResponse *)res error:(NSError *)error forID:(int)ID{
    
    isWorking = false;
     if(ID==103){
        
        return;
    }
      else  {
    
    
        if(!isextend){
            [arr removeAllObjects];
        }
        
    if(_seachMode ==1){
     [arr addObjectsFromArray: [NSMutableArray arrayWithArray: [JSONParser ParsePlaceByBuilding:data]]];
    
    }else if(_seachMode ==0){
    
     [arr addObjectsFromArray: [NSMutableArray arrayWithArray: [JSONParser ParsePlaceByName:data]]];
    
    }else if(_seachMode ==3 ){
        [arr addObjectsFromArray:[NSMutableArray arrayWithArray: [JSONParser ParseFavPlace:data]]];
        //** history
    }else if(_seachMode==4){
    
         [arr addObjectsFromArray:[NSMutableArray arrayWithArray: [JSONParser ParseRecentPlace:data]]];}
    
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self.tableView reloadData];
        _tableView.hidden = false;
        
        self.loading.hidden = YES;
        _loadingConst.constant=0;
        
        _etSearch.enabled =YES;
        _etBuilding.enabled =YES;
    });
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



@end

//
//  main.m
//  Momayaz
//
//  Created by Hammoudeh Alamri on 8/10/16.
//  Copyright © 2016 GCE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

//
//  StylingHelper.m
//  JO Taxi
//
//  Created by Hammoudeh Alamri on 18/04/2017.
//  Copyright © 2017 iTeks. All rights reserved.
//

#import "StylingHelper.h"
#import <QuartzCore/QuartzCore.h>


@implementation StylingHelper


+(void) addCornerRadius:(UIView *) view radius:(CGFloat) r{

    view.layer.cornerRadius =r;
    view.clipsToBounds = YES;
    view.layer.masksToBounds = true;
}


+(void) addShadow:(UIView *)view radius:(CGFloat) r{

    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(1, 4.0);
    view.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    view.layer.shadowRadius = r;
    view.layer.shadowOpacity = 0.7;
    
    view.layer.shadowPath = [UIBezierPath bezierPathWithRect:view.bounds].CGPath;


}

+(void) circlizeView:(UIView *)view{
    CAShapeLayer *shape = [CAShapeLayer layer];
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:view.center radius:(view.bounds.size.width / 2) startAngle:0 endAngle:(2 * M_PI) clockwise:YES];
    shape.path = path.CGPath;
    view.layer.mask = shape;

}



@end

//
//  CallPopup.swift
//  JOTaxi
//
//  Created by Rawan Alaeddin on 9/16/17.
//  Copyright © 2017 GCE. All rights reserved.
//

import UIKit

class CallPopup :UIView{

    @IBOutlet weak var cancelButton: UIButton!{
        didSet{
            cancelButton.layer.borderColor = UIColor.white.cgColor
            cancelButton.layer.borderWidth = 1
        }
    }
    
    @IBOutlet weak var callButton: UIButton!{
        didSet{
            callButton.layer.borderColor = UIColor.white.cgColor
            callButton.layer.borderWidth = 1
        }
    }
    
    
    @IBAction func hideView() {
        
        self.removeFromSuperview()
    }

    @IBAction func call() {
        if let url = URL(string: "tel:090009000") {
            UIApplication.shared.openURL(url)
        }
    }
}
